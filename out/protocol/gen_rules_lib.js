// LICENSE_CODE ZON
'use strict'; /*jslint node:true*/
const zconf = require('../util/config.js');
const _ = require('underscore');
const date = require('../util/date.js');
const string = require('../util/string.js');
const zescape = require('../util/escape.js');
const zurl = require('../util/url.js');
const zutil = require('../util/util.js');
const zerr = require('../util/zerr.js');
const version_util = require('../util/version_util.js');
const countries = require('./pub/countries.js');
const env = process.env, ms = date.ms, assign = Object.assign;
const E = exports;
E.t = {};
// XXX colin: change apkid -> apk_id, and get groups from db
E.rule_get_group = function(apkid, md5){
    let bbc_iplayer = 'bbc.iplayer.android';
    let bbc_mediaplayer = 'air.uk.co.bbc.android.mediaplayer';
    let play_app = 'com.android.vending';
    let play_svc = 'com.google.android.gms';
    if (apkid==bbc_iplayer || apkid==bbc_mediaplayer)
    {
        return [{apkid: bbc_mediaplayer, md5: 'apk'},
            {apkid: bbc_mediaplayer+'.wget', md5: 'apk-play-wget'},
            {apkid: bbc_iplayer, md5: 'apk'},
            {apkid: bbc_iplayer+'.wget', md5: 'apk-play-wget'}];
    }
    if (apkid==play_app || apkid==play_svc)
    {
        return [{apkid: play_app, md5: 'apk'},
            {apkid: play_app+'.wget', md5: 'apk-play-wget'},
            {apkid: play_svc, md5: 'apk'},
            {apkid: play_svc+'.wget', md5: 'apk-play-wget'}];
    }
    return [{apkid: apkid, md5: md5}, {apkid: apkid+'.wget',
        md5: 'apk-play-wget'}];
function merge_json(a, b){
    if (!a)
        return b;
    for (let p in b)
    {
        if (a[p]!==undefined && b[p].constructor==Object)
            a[p] = merge_json(a[p], b[p]);
        else
            a[p] = b[p];
    }
    return a;
E.t.merge_json = merge_json;
function get_script(scripts, md5){
    if (!scripts)
        return;
    for (let i=0; i<scripts.length; i++)
    {
        if (scripts[i]._id==md5)
            return zutil.clone_deep(scripts[i]);
    }
E.script_from_template = function(rule, script){
    return JSON.parse(script.replace(/\$(apkid|host|country|param)\b/g,
        function(r){
        switch (r)
        {
        case '$apkid': return rule.apkid;
        case '$host': return rule.host;
        case '$country': return rule.country;
        case '$param': return rule.param;
        }
    }));
function get_scripts(op, data, err_func){
    let u = op.unblocker2, now = new Date();
    if (op.groups_array)
    {
        u = op.unblocker2 = {urls: {}, apks: {}};
        let counter = 0;
        _.each(op.groups_array, g=>{
            if (!g.type)
                return;
            if (!g.country || !g.md5)
                return;
            let t = g.type=='url' ? 'urls' : 'apks';
            let name = g.name + '_' + counter++;
            u[t][name] = {name: g.name, country: g.country, md5: g.md5,
                enabled: true, ts: now};
        });
    }
    let q = {urls: {}, apks: {}}, i;
    for (i in u.urls)
        q.urls[i] = get_script(data.scripts_urls, u.urls[i].md5);
    for (i in u.apks)
        q.apks[i] = get_script(data.scripts_apks, u.apks[i].md5);
    let r, s, name;
    op.scripts = q;
    for (i in u.urls)
    {
        name = i;
        r = u.urls[i];
        if (!(s=q.urls[i]))
        {
            err_func('get_rules_invalid_id_urls',
                JSON.stringify({id: i, op: op}));
            continue;
        }
        delete s._id;
        s.readonly = true;
        if (r.name)
            name = r.name;
        try {
            s.script = E.script_from_template({host: name,
                country: r.country.toUpperCase()}, s.script);
        } catch(e){} // XXX shachar: decide what to do here
    }
    for (i in u.apks)
    {
        name = i;
        r = u.apks[i];
        if (!(s=q.apks[i]))
        {
            err_func('get_rules_invalid_id_apks',
                JSON.stringify({id: i, op: op}));
            continue;
        }
        delete s._id;
        s.readonly = true;
        if (op.groups_array)
            name = r.name;
        try {
            s.script = E.script_from_template({apkid: name,
                param: r.param, country: r.country.toUpperCase()},
                s.script);
        } catch(e){}
    }
    return u;
E.t.get_scripts = get_scripts;
E.auto_group = function(){
    return {sid: '2', readonly: 1, script: {
        name: 'Automatic VPN rules',
        description: 'Automatically generated scripts from the '
        +'sites/apks you enabled using the auto VPN feature',
        unblocker_rules: {},
    }};
function gen_groups_fmt(op){
    let groups = op.unblocker_groups = [];
    let vals = op.unblocker_vals = [];
    let i, r, s;
    let u2 = op.unblocker2;
    let scripts = op.scripts;
    if (!u2.urls && !u2.apks)
        return;
    let n = groups.push(E.auto_group());
    vals.push({sid: '2', script: {unblocker_rules: {}}});
    let u_group = groups[n-1].script.unblocker_rules;
    let u_val = vals[n-1].script.unblocker_rules;
    let added = 0;
    for (i in u2.urls)
    {
        s = scripts.urls[i];
        r = u2.urls[i];
        if (!s || !r.country)
            continue;
        added = 1;
        u_group[i] = s.script;
        u_val[i] = assign({type: 'url'}, r);
    }
    for (i in u2.apks)
    {
        s = scripts.apks[i];
        r = u2.apks[i];
        if (!s || !r.country)
            continue;
        added = 1;
        u_group[i] = s.script;
        u_val[i] = assign({type: 'apk'}, r);
    }
    if (!added)
    {
        groups.pop();
        vals.pop();
    }
E.t.gen_groups_fmt = gen_groups_fmt;
function is_supported_country(op, rule){
    if (!rule.country)
        return true;
    let c = rule.country;
    if (!c.list || !c.type || !(c.list instanceof Array))
        return true;
    if (c.type=='in' && (!op.country || !c.list.includes(op.country))
        || c.type=='not_in' && op.country && c.list.includes(op.country))
    {
        return false;
    }
    return true;
function is_rule_supported(op, rule){
    if (rule.os && rule.os instanceof Array && rule.os.length
        && !rule.os.includes(op.os_ver))
    {
        return false;
    }
    if (op.os_ver=='android' && rule.apps)
        return false;
    if (op.os_ver=='windows' && (op.uuid || op.plugin
        || op.win_ver_name=='win8') && (rule.apps || rule.ports))
    {
        return false;
    }
    if (!is_supported_country(op, rule))
        return false;
    return true;
function json_groups_fixup(op, json){
    let groups = json.groups;
    for (let i=0; i<groups.length; i++)
    {
        let g_rules = groups[i].script.unblocker_rules;
        for (let _rule in g_rules)
        {
            let rule = g_rules[_rule];
            // XXX vladimir: rgrep logs to find cid/uuid causing errs
            if (typeof rule=='string')
                throw new Error('invalid rule type');
            delete rule.active;
            delete rule.active_bext;
            delete rule.active_plugin;
            rule.supported = is_rule_supported(op, rule);
            if (!rule.n)
                continue;
            for (let j=0; j<rule.n.length; j++)
                rule.n[j].supported = is_rule_supported(op, rule.n[j]);
        }
    }
    return json;
function json_fixup_glob(_if){
    if (_if.host)
        _if.host = zurl.http_glob_host(_if.host, 1);
    else if (_if.url)
        _if.url = zurl.http_glob_url(_if.url, 1);
    _if.type = _if.type=='=glob' ? '=~' : '!~';
    return _if;
E.t.json_fixup_glob = json_fixup_glob;
function json_rules_expand(op, json){
    let groups = json.groups;
    for (let i=0; i<groups.length; i++)
    {
        let g_rules = groups[i].script.unblocker_rules;
        for (let _rule in g_rules)
        {
            let rule = g_rules[_rule];
            rule.name = rule.name||_rule;
            if (!rule.n || !op.expand || op.meta_only)
                continue;
            for (let j=0; j<rule.n.length; j++)
            {
                let r = g_rules[_rule+'_'+j] = rule.n[j];
                r.enabled = rule.enabled;
                r.name = rule.name;
                r.parent = rule;
                if (rule.no_direct_filter)
                    r.no_direct_filter = true;
            }
            delete rule.n;
        }
    }
function merge_groups(op, json){
    let groups = json.groups;
    let enable = json.enabled===undefined||json.enabled;
    let x = {unblocker_rules: {}, enable: enable};
    if (op.enabled_only && !enable)
        return x;
    let rules = x.unblocker_rules, rule;
    for (let i=0; i<groups.length; i++)
    {
        let g_rules = groups[i].script.unblocker_rules;
        if (groups[i].sid!='2' && op.os_ver=='android'
            && version_util.cmp(op.ver, '1.3.554')>0)
        {
            continue;
        }
        for (let _rule in g_rules)
        {
            rule = g_rules[_rule];
            if (!op.show_all)
            {
                if (op.enabled_only && !rule.enabled)
                    continue;
                if (op.defined_only && rule.enabled===undefined)
                    continue;
            }
            /* XXX shachar BACKWARD: old svc versions fail json parsing if any
             * rule was illegal (should probably improve code here to filter
             * other types of errors that cause svc to reject). */
            if (op.os_ver=='android'
                && version_util.cmp(op.ver, '1.2.305')<0)
            {
                let cmds = rule.cmds;
                if (!cmds)
                    continue;
                let j;
                for (j=0; j<cmds.length && cmds[j].hosts; j++);
                if (j<cmds.length)
                    continue;
            }
            /* XXX arik BACKWARD: version 1.1.151 had partial support of
             * root_url and we don't want to enable it */
            if (version_util.cmp(op.ver, '1.1.157')<0)
                delete rule.root_url;
            rule.sid = groups[i].sid;
            // XXX shachar: change rule 'name' to something more appropriate
            // after rules format change
            // XXX shachar: can't change rule name because of android app
            // dependency on format
            rules[groups[i].sid+'_'+_rule] = rule;
        }
    }
    return x;
let dynamic_sites = string.qw(env.DYNAMIC_SITES!==undefined ?
    env.DYNAMIC_SITES :
    'netflix.com hulu.com hulu.jp itv.com channel4.com rte.ie');
function check_dynamic_sites(root_urls){
    let urls = [];
    if (!dynamic_sites.length)
        return null;
    for (let i=0; i<root_urls.length; i++)
        urls.push(new RegExp(root_urls[i]));
    for (let i=0; i<dynamic_sites.length; i++)
    {
        let site = zurl.add_proto(dynamic_sites[i])+'/';
        for (let j=0; j<urls.length; j++)
        {
            if (urls[j].test(site))
                return 'PEER';
        }
    }
    return null;
let dynamic_sites_apk = ['com.netflix.', 'com.hulu.'];
function check_dynamic_sites_apk(root_apks){
    if (!dynamic_sites_apk.length)
        return null;
    for (let i=0; i<dynamic_sites_apk.length; i++)
    {
        for (let j=0; j<root_apks.length; j++)
        {
            if (root_apks[j].startsWith(dynamic_sites_apk[i]))
                return 'PEER';
        }
    }
    return null;
function json_fixup_glob_url(urls){
    for (let u=0; u<urls.length; u++)
        urls[u] = zurl.http_glob_url(urls[u], 1);
    return urls;
E.t.json_fixup_glob_url = json_fixup_glob_url;
function root_url_fixup(rule){
    rule.root_url_orig = zutil.clone_deep(rule.root_url);
    json_fixup_glob_url(rule.root_url);
function root_url_fixup_dynamic(rule){
    if (!rule.root_url_orig)
        root_url_fixup(rule);
    return check_dynamic_sites(rule.root_url);
function bext_fixup_then(op, rule, country, features){
    let country_l = country.toLowerCase();
    let port = countries.ports_offset+countries.ports[country_l].port;
    if (op.agent_proxy[country_l])
        port = 22222;
    if (!features)
    {
        rule.then = 'PROXY '+country+':'+port;
        op.pac_countries[country.toLowerCase()] = 1;
        return country;
    }
    let f = zescape.parse.http_words(features), v, j;
    rule.then = 'PROXY '+country+':'+port;
    for (let i=0; i<f.length; i++)
    {
        let name = f[i][0], value = f[i][1];
        if (name=='PEER')
        {
            if (op.agent_proxy[country_l])
                port = 22223;
            if (!value)
            {
                rule.then = 'PROXY '+country+':'+port;
                continue;
            }
            v = value.split(',');
            for (j=0; j<v.length && v[j]!='BEXT'; j++);
            if (j<v.length)
                rule.then = 'PROXY '+country+':'+port;
        }
        else if (name=='VPS')
        {
            if (op.agent_proxy[country_l])
                port = 22222;
            if (!value)
            {
                rule.then = 'PROXY '+country+':'+port;
                continue;
            }
            v = value.split(',');
            if (v[0]=='CLIENT')
                continue;
            rule.then = 'PROXY '+country+':'+port;
            j = 0;
            if (v[0]=='BEXT')
                j++;
            for (; j<v.length; j++)
            {
                let _country = country+'_'+v[j];
                if (!op.agent_proxy[_country.toLowerCase()])
                    continue;
                op.pac_countries[_country.toLowerCase()] = 1;
                rule.then = 'PROXY '+_country+':'+port;
            }
        }
    }
    op.pac_countries[country.toLowerCase()] = 1;
    return country;
function _json_fixup_then(op, rule, dynamic){
    if (!rule.then)
    {
        rule.then = 'DIRECT';
        return null;
    }
    let n = rule.then.match(/^(DIRECT|PROXY ([A-Z]{2})(\.(.+))?)$/);
    if (!n || n[1]=='DIRECT')
    {
        rule.then = 'DIRECT';
        return null;
    }
    let country = n[2];
    /* XXX shachar hack: handle cases where country is UK in the database */
    if (country=='UK')
        country = 'GB';
    if (!countries.ports[country.toLowerCase()])
    {
        rule.then = 'DIRECT';
        return null;
    }
    let features = n[4];
    let c_rule;
    if (dynamic)
    {
        features = dynamic+(features ? ','+features : '');
        rule.then = 'PROXY '+country+'.'+features;
    }
    if (c_rule = op.peer_rules[country])
    {
        if (!features)
            features = c_rule;
        else if (!features.includes(c_rule))
            features = c_rule+','+features;
        rule.then = 'PROXY '+country+'.'+features;
    }
    if (!op.agent_proxy[country.toLowerCase()])
    {
        if (!features)
            features = 'PEER';
        else if (!features.includes('PEER'))
            features = 'PEER,'+features;
        rule.then = 'PROXY '+country+'.'+features;
    }
    if (!op.pac)
        return country;
    if (op.cid)
        rule.then = 'PROXY '+country;
    else
        country = bext_fixup_then(op, rule, country, features);
    return country;
function json_fixup_then(op, p_rule, rule, dynamic){
    let c = _json_fixup_then(op, rule, dynamic);
    if (c)
        p_rule.country = c.toLowerCase();
    if (op.direct_first && p_rule.country && rule.then=='DIRECT')
    {
        rule.then = 'PROXY '+p_rule.country.toUpperCase()+'.'+
            (dynamic ? dynamic+',' : '')+'direct_first';
    }
    if (p_rule.full_vpn)
    {
        rule.then = 'PROXY '+p_rule.country.toUpperCase()+
            (dynamic ? '.'+dynamic : '');
    }
    return c;
E.t.json_fixup_then = json_fixup_then;
/* XXX noam: add unit-test */
function json_rules_fixup(op, json){
    if (op.extended)
        return json_rules_fixup_extended(op, json);
    let rules = json.unblocker_rules;
    for (let i in rules)
    {
        let rule = rules[i], cmds = rule.cmds, dynamic, parent = rule.parent;
        if (rule.root_url)
            dynamic = root_url_fixup_dynamic(rule);
        else if (parent && parent.root_url)
            dynamic = root_url_fixup_dynamic(parent);
        if (rule.root_apk)
            dynamic = check_dynamic_sites_apk(rule.root_apk);
        else if (parent && parent.root_apk)
            dynamic = check_dynamic_sites_apk(parent.root_apk);
        if (rule.then && !json_fixup_then(op, rule, rule, dynamic))
            delete rule.then;
        if (!cmds)
            continue;
        /* XXX bahaa/shachar: duplicated in pub/pac_engine.js */
        let def_ext = rule['def-ext'] || ['gif', 'png', 'jpg', 'mp3',
            'css', 'mp4', 'wmv', 'flv', 'swf', 'mkv', 'ico', 'f4v', 'h264',
            'webp', 'webm', 'ts'];
        for (let j=0; j<cmds.length; j++)
        {
            let cmd = cmds[j];
            json_fixup_then(op, rule, cmd, dynamic);
            let _if = cmd['if'];
            if (!_if)
            {
                if (cmd.dst_dns && op.cid)
                    continue;
                _if = cmd['if'] =
                    [{ext: 'def-ext', type: 'in', then: 'DIRECT'}];
            }
            let has_ext = false;
            for (let k=0; k<_if.length; k++)
            {
                let __if = _if[k];
                json_fixup_then(op, rule, __if, dynamic);
                if (!__if.type)
                    continue;
                if (__if.type=='=glob' || __if.type=='!glob')
                {
                    json_fixup_glob(__if);
                    continue;
                }
                let ext = __if.ext;
                if (!ext)
                    continue;
                has_ext = true;
                if (ext instanceof Array)
                {
                    for (let l=0; l<ext.length; l++)
                    {
                        if (ext[l]!='def-ext')
                            continue;
                        ext.splice(l, 1);
                        __if.ext = ext.concat(def_ext);
                        break;
                    }
                }
                else if (ext=='def-ext')
                    __if.ext = def_ext;
            }
            if (!has_ext)
                _if.push({ext: def_ext, type: 'in', then: 'DIRECT'});
        }
        if (op.meta_only)
            delete rule.cmds;
    }
    return json;
function json_rules_fixup_extended(op, json){
    let rules = json.unblocker_rules;
    for (let i in rules)
    {
        let rule = rules[i], cmds = rule.cmds, dynamic, parent = rule.parent;
        if (rule.root_url)
            dynamic = root_url_fixup_dynamic(rule);
        else if (parent && parent.root_url)
            dynamic = root_url_fixup_dynamic(parent);
        if (rule.root_apk)
            dynamic = check_dynamic_sites_apk(rule.root_apk);
        else if (parent && parent.root_apk)
            dynamic = check_dynamic_sites_apk(parent.root_apk);
        if (op.meta_only)
            continue;
        if (!cmds)
        {
            cmds = rule.cmds = [];
            if (!rule.then)
                continue;
            cmds.push({then: rule.then});
            delete rule.then;
        }
        let def_ext = {
            direct: {gif: 1, png: 1, jpg: 1, css: 1, ico: 1, webp: 1},
            discover: {mp3: 1, mp4: 1, wmv: 1, flv: 1, swf: 1, mkv: 1, f4v: 1,
                h264: 1, ts: 1, webm: 1}
        };
        for (let j=0; j<cmds.length; j++)
        {
            let cmd = cmds[j], _dynamic = dynamic;
            json_fixup_then(op, rule, cmd, dynamic);
            if (!_dynamic && cmd.then.includes('PEER'))
                _dynamic = 'PEER';
            let c = (rule.country||'').toUpperCase();
            let _if = cmd['if'];
            if (!_if)
                _if = cmd['if'] = [];
            if (op.ext)
            {
                _if.push({main: true, type: '==', then: 'PROXY '+c});
                _if.push({ext: def_ext.direct, type: '=o',
                    then: 'PROXY '+c+'.direct_first', static: true});
                _if.push({ext: def_ext.discover, type: '=o',
                    then: 'PROXY '+c+'.direct_first_discover',
                    static: true});
                _if.push({url: '**/**Seg[0-9]+-Frag[0-9]+**', type: '=glob',
                    then: 'PROXY '+c+'.direct_first_discover',
                    static: true});
            }
            else
            {
                _if.push({ext: Object.keys(def_ext.direct), type: 'in',
                    then: 'PROXY '+c+'.direct_first', valid: true});
                _if.push({ext: Object.keys(def_ext.discover), type: 'in',
                    then: 'PROXY '+c+'.direct_discover', valid: true});
                _if.push({url: '**/**Seg[0-9]+-Frag[0-9]+**', type: '=glob',
                    then: 'PROXY '+c+'.direct_discover'});
            }
            _if.push({url: 'https://**', type: '=glob', then: 'PROXY '+c});
            _if.push({url: 'http://**', type: '=glob',
                then: 'PROXY '+c+'.direct_discover'});
            for (let k=0; k<_if.length; k++)
            {
                let __if = _if[k];
                let ext = __if.ext;
                if (!__if.type || !__if.then)
                {
                    ext.splice(k, 1);
                    k--;
                    continue;
                }
                json_fixup_then(op, rule, __if,
                    __if['static'] ? null : _dynamic);
                delete __if['static'];
                if (__if.type=='=glob' || __if.type=='!glob')
                {
                    json_fixup_glob(__if);
                    continue;
                }
                if (!ext)
                    continue;
                if (__if.valid)
                {
                    delete __if.valid;
                    continue;
                }
                if (ext instanceof Array)
                {
                    for (let l=0; l<ext.length; l++)
                    {
                        if (ext[l]!='def-ext' && !def_ext.direct[ext[l]] &&
                            !def_ext.discover[ext[l]])
                        {
                            continue;
                        }
                        ext.splice(l, 1);
                        l--;
                    }
                    if (!ext.length)
                    {
                        _if.splice(k, 1);
                        k--;
                    }
                }
                else if (ext=='def-ext')
                {
                    _if.splice(k, 1);
                    k--;
                }
            }
        }
    }
    return json;
E.t.json_rules_fixup_extended = json_rules_fixup_extended;
let limited_sites = [
    {name: 'youtube', re: '(^|\\.)youtube\\.com$', time: ms.DAY},
    {name: 'facebook', re: '(^|\\.)facebook\\.com$', time: ms.DAY},
    {name: 'google', re: '(^|\\.)google\\..*$', time: ms.DAY},
let _limited_sites = _.map(limited_sites, site=>({re: new RegExp(site.re),
    time: site.time}));
function is_rule_site_expired(root_urls, diff){
    for (let i=0; i<_limited_sites.length; i++)
    {
        let site = _limited_sites[i];
        for (let j=0; j<root_urls.length; j++)
        {
            if (site.re.test(root_urls[j]) && diff>site.time)
                return true;
        }
    }
    return false;
function is_rule_expired(op, rule){
    let now = op.ts||new Date();
    let diff = now-(rule.ts||0);
    if (!rule.enabled)
        return false;
    if (op.country && rule.country && op.country.toLowerCase()==rule.country &&
        diff>ms.DAY)
    {
        return true;
    }
    if (rule.root_url_orig)
        return is_rule_site_expired(rule.root_url_orig, diff);
    if (rule.parent && rule.parent.root_url_orig)
        return is_rule_site_expired(rule.parent.root_url_orig, diff);
    return false;
E.t.is_rule_expired = is_rule_expired;
function json_rules_expired(op, json){
    let rules = json.unblocker_rules;
    for (let i in rules)
    {
        let rule = rules[i];
        rule.expired = is_rule_expired(op, rule);
        if (op.disable_expired && rule.expired)
            rule.enabled = false;
    }
    return json;
function json_rules_cleanup(op, json){
    let rules = json.unblocker_rules;
    for (let i in rules)
    {
        let rule = rules[i];
        delete rule.ts;
        delete rule.parent;
        if (op.meta_only)
        {
            delete rule.cmds;
            delete rule.n;
        }
    }
    return json;
E.t.json_rules_cleanup = json_rules_cleanup;
function json_rules_vpn_only(op, json){
    let rules = json.unblocker_rules, _rules = {};
    if (!op.vpn_only)
        return json;
    for (let i in rules)
    {
        let r = rules[i];
        _rules[i] = {name: r.name, country: r.country, enabled: r.enabled,
            premium: r.md5=='premium', mode: r.mode||'unblock'};
    }
    json.unblocker_rules = _rules;
    return json;
E.get_rules_internal = function(op, data, err){
    op.peer_rules = data.peer_rules;
    op.agent_proxy = data.agent_proxy;
    op.pac_countries = {};
    op.unblocker2 = op.unblocker2||{};
    op.active = !op.cid || op.unblocker2.enabled;
    if (op.enabled)
        return op.active ? '1' : '0';
    get_scripts(op, data, err);
    gen_groups_fmt(op);
    op.res = {groups: {groups: op.unblocker_groups, enabled: op.active},
        vals: {groups: op.unblocker_vals, enabled: op.active}};
    let groups = op.res.groups;
    let vals = op.res.vals;
    json_groups_fixup(op, groups);
    if (!op.merge)
        return op.res;
    // XXX shachar: move loop to merge_groups
    for (let i=0; i<groups.groups.length; i++)
    {
        let un_g = groups.groups[i].script.unblocker_rules;
        let un_v = vals.groups[i].script.unblocker_rules;
        for (let r in un_g)
            un_g[r] = merge_json(un_g[r], un_v[r]);
    }
    json_rules_expand(op, groups);
    op.res = merge_groups(op, groups);
    let json = op.res;
    json_rules_fixup(op, op.res);
    json_rules_expired(op, op.res);
    json_rules_cleanup(op, op.res);
    json_rules_vpn_only(op, op.res);
    if (op.stamp)
        json.stamp = op.unblocker2.stamp||0;
    if (op.exceptions)
        json.exceptions = E.get_rule_exceptions(op);
    return json;
E.get_rule_exceptions = function(opt){
    opt = opt||{};
    return {
        dynamic: dynamic_sites,
        peer: dynamic_sites,
        pairs: [['bbc.com', 'bbc.co.uk']],
        limited: opt.no_limited ? [] : _.map(limited_sites,
            site=>_.pick(site, 're', 'time')),
    };
E.gen_apk_rules = function(rules_json, unblocker_params){
    let data = zutil.clone_deep(unblocker_params);
    // XXX vadiml fix properly someday: provide this data or get rid of it
    data.peer_rules = {};
    data.agent_proxy = {};
    let op = {merge: 1, expand: 1, enabled_only: 1, direct_first: 1,
        extended: 1, exceptions: 1, country: unblocker_params.country,
        os_ver: 'android', ver: zconf.ZON_VERSION};
    let u2 = {urls: {}, apks: {}};
    for (let rule in rules_json)
    {
        const original_rule = {name: rule, uid: rules_json[rule].uid,
            country: rules_json[rule].country, enabled: true, type: 'apk'};
        let extended_rules = E.rule_get_group(rule);
        for (let i=0; i<extended_rules.length; i++)
        {
            const erule = extended_rules[i];
            const rule_id = erule.apkid;
            let res_rule = zutil.clone_deep(original_rule);
            assign(res_rule, {name: erule.apkid, md5: erule.md5});
            const is_url = !res_rule.type||res_rule.type=='url';
            res_rule.md5 = res_rule.md5||(is_url ? 'vpn' : 'apk');
            if (is_url)
                u2.urls[rule_id] = res_rule;
            else
                u2.apks[rule_id] = res_rule;
        }
    }
    op.unblocker2 = u2;
    function err(msg, info){ zerr.err(msg+': '+JSON.stringify(info)); }
    E.get_rules_internal(op, data, err);
    return {unblocker_rules: op.res.unblocker_rules,
        exceptions: op.res.exceptions};
