// LICENSE_CODE ZON
'use strict'; /*jslint node:true*/
require('../util/config.js');
const conf = require('../util/conf.js');
// XXX vladimir: -DNODE_PLATFORM="android"
conf.app = 'svc_js_'+({linux: 'android'}[process.platform]||process.platform);
const agent_proxy = require('./unblocker/agent_proxy.js');
const unblocker_lib = require('./vpn/pub/unblocker_lib.js');
const smart_proxy = require('./unblocker/smart_proxy.js');
const transparent = require('./unblocker/transparent.js');
const zerr = require('../util/zerr.js');
const etask = require('../util/etask.js');
const gen_rules_lib = require('../protocol/gen_rules_lib.js');
const client = require('./vpn/peer/client.js');
const embed_perr = require('./embed_perr.js');
const local_storage = require('./local_storage.js');
const fs = require('fs');
const util = require('util');
const assert = require('assert');
const semver = require('semver');
const node10 = semver.gte(process.version, '10.0.0');
const async_hooks = node10 ? require('async_hooks') : null;
const E = exports;
let uuid, premium_mode;
if (process.env.ZNODE_DEP_SCAN)
    let dep_scan = require('../util/dep_scan.js');
    process.stdout.write(JSON.stringify(dep_scan()));
    process.exit(0);
process.zon.init();
// XXX shachar: find a better place to put this
etask(function*global_agent_cleanup(){
    const http = require('http');
    const https = require('https');
    this.on('finally', ()=>{
        http.globalAgent.destroy();
        https.globalAgent.destroy();
    });
    yield this.wait();
// Cannot call zerr/console from async hooks due to them being async call
// themselves which will cause an infinite loop
function debug(...args){
    fs.writeSync(process.stderr.fd, `${util.format(...args)}\n`); }
const hooks = new Map();
if (process.env.ASYNC_HOOK_BT)
    process.env.ASYNC_HOOK_PRINT = true;
    Error.stackTraceLimit = 30;
if (async_hooks)
    async_hooks.createHook({
        init: function(asyncid, type, trigger, res){
            const eid = async_hooks.executionAsyncId();
            if (process.env.ASYNC_HOOK_PRINT)
                debug(`${type}(${asyncid}): T(${trigger}) E(${eid})`);
            let stack = process.env.ASYNC_HOOK_BT ? new Error().stack
                : undefined;
            if (stack)
            {
                let n = 1; // 'Error'
                let l = stack.split('\n');
                assert(/^\s+at AsyncHook.init/.test(l[1]));
                n++;
                for (let i=2; i<l.length; i++)
                {
                    if (!/^\s+at [^\s]+ \(internal\/async_hooks\.js/.test(l[i]))
                        break;
                    n++;
                }
                stack = '\n'+l.slice(n).join('\n');
            }
            if (stack && trigger)
            {
                const h = hooks.get(trigger);
                if (h && h.stack)
                    stack += `\nASYNC ${h.type}(${h.trigger})${h.stack}`;
            }
            hooks.set(asyncid, {type, trigger, stack});
        },
        destroy: function(asyncid){
            const h = hooks.get(asyncid);
            hooks.delete(asyncid);
            if (h && process.env.ASYNC_HOOK_PRINT)
                debug(`${h.type}(${asyncid}): destroy (len ${hooks.size})`);
        }
    }).enable();
E.get_cid_js = ()=>client.cid_js||'';
E.get_session_key_js = ()=>client.session_key_js||'';
E.is_connected = ()=>client.connected>0;
E.has_internet = ()=>client.has_internet>0;
E.proxy_get_port = function(country, force_port, user, zone, password){
    return smart_proxy.get(country, force_port).port;
E.set_auth_info = function(_uuid, session_key, jwt){
    agent_proxy.set_auth_info(_uuid, session_key, jwt);
    E.reset_proxies();
E.should_redirect_outgoing_tcp = transparent.should_redirect_outgoing_tcp;
E.enable_app_unblocker = transparent.enable;
E.disable_app_unblocker = transparent.disable;
E.set_apkid = apkid=>{
    client.set_apkid(apkid);
E.set_appid = appid=>{
    client.set_appid(appid);
E.set_partnerid = client.set_partnerid;
E.set_sdk_version = client.set_sdk_version;
E.set_uuid = _uuid=>{
    uuid = _uuid;
    client.set_uuid(uuid);
E.get_uuid = ()=>client.get_uuid && client.get_uuid() || uuid || '';
E.set_after_install = client.set_after_install;
E.set_idle = client.set_idle;
E.set_confdir = confdir=>{
    zerr.notice(`set_confdir ${confdir}`);
    client.set_confdir(confdir);
    // XXX vadiml process.platform=='linux' = android
    if (process.platform=='linux')
        local_storage.set_confdir(confdir);
E.set_os_ver = ver=>{
    zerr.notice(`set os_ver to ${ver}`);
    client.set_os_ver(ver);
E.zid_update = client.zid_update;
E.set_mobile_enable = client.set_mobile_enable;
E.get_premium_mode = client.get_premium_mode;
E.set_premium_mode = function(mode){
    mode = !!mode;
    let is_real_change = premium_mode!==undefined && mode!=premium_mode;
    premium_mode = mode;
    unblocker_lib.set_user_plus(mode);
    client.set_premium_mode(mode, is_real_change);
E.set_product = function(prod){
    zerr.notice(`set product to ${prod}`);
    agent_proxy.set_product(prod);
E.set_paid_mode = function(mode){
    agent_proxy.set_paid_mode(mode);
    E.reset_proxies();
E.set_app_trial_mode = function(mode){
    unblocker_lib.set_app_trial_mode(mode);
    E.reset_proxies();
E.set_verify_proxy_conf = function(json){
    local_storage.put('verify_proxy_conf', json);
E.set_user_token = client.set_user_token;
E.get_user_token = client.get_user_token;
E.get_sync_token = client.get_sync_token;
E.set_client_conf = client.set_client_conf;
E.usage_update = client.usage_update;
E.notify_idle_state = client.notify_idle_state;
E.get_remote_data = client.get_remote_data;
E.start_connection = transparent.start_connection;
E.stop_connection = transparent.stop_connection;
E.get_and_flush = transparent.get_and_flush;
E.set_browser_root = function(url){
    smart_proxy.browser_root = url;
E.unblocker_json_set = function(json, opt){
    unblocker_lib.unblocker_json_set(json, opt);
    transparent.unblocker_json_set(json, opt);
E.set_unblocker_params = function(json){
    local_storage.put('unblocker_params', json);
    const rules = local_storage.get('unblocker_rules', {});
    E.unblocker_json_set_2(rules);
let unblocker_rules_cache = {};
E.unblocker_json_set_2 = function(json, opt){
    local_storage.put('unblocker_rules', json);
    const unblocker_params = local_storage.get('unblocker_params', {});
    unblocker_rules_cache = gen_rules_lib.gen_apk_rules(json,
        unblocker_params);
    if (unblocker_params.enable_proxy_rules)
    {
        unblocker_lib.unblocker_json_set({
            unblocker_rules: unblocker_rules_cache.unblocker_rules,
            unblocker_globals: unblocker_params.unblocker_globals,
            unblocker_plus: unblocker_params.unblocker_plus,
            exceptions: unblocker_rules_cache.exceptions,
        }, opt);
    }
    else
        unblocker_lib.unblocker_json_set(unblocker_rules_cache, opt);
    transparent.unblocker_json_set(unblocker_rules_cache,
        unblocker_params.apk_to_root_url_map, opt);
E.get_unblocker_rules = function(){
    return unblocker_rules_cache;
E.perr_wakeup = client.perr_wakeup;
E.reset_proxies = function(){ smart_proxy.reset(); };
E.change_agent = function(country){ smart_proxy.change_agent(country); };
E.get_last_req_ts = transparent.get_last_req_ts;
E.get_req_stats = function(uid){
    var stats = transparent.get_req_stats(uid);
    return JSON.stringify(stats);
E.get_pending_perrs = embed_perr.get_pending_perrs;
let last_gw;
E.gw_change_notify = function(gw){
    if (gw==last_gw)
        return;
    zerr.notice('Gateway IP changed from '+last_gw+' to '+gw);
    last_gw = gw;
    smart_proxy.purge_stats();
    unblocker_lib.cache_purge();
    client.init(gw);
E.connect_no_monitor = client.init;
process.once('SIGTERM', ()=>{
    // XXX shachar: rm old node handling (Jun-19)
    if (!node10)
    {
        process.exit(0);
        return;
    }
    etask.shutdown();
if (client.install_api)
    client.install_api(E);
