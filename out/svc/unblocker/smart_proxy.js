// LICENSE_CODE ZON
'use strict'; /*jslint node:true*/
var zconf = require('../../util/config.js');
var agent_proxy = require('./agent_proxy.js');
var unblocker_lib = require('../vpn/pub/unblocker_lib.js');
var incoming_request = require('./incoming.js');
var local_storage = require('../local_storage.js');
var etask = require('../../util/etask.js');
var date = require('../../util/date.js');
var ztls = require('../../util/tls.js');
var stats = require('../../util/stats.js');
var zurl = require('../../util/url.js');
var zerr = require('../../util/zerr.js');
var http_hdr = require('../../util/http_hdr.js');
var http = require('http');
var https = require('https');
var net = require('net');
var E = exports;
var engines = {http: http, https: https};
var smart_proxies = {};
var uid_to_root_url_map = {};
var last_id = 0;
var user_agent = 'Hola svc_js_'+agent_proxy.os+'/'+zconf.ZON_VERSION;
function smart_proxy(country){
    const unblocker_params = local_storage.get('unblocker_params', {});
    this.country = country;
    this.server = http.createServer();
    // HttpServer.prototype.listen is asynchronous, but only because it tries
    // DNS resolution on its address argument. We want to keep this sequence
    // synchronous, so using this undocumented Node internal here.
    this.server._listen2('127.0.0.1', undefined, -1, -1);
    var addr = {};
    this.server._handle.getsockname(addr);
    this.port = addr.port;
    zerr.notice('Opened smart proxy port '+this.port+' for '+this.country);
    this.proxy_time_series = new stats.ewma_series(date.ms.MIN);
    this.agent_proxy = new agent_proxy(this.country);
    this.agent_proxy.use();
    this.peer_proxy = new agent_proxy(this.country+'.peer');
    this.peer_proxy.use();
    if (unblocker_params.enable_proxy_rules)
    {
        this.pool_proxy = new agent_proxy(this.country+'.pool');
        this.pool_proxy.use();
    }
    this.server.on('request', this.onrequest.bind(this));
    this.server.on('connect', this.onconnect.bind(this));
    this.server.on('upgrade', this.onupgrade.bind(this));
E.get = function(country){
    country = country.toUpperCase();
    var proxy = smart_proxies[country];
    if (!proxy)
        smart_proxies[country] = proxy = new smart_proxy(country);
    return proxy;
E.reset = function(){ smart_proxies = {}; };
E.change_agent = function(country){
    country = country.toUpperCase();
    var proxy = smart_proxies[country];
    if (!proxy)
        return;
    proxy.agent_proxy.blacklist_current();
    proxy.peer_proxy.blacklist_current();
    if (proxy.pool_proxy)
        proxy.pool_proxy.blacklist_current();
    smart_proxies[country] = undefined;
E.purge_stats = function(){
    for (var country in smart_proxies)
        smart_proxies[country].purge_stats();
E.last_req_ts = function(){
    var last_req_ts = 0;
    for (var country in smart_proxies)
    {
        if (smart_proxies[country].get_last_req_ts()>last_req_ts)
            last_req_ts = smart_proxies[country].get_last_req_ts();
    }
    return last_req_ts;
E.is_operational = function(country){
    country = country.toUpperCase();
    var proxy = smart_proxies[country];
    if (!proxy)
    {
        // Not initialized yet, return true to proceed
        return true;
    }
    return proxy.agent_proxy.is_operational();
E.set_root_url_for_uid = function(uid, root_url){
    uid_to_root_url_map[uid] = root_url; };
smart_proxy.prototype.browser_root = function(){
    return E.browser_root ? zurl.get_root_domain(E.browser_root) : undefined;
smart_proxy.prototype.onrequest = function(req, res){
    var id = ++last_id;
    try {
        var incoming = new incoming_request(this, id, req, res);
        if (this.req_stats_cb && req.socket.zsvc_rdr_info)
        {
            this.req_stats_cb(req.socket.zsvc_rdr_info.uid,
                incoming.url.hostname);
        }
        incoming.run();
    } catch(e){
        zerr.err(id+'bp> '+(e instanceof TypeError ? e.stack : e));
        res.statusCode = e.code || 502;
        res.end(e.toString());
    }
smart_proxy.prototype.onupgrade = function(req, socket, head){
    var _this = this, __this;
    zerr.notice('XXX upgrade');
    if (req.method!='GET' || req.headers.upgrade!='websocket')
        return socket.destroy();
    var client, client_socket;
    var id = ++last_id;
    var rdr_info = socket.zsvc_rdr_info;
    var url = zurl.parse('wss://'
        +(rdr_info ? rdr_info.ip+':'+rdr_info.port : req.url), true);
    var strategy;
    var proxy, proxy_subreq, remote_role;
    const unblocker_params = local_storage.get('unblocker_params', {});
    return etask('smart_proxy.onupgrade', [function(){
        __this = this;
        if (unblocker_params.enable_proxy_rules)
        {
            const hro = {
                top_url: rdr_info ? undefined : url,
                root_url: _this.uid2root_url(rdr_info),
                method: 'GET',
                country: _this.country, // XXX nikita: rm
                proxy_country: _this.country,
                src_country: unblocker_params.country.toUpperCase(),
            };
            strategy = unblocker_lib.handle_request(url, hro);
            zerr.notice('unblocker_lib.handle_request('
                +`'${url.orig}', ${JSON.stringify(hro)}) = `
                +`'${strategy.desc}'`);
        }
        else
        {
            strategy = unblocker_lib.handle_request(url, {method: 'GET',
                country: _this.country});
        }
        zerr.debug(id+'p> ('+strategy.desc+') '+_this.country+'/'
            +(rdr_info ? 'app' : 'proxy')+' Websocket '+url.host);
    }, function loop(){
        var cmd = strategy(null, proxy_subreq)||{};
        if (unblocker_params.enable_proxy_rules)
        {
            proxy = cmd.proxy && cmd.proxy.start &&
                (cmd.proxy.peer ? _this.peer_proxy :
                cmd.proxy.pool ? _this.pool_proxy : _this.agent_proxy);
        }
        else
        {
            proxy = cmd.proxy && cmd.proxy.start &&
                (cmd.proxy.peer ? _this.peer_proxy : _this.agent_proxy);
        }
        remote_role = proxy ? 'a' : 'w';
        if (proxy)
            return proxy.use();
    }, function(){
        if (proxy || rdr_info && rdr_info.port==80)
        {
            zerr.debug(id+'p'+remote_role+'> '
                +(proxy ? 'via '+proxy.protocol+'://'+proxy.server+':'
                +proxy.port : 'direct'));
            var headers = proxy ? {
                'x-hola-version': agent_proxy.version_header,
                'proxy-authorization': proxy.basic,
                'user-agent': user_agent,
            } : {};
            if (proxy.jwt&&proxy.jwt.jwt)
                headers['x-hola-jwt'] = proxy.jwt.jwt;
            var params = {
                host: proxy ? proxy.ip : rdr_info.ip,
                port: proxy ? proxy.port : rdr_info.port,
                method: 'CONNECT',
                path: url.host,
                headers: headers,
            };
            if (proxy && proxy.protocol=='https')
                params.servername = proxy.server;
            client = engines[proxy ? proxy.protocol : 'http'].request(params);
            client.on('connect', function(res, _client_socket){
                client_socket = _client_socket;
                __this.continue(res);
            });
            client.on('response', this.continue_fn());
            client.on('error', this.throw_fn());
            client.end();
        }
        else
        {
            zerr.debug(id+'p'+remote_role+'> direct');
            client_socket = net.connect(url.port,
                rdr_info ? rdr_info.ip : url.hostname,
                this.continue_fn());
            client_socket.on('error', this.throw_fn());
        }
        (req||socket).on('error', this.throw_fn());
        return this.wait();
    }, function(res){
        var policy = proxy && res && res.headers['x-hola-policy'];
        if (policy)
        {
            zerr.debug(id+'up'+remote_role+'< '+res.statusCode+' '+policy);
            proxy_subreq = {code: res.code, policy: policy};
            return this.goto('loop');
        }
        if (!client_socket)
            throw new Error(res ? ''+res.code : 'Connection failed');
        zerr.debug(id+'up'+remote_role+'< connected');
        var hdrs = http_hdr.restore_case(req.headers, req.rawHeaders);
        client_socket.write('GET '+url.path+' HTTP/1.1\r\n');
        client_socket.write(Object.keys(hdrs).map(function(key){
            var value = hdrs[key];
            return !Array.isArray(value) ? key+': '+value :
                value.map(function(val){ return key+': '+val; }).join('\r\n');
        }).join('\r\n')+'\r\n\r\n');
        socket.pipe(client_socket);
        client_socket.pipe(socket);
        socket.on('error', this.throw_fn());
        client_socket.on('end', __this.continue_fn());
        client_socket.on('error', this.throw_fn());
        return this.wait();
    }, function(){
        zerr.debug(id+'p'+remote_role+' done');
    }, function catch$(err){
        zerr.err(id+'p'+remote_role+' '
            +(err instanceof TypeError ? err.stack : err));
        if (client)
            client.abort();
        if (client_socket)
            client_socket.destroy();
        socket.destroy();
    }]);
smart_proxy.prototype.onconnect = function(req, socket){
    var _this = this, __this;
    var client, client_socket;
    var id = ++last_id;
    var rdr_info = socket.zsvc_rdr_info;
    var url = zurl.parse('https://'
        +(rdr_info ? rdr_info.ip+':'+rdr_info.port : req.url), true);
    var strategy;
    var proxy, proxy_subreq, remote_role, tls_handshake;
    const unblocker_params = local_storage.get('unblocker_params', {});
    return etask('smart_proxy.onconnect', [function(){
        __this = this;
        if (!req)
        {
            socket.once('data', this.continue_fn());
            return this.wait();
        }
    }, function handshake(packet){
        if (packet)
        {
            socket.pause();
            if (!tls_handshake)
                tls_handshake = new ztls.Handshake();
            var res = tls_handshake.extract_sni(packet);
            switch (res.status)
            {
            case 'truncated':
                socket.once('data', this.goto_fn('handshake'));
                return this.wait();
            case 'ok':
                url = zurl.parse('https://'+res.hostname+':'+rdr_info.port,
                    true);
                if (_this.req_stats_cb)
                    _this.req_stats_cb(rdr_info.uid, res.hostname);
                break;
            case 'err':
                zerr.warn(id+'bp> TLS handshake parse error: '+res.message);
                break;
            }
        }
    }, function(){
        if (unblocker_params.enable_proxy_rules)
        {
            const hro = {
                top_url: rdr_info ? undefined : url,
                root_url: _this.uid2root_url(rdr_info),
                method: 'CONNECT',
                country: _this.country, // XXX nikita: rm
                proxy_country: _this.country,
                src_country: unblocker_params.country.toUpperCase(),
            };
            strategy = unblocker_lib.handle_request(url, hro);
            zerr.notice('unblocker_lib.handle_request('
                +`'${url.orig}', ${JSON.stringify(hro)}) = `
                +`'${strategy.desc}'`);
        }
        else
        {
            strategy = unblocker_lib.handle_request(url, {method: 'CONNECT',
                country: _this.country});
        }
        zerr.debug(id+'bp> ('+strategy.desc+') '+_this.country+'/'
            +(rdr_info ? 'app' : 'proxy')+' CONNECT '+url.host);
    }, function loop(){
        var cmd = strategy(null, proxy_subreq)||{};
        if (unblocker_params.enable_proxy_rules)
        {
            proxy = cmd.proxy && cmd.proxy.start &&
                (cmd.proxy.peer ? _this.peer_proxy :
                cmd.proxy.pool ? _this.pool_proxy : _this.agent_proxy);
        }
        else
        {
            proxy = cmd.proxy && cmd.proxy.start &&
                (cmd.proxy.peer ? _this.peer_proxy : _this.agent_proxy);
        }
        remote_role = proxy ? 'a' : 'w';
        if (proxy)
            return proxy.use();
    }, function(){
        if (proxy || rdr_info && rdr_info.port==80)
        {
            zerr.debug(id+'p'+remote_role+'> '
                +(proxy ? 'via '+proxy.protocol+'://'+proxy.server+':'
                +proxy.port : 'direct'));
            var headers = proxy ? {
                'x-hola-version': agent_proxy.version_header,
                'proxy-authorization': proxy.basic,
                'user-agent': user_agent,
            } : {};
            if (proxy.jwt&&proxy.jwt.jwt)
                headers['x-hola-jwt'] = proxy.jwt.jwt;
            var params = {
                host: proxy ? proxy.ip : rdr_info.ip,
                port: proxy ? proxy.port : rdr_info.port,
                method: 'CONNECT',
                path: url.host,
                headers: headers,
            };
            if (proxy && proxy.protocol=='https')
                params.servername = proxy.server;
            client = engines[proxy ? proxy.protocol : 'http'].request(params);
            client.on('connect', function(res, _client_socket){
                client_socket = _client_socket;
                __this.continue(res);
            });
            client.on('response', this.continue_fn());
            client.on('error', this.throw_fn());
            client.end();
        }
        else
        {
            zerr.debug(id+'p'+remote_role+'> direct');
            client_socket = net.connect(url.port,
                rdr_info ? rdr_info.ip : url.hostname,
                this.continue_fn());
            client_socket.on('error', this.throw_fn());
        }
        (req||socket).on('error', this.throw_fn());
        return this.wait();
    }, function(res){
        var policy = proxy && res && res.headers['x-hola-policy'];
        if (policy)
        {
            zerr.debug(id+'p'+remote_role+'< '+res.statusCode+' '+policy);
            proxy_subreq = {code: res.code, policy: policy};
            return this.goto('loop');
        }
        if (!client_socket)
            throw new Error(res ? ''+res.code : 'Connection failed');
        zerr.debug(id+'p'+remote_role+'< connected');
        if (req)
        {
            socket.write('HTTP/'+req.httpVersion
                +' 200 Connection Established\r\n\r\n');
        }
        if (tls_handshake)
        {
            client_socket.write(tls_handshake.client.buf);
            tls_handshake = null;
        }
        socket.pipe(client_socket);
        client_socket.pipe(socket);
        socket.on('error', this.throw_fn());
        client_socket.on('end', this.continue_fn());
        client_socket.on('error', this.throw_fn());
        return this.wait();
    }, function(){
        zerr.debug(id+'p'+remote_role+' done');
    }, function catch$(err){
        zerr.err(id+'p'+remote_role+' '
            +(err instanceof TypeError ? err.stack : err));
        if (client)
            client.abort();
        if (client_socket)
            client_socket.destroy();
        socket.destroy();
    }]);
smart_proxy.prototype.intercept = function(socket){
    if (socket.zsvc_rdr_info.port==80)
        this.server.emit('connection', socket);
    else
        this.onconnect(null, socket);
smart_proxy.prototype.report_proxy_time = function(time){
    this.proxy_time_series.append(time, Date.now());
    zerr.info(this.country+' proxy response time '+time+'ms, smoothed average '
        +this.get_avg_proxy_time()+'ms');
smart_proxy.prototype.get_avg_proxy_time = function(){
    return this.proxy_time_series.get(); };
smart_proxy.prototype.purge_stats = function(){
    this.proxy_time_series.reset(); };
smart_proxy.prototype.set_req_stats_cb = function(cb){
    this.req_stats_cb = cb;
smart_proxy.prototype.uid2root_url = function(rdr_info){
    if (rdr_info)
        return uid_to_root_url_map[rdr_info.uid];
    // XXX sergey: check if this really browser
    if (!E.browser_root)
        return;
    let url = zurl.parse(E.browser_root);
    return zurl.get_root_domain(url.hostname);
