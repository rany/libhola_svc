// LICENSE_CODE ZON
'use strict'; /*jslint node:true*/
const zconf = require('../../util/config.js');
const date = require('../../util/date.js');
const etask = require('../../util/etask.js');
const wget = require('../../util/wget.js');
const zescape = require('../../util/escape.js');
const rand = require('../../util/rand.js');
const zerr = require('../../util/zerr.js');
const zutil = require('../../util/util.js');
const unblocker_lib = require('../vpn/pub/unblocker_lib.js');
const embed_perr = require('../embed_perr.js');
const local_storage = require('../local_storage.js');
const pcountries = require('../../protocol/pub/countries.js');
const crypto = require('crypto');
const E = module.exports = agent_proxy, assign = Object.assign;
const AGENT_TTL = 5*date.ms.MIN;
let paid_mode = false;
let product;
let blacklist_agents = {};
// XXX nikita: generate automatically like for ext/svc
const fallback_agents = [
    {host: 'zagent43.hola.org', ip: '207.148.24.201', port: 25500},
    {host: 'zagent49.hola.org', ip: '161.35.108.242', port: 25500},
function agent_proxy(country){
    this.country = country;
    this.server = null;
    this.ip = null;
    this.port = null;
    this.credentials = null;
    this.basic = null;
    this.ts = null;
    this.sp = null;
    this.pending = [];
    this.exclude = [];
// XXX: replace with some kind of config variable when available
// (currently, the smart proxy only runs on Android)
E.os = 'android';
E.version_header = zconf.ZON_VERSION+' '+E.os;
let unblocker_lib_inited;
const init_unblocker_lib = ()=>{
    if (unblocker_lib_inited)
        return;
    zerr.notice('unblocker_lib init');
    unblocker_lib.init({
        perr: opt=>embed_perr.perr(opt.id, zutil.omit(opt, ['id'])),
        ajax: opt=>etask(function*(){
            let method = opt.method||'GET';
            let qs = opt.qs;
            if (method=='GET')
                assign(qs, opt.data);
            let uri = zescape.uri(opt.url, qs);
            let headers = {};
            const ul_store = local_storage.get('unblocker_lib', {});
            if (ul_store.jwt)
                headers['x-hola-jwt'] = ul_store.jwt;
            let res = yield wget({
                url: uri,
                method: method,
                timeout: opt.timeout,
                body: method=='POST' ? opt.data : undefined,
                json: true,
                expect: 200,
                retry: opt.retry,
                headers,
            });
            return res.resp.body;
        }),
        storage: {
            get: id=>{
                const ul_store = local_storage.get('unblocker_lib', {});
                return ul_store[id];
            },
            set: (id, val)=>{
                let ul_store = local_storage.get('unblocker_lib', {});
                ul_store[id] = val;
                local_storage.put('unblocker_lib', ul_store);
            },
        },
        conf_get: (id, def)=>{
            const vp_store = local_storage.get('verify_proxy_conf', {});
            return vp_store[id]||def;
        },
        get_auth: ()=>{
            const ul_store = local_storage.get('unblocker_lib', {});
            let auth_info = {
                uuid: ul_store.uuid,
                session_key: ul_store.session_key,
                apk_ver: zconf.ZON_VERSION,
            };
            if (paid_mode)
            {
                assign(auth_info, {identity: sh256_hmac(ul_store.uuid,
                    'true')});
            }
            if (product)
                auth_info.product = product;
            return auth_info;
        },
        get_prod_info: ()=>({product: 'android', svc_ver: zconf.ZON_VERSION}),
    });
    unblocker_lib_inited = true;
E.set_auth_info = function(_uuid, _session_key, _jwt){
    zerr.notice('set_auth_info '+_uuid+' '+_session_key+' '+_jwt);
    let ul_store = local_storage.get('unblocker_lib', {});
    ul_store.uuid = _uuid;
    if (_session_key.length!=0)
        ul_store.session_key = _session_key;
    ul_store.jwt = _jwt;
    local_storage.put('unblocker_lib', ul_store);
    init_unblocker_lib();
E.set_paid_mode = function(mode){
    if (mode==paid_mode)
        return;
    paid_mode = !!mode;
    unblocker_lib.set_user_plus(paid_mode);
    unblocker_lib.reset();
E.is_paid_mode = function(){ return paid_mode; };
E.set_product = function(prod){ product = prod; };
E.prototype.use = function(){
    let _this = this;
    if (this.server && this.ts && Date.now()-this.ts<=AGENT_TTL && !this.sp)
        return;
    return etask('agent_proxy.use', [function(){
        if (_this.sp)
        {
            _this.pending.push(this);
            return this.wait();
        }
        return _this.find_agent();
    }]);
const sh256_hmac = (_uuid, msg)=>{
    _uuid = _uuid.replace(/^apk[-p]*/, '').toLowerCase();
    let hmac = crypto.createHmac('sha256', _uuid);
    hmac.update(msg);
    return hmac.digest('hex');
const get_fallback_agent = country=>{
    let res = {verify_proxy: {}};
    const rr = country.match(/^(..)(\.peer)?$/);
    if (!rr)
        return res;
    const c = rr[1];
    let agent = assign({}, rand.rand_element(fallback_agents));
    agent.port += (pcountries.ports[c]||{}).port||0;
    res.verify_proxy[country] = [{agent}];
    return res;
// XXX arik/sergeir: we need generic way to print circular objects.
// find generic library for it and implement it in zerr.json (+ test)
// eg. https://github.com/WebReflection/circular-json
const json_to_str = _obj=>{
    // Same as JSON.stringify, but prints up to 5th level to avoid crashing on
    // circular structures
    function pp(obj, level){
        if (level>5)
            return '[...]';
        if (obj instanceof String || obj instanceof Date)
            return `"${obj}"`;
        if (Array.isArray(obj))
        {
            let ret = '[';
            for (let i=0; i<obj.length; i++)
                ret += pp(obj[i], level+1)+', ';
            ret = ret.substring(0, ret.length-2);
            ret += ']';
            return ret;
        }
        if (obj instanceof Object)
        {
            let ret = [];
            for (let e in obj)
                ret.push(`"${e}": ${pp(obj[e], level+1)}`);
            return `{${ret.join(', ')}}`;
        }
        return `"${''+obj}"`;
    }
    return pp(_obj, 0);
E.prototype.find_agent = function(){
    const country = this.country.toLowerCase();
    const unblocker_params = local_storage.get('unblocker_params', {});
    let _this = this;
    return this.sp = etask('agent_proxy.get_agent', function*(){
        try {
            if (!unblocker_lib_inited)
                throw new Error('no auth data available');
            let res;
            let pool = paid_mode && !country.match(/^..\.peer$/);
            if (unblocker_params.enable_proxy_rules)
                pool = undefined;
            if (blacklist_agents[country])
            {
                res = yield unblocker_lib.replace_agents({country, pool},
                    blacklist_agents[country], {user_not_working: true});
                const err = res.info && res.info.error ? res.info : res;
                if (err.error)
                {
                    zerr.err('replace_agents() failed: '+err.err);
                    embed_perr.perr('get_agents_failed', err);
                    res.verify_proxy = [];
                }
                zerr.notice('replace_agents(): '
                    +json_to_str(res.verify_proxy)+' replacing: '
                    +json_to_str(blacklist_agents[country]));
            }
            else
            {
                res = yield unblocker_lib.get_agents(null, {country, pool});
                if (!res)
                    res = {error: 'empty res'};
                const err = res.info && res.info.error ? res.info : res;
                if (err.error)
                {
                    zerr.err('get_agents() failed: '+err.err);
                    embed_perr.perr('get_agents_failed', err);
                    res.verify_proxy = [];
                }
                zerr.notice('get_agents(): '+json_to_str(res.verify_proxy));
            }
            const ul_store = local_storage.get('unblocker_lib', {});
            let proxies = res.verify_proxy[country+(pool ? '.pool' : '')];
            // Fallback to normal agents, if there is no pool agents
            // available
            if ((!proxies || !proxies.length) && pool)
                proxies = res.verify_proxy[country];
            if (!proxies || !proxies.length)
            {
                embed_perr.perr('get_agents_empty', {
                    country,
                    uuid: ul_store.uuid,
                    session_key: ul_store.session_key,
                    info: res.info,
                });
                res = get_fallback_agent(country);
                proxies = res.verify_proxy[country];
                if (!proxies || !proxies.length)
                    throw new Error('No servers available');
            }
            blacklist_agents[country] = undefined;
            const proxy = proxies[0].agent;
            _this.server = proxy.host;
            _this.ip = proxy.ip;
            _this.port = proxy.port;
            _this.protocol = 'http';
            const agent_key = ul_store.agent_key;
            if (!agent_key)
            {
                embed_perr.perr('no_agent_key', {country, info: res.info});
                throw 'no_agent_key';
            }
            _this.credentials = 'user-uuid-'+ul_store.uuid+':'+
                (paid_mode ? sh256_hmac(ul_store.uuid, 'true') : agent_key);
            _this.basic = 'Basic '+Buffer.from(_this.credentials)
                .toString('base64');
            for (let i in _this.pending)
                _this.pending[i].continue();
            _this.pending = [];
        } catch(e){
            let error = zerr.e2s(e);
            _this.server = _this.ip = _this.port = null;
            _this.credentials = _this.basic = null;
            zerr.err('No agents for '+_this.country+': '+error);
            embed_perr.perr('get_agents_exception', {
                country: _this.country, error});
            for (let i in _this.pending)
                _this.pending[i].throw(e);
            _this.pending = [];
            this.throw(e);
        } finally {
            _this.sp = null;
        }
    });
E.prototype.report_response = function(code){
    if (code>=200 && code<300 || code==304)
        this.ts = Date.now();
E.prototype.report_error = function(){
    this.ts = null; // force agent verification
    this.use();
E.prototype.probe = function(url){
    let _this = this;
    return etask('agent_proxy.probe', function*(){
        yield _this.use();
        const res = yield wget(
            zescape.uri('http://'+_this.ip+':'+_this.port+'/probe',
                {url: url.orig, country: _this.country}),
            {expect: 200, json: true, slow: 0});
        return res.body.type;
    });
E.prototype.blacklist_current = function(){
    const country = this.country.toLowerCase();
    if (!blacklist_agents[country])
        blacklist_agents[country] = [];
    blacklist_agents[country].push({host: this.server, ip: this.ip,
        port: this.port});
E.prototype.is_operational = function(){
    return this.server && this.ip && this.port && this.credentials
        && this.basic;
