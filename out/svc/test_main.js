// LICENSE_CODE ZON
'use strict'; /*jslint node:true*/
const date = require('../util/date.js');
const etask = require('../util/etask.js');
// Main embedded Node script for testing. Can be removed once main.js is
// ready to be run in unit tests in embedded Node.
if (process.env.ZNODE_DEP_SCAN)
    let dep_scan = require('../util/dep_scan.js');
    process.stdout.write(JSON.stringify(dep_scan()));
    process.exit(0);
let e = etask.interval(10*date.ms.SEC, ()=>console.log('Test timer fired'));
process.on('SIGTERM', ()=>{
    e.return();
