// LICENSE_CODE ZON
'use strict'; /*jslint node:true*/
require('../util/config.js');
const etask = require('../util/etask.js');
const efile = require('../util/efile.js');
const zerr = require('../util/zerr.js');
const zutil = require('../util/util.js');
const E = exports;
const DATA_FILE_NAME = 'local_storage.json';
let confdir;
let data = {}, data_sp, data_pending = [];
const dump_local_storage = ()=>{
    zerr.notice('{');
    for (let i in data)
    {
        const str = JSON.stringify(data[i]);
        zerr.notice(`    "${i}": ${str},`);
    }
    zerr.notice('}');
/* eslint-disable no-unmodified-loop-condition */
const load_data = ()=>etask(function*_load_data(){
    const load_data_internal = ()=>etask(function*_load_data_internal(){
        const fname = `${confdir}/${DATA_FILE_NAME}`;
        if (!confdir || !(yield efile.exists(fname)))
        {
            for (let i in data_pending)
                data_pending[i].continue();
            data_sp = undefined;
            data_pending = [];
            return;
        }
        const size = yield efile.size_e(fname);
        let fd;
        try {
            fd = yield efile.open_e(fname, 'r');
            let str = yield efile.fread_e(fd, 0, size, {encoding: 'utf8'});
            let _data = JSON.parse(str);
            // Keep, in priority, the data already set before load have
            // finished
            data = zutil.union_with((orig_val, new_val)=>orig_val,
                data, _data);
            zerr.notice('Loaded local storage data: ');
            dump_local_storage();
        } catch(e){
            zerr.err('Unable to read local storage data from '
                +`${confdir}/${DATA_FILE_NAME}: `+e);
        } finally {
            if (fd)
                yield efile.close_e(fd);
            for (let i in data_pending)
                data_pending[i].continue();
            data_sp = undefined;
            data_pending = [];
        }
    });
    while (data_sp)
    {
        data_pending.push(this);
        yield this.wait();
    }
    data_sp = load_data_internal();
    yield data_sp;
const save_data = ()=>etask(function*_save_data(){
    const save_data_internal = ()=>etask(function*_sava_data_internal(){
        const fname = `${confdir}/${DATA_FILE_NAME}`;
        if (!confdir)
        {
            for (let i in data_pending)
                data_pending[i].continue();
            data_sp = undefined;
            data_pending = [];
            return;
        }
        try {
            let str = JSON.stringify(data);
            yield efile.write_e(fname, str, {rm_rf: true});
        } catch(e){
            zerr.err('Unable to write local storage data to '
                +`${confdir}/${DATA_FILE_NAME}: `+e);
        } finally {
            for (let i in data_pending)
                data_pending[i].continue();
            data_sp = undefined;
            data_pending = [];
        }
    });
    while (data_sp)
    {
        data_pending.push(this);
        yield this.wait();
    }
    data_sp = save_data_internal();
    yield data_sp;
/* eslint-enable no-unmodified-loop-condition */
E.set_confdir = _confdir=>{
    confdir = _confdir;
    load_data();
E.get = (name, def_value)=>data[name]||def_value;
E.put = (name, value)=>{
    data[name] = zutil.clone_deep(value);
    save_data();
