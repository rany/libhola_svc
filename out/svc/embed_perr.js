// LICENSE_CODE ZON
'use strict'; /*jslint node:true*/
require('../util/config.js');
const is_android =
    ({linux: 'android'}[process.platform]||process.platform)=='android';
const E = module.exports;
let pending_perrs = [];
E.perr = function(name, info){
    // embed_perr.js is only needed for android app
    if (!is_android)
        return;
    pending_perrs.push({name, info});
E.get_pending_perrs = function(){
    const ret = JSON.stringify(pending_perrs);
    pending_perrs = [];
    return ret;
