// LICENSE_CODE ZON
'use strict'; /*jslint browser:true, node:true, es6:true*/
(function(){
let define;
const is_node = typeof module=='object' && module.exports && module.children;
var is_rn = typeof global=='object' && !!global.nativeRequire ||
    typeof navigator=='object' && navigator.product=='ReactNative';
if (is_rn)
    define = require('../../../util/require_node.js').define(module, '../',
        require('/util/sprintf.js'), require('/util/util.js'),
        require('/util/url.js'), require('/util/version_util.js'));
else if (!is_node)
    define = self.define;
else
    define = require('../../../util/require_node.js').define(module, '../');
define(['/util/sprintf.js', '/util/util.js', '/util/url.js',
    '/util/version_util.js'], function(sprintf, zutil, zurl, version_util)
const E = {};
const assign = Object.assign;
E.get_root_domain = zurl.get_root_domain;
E.get_root_url = url=>{
    const n = (url||'').match(/^https?:\/\/([^\/]+)(\/.*)?$/);
    if (!n)
        return null;
    return E.get_root_domain(n[1]);
E.find_rule = (rules, opt)=>{
    if (!rules)
        return;
    for (let i in rules)
    {
        const r = rules[i];
        if (opt.name==r.name
            && (opt.type===undefined || opt.type==r.type)
            && (opt.md5===undefined || opt.md5==r.md5)
            && (opt.country===undefined
            || opt.country.toLowerCase()==r.country.toLowerCase()))
        {
            return r;
        }
    }
    return null;
E.gen_route_str = (route_opt, opt)=>{
    opt = opt||{};
    if (route_opt.direct)
        return 'direct';
    let s = route_opt.country.toLowerCase(), r = [];
    if (route_opt.peer)
        r.push('peer');
    if (route_opt.pool)
    {
        r.push('pool'+(typeof route_opt.pool=='string' ?
            '_'+route_opt.pool : ''));
    }
    if (!opt.no_algo && route_opt.algo)
        r.push(route_opt.algo);
    if (r.length)
        s += '.'+r.join(',');
    return s;
function ip_list_to_agents(ip_list){
    return fix_zagent(Object.keys(ip_list).join(','));
function fix_zagent(s){
    return s.replace(/.hola.org/g, '').replace(/zagent/g, 'za');
// XXX arik: mv to util?
function pad(num, size){ return ('000'+num).slice(-size); }
function time_str(d){
    d = new Date(d);
    if (isNaN(d))
        return '00:00:00.000';
    return pad(d.getUTCHours(), 2)+':'+pad(d.getUTCMinutes(), 2)
    +':'+pad(d.getUTCSeconds(), 2)
    +'.'+pad(d.getUTCMilliseconds(), 3);
E.events_enabled = false;
E.events = [];
E.events_n = {};
E.reset_events = ()=>{
    E.events = [];
    E.events_n = {};
E.push_event = (name, opt)=>{
    try {
        if (!E.events_enabled)
            return;
        if (E.events.length > 512)
            E.events.splice(0, E.events.length/2);
        E.events.push({name, ts: Date.now(), opt});
        E.events_n[name] = (E.events_n[name]||0)+1;
    } catch(err){ console.error('push_event error %s', err&&err.stack); }
E.enable_events_logging = enabled=>{
    E.events_enabled = !!enabled;
    if (!enabled)
        E.reset_events();
E.get_event_n = e=>(E.events_n[e]);
E.events_to_str = ()=>{
    let s = '', a =[];
    E.events.forEach(e=>{
        let s = time_str(e.ts)+' '+e.name, o = e.opt;
        switch (e.name)
        {
        case '>zgettunnels':
            s += sprintf(' %s%s', o.country,
                (o.exclude||[]).length ? ' exclude '+
                o.exclude.map(e=>fix_zagent(e)).join(',') : '');
            break;
        case '<zgettunnels':
            s += sprintf(' %s%s', o.country,
                ' agents '+ip_list_to_agents(o.ip_list));
            break;
        case 'set_rule':
            s += sprintf(' %s%s%s%s%s', o.name,
                o.enabled ? ' '+o.country : ' off',
                o.del ? ' del' : '',
                o.src ? ' src '+o.src : '',
                !o.full_vpn ? ' !full_vpn' : '',
                o.mode=='unblock' ? '' : ' mode '+o.mode);
            break;
        case '<verify_err':
            s += sprintf(' %s %s', o.host ? fix_zagent(o.host) : o.ip,
                ' '+o.err);
            break;
        case '<verify':
            s += sprintf(' %s %s', o.host ? fix_zagent(o.host) : o.ip,
                o.rtt ?o.rtt+'ms' : '!rtt');
            break;
        case '>verify':
            s += sprintf(' %s', o.host ? fix_zagent(o.host) : o.ip);
            break;
        case '>stuck':
        case '>stuck_long':
           break;
        case '<stuck_end':
            s += sprintf(' %sms', o.duration);
           break;
        case 'pac_miss':
            s += sprintf(' %s!=%s %s', o.req_ip, o.agent_ip, o.url);
            break;
        case 'pac_reload':
            s += sprintf(' after %ssec', (o.new_ts - o.init_ts)/1000);
            break;
        case 'navgiate': s += sprintf(' tab %s %s', o.tab_id, o.url); break;
        case 'popup_render':
        case 'tpopup_render':
            break;
        case 'pac_slow':
            s += sprintf(' %dms %s', o.dur, (o.url||'').substr(0, 50));
            break;
        default: s += JSON.stringify(o)||''; break;
        }
        a.push(s);
    });
    a = a.reverse();
    return a.join('\n');
class Domain_tree {
    constructor(){
        this.clear();
    }
    *[Symbol.iterator](){
        let parts = [];
        for (let item of Domain_tree.iterate_subtree(parts, this.tree))
            yield item;
    }
    clear(){
        this.tree = {};
    }
    get_value(domain){
        if (!domain)
            return;
        let parts = domain.split('.');
        let leaf = this.tree, value = leaf[Domain_tree.VALUE];
        for (let i = parts.length-1; leaf && i>=0; i--)
        {
            leaf = leaf[parts[i]];
            value = leaf && (Domain_tree.VALUE in leaf) ?
                leaf[Domain_tree.VALUE] : value;
        }
        return value;
    }
    set_value(domain, value){
        if (!domain)
            return void (this.tree[Domain_tree.VALUE] = value);
        let parts = domain.split('.');
        let leaf = this.tree;
        for (let i = parts.length-1; i>=0; i--)
            leaf = leaf[parts[i]] = leaf[parts[i]]||{};
        leaf[Domain_tree.VALUE] = value;
    }
Domain_tree.VALUE = Symbol.for('value');
Domain_tree.iterate_subtree = function*(parts, leaf){
    if (Domain_tree.VALUE in leaf)
        yield [parts.slice().reverse().join('.'), leaf[Domain_tree.VALUE]];
    for (let key in leaf)
    {
        parts.push(key);
        for (let item of Domain_tree.iterate_subtree(parts, leaf[key]))
            yield item;
        parts.pop();
    }
E.Domain_tree = Domain_tree;
E.get_site_key = (conf, ext_opt, root_url)=>{
    let {ext_ver} = ext_opt;
    let sites = conf && conf.sites || {};
    return Object.keys(sites).find(k=>{
        let v = sites[k];
        let urls = Array.isArray(v.root_url) ? v.root_url : [v.root_url];
        return urls.includes(root_url) && E.check_min_ver(ext_ver, v.min_ver);
    });
const get_match_fn = operator=>{
    if (operator=='gt')
        return (a, b)=>a>b;
    if (operator=='gte')
        return (a, b)=>a>=b;
    if (operator=='lt')
        return (a, b)=>a<b;
    if (operator=='lte')
        return (a, b)=>a<=b;
    if (operator=='in')
        return (a, b)=>Array.isArray(b) && b.includes(a);
    return (a, b)=>a==b;
E.match_filter = (query, filter)=>{
    if (!filter)
        return true;
    if (!query)
        return false;
    query = assign({}, query);
    for (let key in query)
    {
        if (key.endsWith('_ts') && query.now)
            query['since_'+key] = query.now-query[key];
    }
    for (let f_key in filter)
    {
        let m = f_key.match(/^(.*)_(gt|gte|lt|lte|in)$/);
        let q_key = m ? m[1] : f_key;
        let operator = m ? m[2] : 'eq';
        // fix for filters like {signed_in: true}
        if (operator=='in' && !Array.isArray(filter[f_key]))
        {
            q_key = f_key;
            operator = 'eq';
        }
        let op1, op2;
        if (q_key.endsWith('_ver') && operator!='in')
        {
            op1 = version_util.cmp(query[q_key], filter[f_key]);
            op2 = 0;
        }
        else
        {
            op1 = query[q_key];
            op2 = filter[f_key];
        }
        if (!get_match_fn(operator)(op1, op2))
            return false;
    }
    return true;
const apply_conf_override = (site_conf, ext_opt)=>{
    let {ext_ver, install_ver, install_ts} = ext_opt;
    let {override} = site_conf;
    site_conf = zutil.omit(site_conf, ['override']);
    if (!override)
        return site_conf;
    let versions = Object.keys(override).sort((a, b)=>
        -version_util.cmp(a, b));
    let ver = versions.find(v=>E.check_min_ver(ext_ver, v) &&
        E.check_min_install_ver(install_ver, override[v].install_min_ver) &&
        E.check_min_install_ts(install_ts, override[v].install_min_ts)
    );
    if (ver)
    {
        site_conf = zutil.extend_deep(zutil.clone_deep(site_conf),
            zutil.omit(override[ver], ['install_min_ver', 'install_min_ts']));
    }
    return site_conf;
const apply_trial_conf_override = (tconf, ext_opt)=>{
    let {override} = tconf;
    tconf = zutil.omit(tconf, ['override']);
    if (!override || !ext_opt)
        return tconf;
    let {ext_ver, trial_state, now} = ext_opt;
    let first_start_ts = trial_state && trial_state.first_start_ts;
    // old ext verssions do not support override
    if (!first_start_ts || !now || version_util.cmp(ext_ver, '1.174.712')<0)
        return tconf;
    let match_opt = assign({first_start_ts}, ext_opt);
    delete match_opt.trial_state;
    // XXX andrey: for backward compat. New versions should use since_* syntax
    match_opt.first_start = now - first_start_ts;
    // find last match
    let ov = override.slice().reverse()
        .find(o=>E.match_filter(match_opt, o.filter));
    if (ov)
        tconf = zutil.extend_deep(zutil.clone_deep(tconf), ov.conf);
    return tconf;
E.get_all_sites_conf = (conf, ext_opt)=>{
    let {ext_ver, install_ver, install_ts} = ext_opt;
    let c = conf && conf.sites_default || {};
    if (!E.check_min_ver(ext_ver, c.min_ver) ||
        !E.check_min_install_ver(install_ver, c.install_min_ver) ||
        !E.check_min_install_ts(install_ts, c.install_min_ts))
    {
        return;
    }
    return apply_conf_override(c, ext_opt);
E.get_specific_site_conf = (conf, ext_opt, root_url)=>{
    let site_conf = zutil.get(conf && conf.sites, E.get_site_key(conf, ext_opt,
        root_url));
    if (site_conf)
        site_conf = apply_conf_override(site_conf, ext_opt);
    return site_conf;
E.get_site_conf = (conf, ext_opt, root_url, path, def)=>{
    let site_conf = root_url &&
        E.get_specific_site_conf(conf, ext_opt, root_url) ||
        E.get_all_sites_conf(conf, ext_opt);
    return path ? zutil.get(site_conf, path, def) : site_conf;
E.get_trial_conf = (conf, ext_opt, root_url, path, def)=>{
    let {ext_ver} = ext_opt;
    let tconf = E.get_site_conf(conf, ext_opt, root_url, 'trial');
    if (!tconf)
        return;
    let def_trial = zutil.get(E.get_all_sites_conf(conf, ext_opt), 'trial');
    if (tconf.all_sites_timer && !def_trial)
        tconf = zutil.omit(tconf, ['all_sites_timer']);
    // if all_sites_timer is enabled, we must use common trial conf for all
    // sites, but each site can have own skip_signin value
    if (tconf.all_sites_timer && def_trial)
        tconf = assign({}, tconf, zutil.omit(def_trial, ['skip_signin']));
    tconf = apply_trial_conf_override(tconf, ext_opt);
    let {time_ended_bonus} = tconf;
    if (time_ended_bonus && !E.check_min_ver(ext_ver,
        time_ended_bonus.min_ver))
    {
        tconf = zutil.omit(tconf, ['time_ended_bonus']);
    }
    return path ? zutil.get(tconf, path, def) : tconf;
E.check_min_ver = (ext_ver, min_ver)=>
    version_util.valid(min_ver) && version_util.cmp(ext_ver, min_ver)>=0;
E.check_min_install_ver = (install_ver, min_ver)=>!min_ver ||
    version_util.valid(min_ver) && version_util.cmp(install_ver, min_ver)>=0;
E.check_min_install_ts = (install_ts, ts)=>!ts || +install_ts>=+ts;
return E; }); }());
