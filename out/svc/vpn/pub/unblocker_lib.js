// LICENSE_CODE ZON
'use strict'; /*jslint node:true, browser:true, es6:true*/
(function(){
let define;
let is_node = typeof module=='object' && module.exports && module.children;
var is_rn = typeof global=='object' && !!global.nativeRequire ||
    typeof navigator=='object' && navigator.product=='ReactNative';
if (!is_node && !is_rn)
    define = self.define;
else if (is_rn)
    define = require('../../../util/require_node.js').define(module, '../',
        require('/util/date.js'), require('/util/browser.js'),
        require('/util/escape.js'), require('/util/url.js'),
        require('/util/util.js'), require('/util/zerr.js'),
        require('/util/etask.js'), require('/svc/vpn/pub/util.js'),
        require('/util/version_util.js'), require('/util/events.js'),
        require('/util/array.js'), require('/util/rate_limit.js'),
        require('/util/rand.js'));
else
    define = require('../../../util/require_node.js').define(module, '../');
define(['/util/date.js', '/util/browser.js', '/util/escape.js', '/util/url.js',
    '/util/util.js', '/util/zerr.js', '/util/etask.js', '/svc/vpn/pub/util.js',
    '/util/version_util.js', '/util/events.js', '/util/array.js',
    '/util/rate_limit.js', '/util/rand.js'],
    function(date, zbrowser, zescape, zurl, zutil, zerr, etask, svc_vpn_util,
    version_util, events, array, rate_limit, rand){
const E = new events.EventEmitter();
const assign = Object.assign, ms = date.ms;
const CACHE_TTL = ms.MIN, POLICY_TTL = 5*ms.MIN, SEC = ms.SEC;
const gen_ping_id = id=>id || (zutil.is_mocha() ? '1111' : Math.random());
const ctx = [];
let force_agent_changed;
// XXX ron: to fix
// update_chosen -> update on real usage
// These are the actual, up-to-date extension lists. Any similar extension list
// elsewhere is obsolete. -- alexey 2015-05-19
const extensions = {
    archive: zutil.bool_lookup(
        '7z apk arj bin bz deb dmg exe gz iso msi rar rpm tbz tgz xz zip'),
    media: zutil.bool_lookup(
        '3gp avi f4v flv h264 m4a m4s m4v mkv mov mp3 mp4 ogg ogv swf ts wav '
        +'webm wmv'),
    static: zutil.bool_lookup(
        'css eot gif ico jar jpeg jpg png svg svgz ttf webp woff'),
const expected_types = {
    archive: /^application\/(?!json$)/,
    media: /^((audio|video)\/|application\/octet-stream$)/,
const CG = (s, def)=>api.conf_get && api.conf_get(s, def);
let cache = {};
let peer_domain_re;
let user_plus = false;
let _app_trial_mode = false;
let active_reqs = [];
let conf_rand_id;
let hola_uid;
let proxy_rules_engine;
// XXX nikita: rm ext_ver usage
function ext_ver(){ return (api.get_prod_info()||{}).ext_ver; }
function product_ver(){
    let info = api.get_prod_info()||{};
    return info.ext_ver||info.apk_ver||info.svc_ver;
function get_strategy(strategy, ex, opt){
    opt = opt||{};
    if (!ex)
        return strategy;
    // XXX arik/nikita HACK: need proper api to parse ex
    if (opt.country)
    {
        ex = ex.replace(new RegExp('\\b'+zescape.regex(opt.country)+'\\b'),
            'XX');
    }
    let m;
    if (m = ex.match(/PROXY XX\.POOL_(\w+)/))
    {
        m = m[1];
        ex = 'PROXY XX.POOL';
    }
    switch (ex)
    {
    case 'DIRECT': strategy = {name: 'direct'}; break;
    case 'PROXY XX': strategy.name = 'proxy'; break;
    case 'PROXY XX.direct_first': strategy.name = 'direct_first'; break;
    case 'PROXY XX.PEER':
        strategy.name = 'proxy';
        strategy.peer = true;
        break;
    case 'PROXY XX.POOL':
        strategy.name = 'proxy';
        strategy.pool = m||true;
        break;
    }
    return strategy;
// handle_request(url, [opt]):
// url: the absolute url string or the object returned by
//     zurl.parse(absolute_url, true)
// opt: a hash of options:
// opt.top_url: the root URL of the web page loaded in a browser tab; if not
//     available, it's acceptable to pass the Referer header here
// opt.method: request method in uppercase (if known)
// opt.type: request type as per
//     https://developer.chrome.com/extensions/webRequest#type-ResourceType
//     (if known)
// opt.force: force 'direct', 'proxy' or 'peer' routing
// opt.country: country (uppercase), for policy caching purposes
// opt.premium: force 'proxy'
// opt.strict: strategy will stick to forced routing, even on errors; use only
//     for development; only implemented for force='peer'
// Returns a decision function. The caller should obtain a new decision
// function instance for each new URL being handled, and even when handling a
// new request to the same URL.
// The decision function has an extra property: desc. It's a string for use in
// the logs that briefly describes the chosen strategy.
// Decision function signature: (direct, proxy)
// The decision function should be called first at the start of request
// handling, and then again each time one of the subrequests comes back with a
// response, times out or suffers from a network error. A subrequest that has
// timed out should continue to run (the timeout is soft). The timeout event
// should not happen after the request has already returned a response or a
// network error. The caller should also set a reasonably long (minutes) hard
// timeout and treat it as a network error, to prevent unbounded memory
// consumption.
// The direct and proxy arguments are objects describing the subrequests. One
// or both can be null when the corresponding subrequest does not (yet) exist.
// The caller should not create any subrequests automatically. This means that,
// in the first call to the decision function, both arguments shall be null.
// Fields in the subrequest objects (all optional):
// code: (int) response code, if the subrequest has received a response
// type: the response's Content-Type header value, if any
// len: (int) the response's Content-Length header value, as integer, if any
// etag: the response's ETag header value, if any
// location: the response's Location header value, if any
// policy: the value of the proxy response's X-Hola-Policy header, if any
// error: the error description, in case of a network error. A non-empty string
//     or an object meaningfully convertible to a string.
// slow: (boolean) true if the request has timed out. Once it has happened,
//     this shall remain true even if the subrequest later gets a response.
// peer: (boolean) true in case of a proxy response routed through the P2P
//     network. This must be set even before a response arrives.
// hdrs_only: (boolean) true in case of a headers-only proxy response. This
//     need not be set before a response arrives.
// The decision function should return a decision object or undefined; the
// latter is equivalent to {}.
// Fields in the decision object (all optional):
// direct: a nested object describing actions on the direct subrequest
// proxy: a nested object describing actions on the proxy subrequest
// (direct|proxy).start: a truthy value to start the subrequest. This can also
//     be used to restart a previously headers-only subrequest as a full one,
//     or to switch between peer and agent proxies.
// direct.timeout: only meaningful together with start; a truthy value
//     instructs the caller to set an implementation-defined soft timeout on
//     the subrequest.
// proxy.peer: only meaningful together with start; a truthy value to route the
//     request through the P2P network rather than an agent.
// proxy.hdrs_only: only meaningful together with start; a truthy value
//     specifies that the proxy request should be for headers only.
// proxy.allowed: only meaningful together with start; a truthy value means
//     that we know in advance that proxy access is allowed due to a cached
//     policy value. Unless this is set, the implementation may need to issue
//     an advance probe request to the proxy.
// (direct|proxy).abort: a truthy value to abort the subrequest.
// (direct|proxy).serve: a truthy value to serve the response from this
//     subrequest to the client. Also valid for a subrequest with a network
//     error: relay the error to the client, as 502 response or by other means.
//     When one subrequest is being served while another one has a response and
//     is not explicitly aborted, the caller may choose to download the latter
//     one's body for caching, if applicable, or, alternatively, to abort
//     automatically. This should be ignored if a subresponse is being served
//     already. Both direct.serve and proxy.serve can be set together; in this
//     case, the caller should choose whichever one is easier to serve.
// log: a string commenting the decision; to be logged by the caller. This
//     won't describe the actions listed above (they should be logged by the
//     caller anyway), but rather give additional explanations as to why the
//     decision is made.
// Some aspects of a decision may be impossible to implement due to limitations
// of the environment; in such cases, the caller should make the best effort to
// carry out the remaining part of the decision object.
E.get_rule_strategy = (s, url, opt)=>{
    if (!url && (opt.root_url || opt.src_country))
        url = {orig: '', hostname: ''};
    else if (!url || !url.hostname)
        return s;
    let ex = null;
    if (proxy_rules_engine)
    {
        ex = proxy_rules_engine.find_proxy_for_url_exception(url.orig,
            url.hostname, opt);
    }
    return get_strategy(s, ex, opt);
E.handle_request = (url, opt)=>{
    opt = opt||{};
    if (opt.premium===undefined)
        opt.premium = user_plus||_app_trial_mode;
    if (typeof url=='string')
        url = zurl.parse(url, true);
    let top_url = typeof opt.top_url=='string' ?
        zurl.parse(opt.top_url, true) : opt.top_url;
    let strategy, centry = E.cache_access(url.hostname);
    let policy_key = 'policy:'+opt.country;
    let policy = centry.get(policy_key);
    if (policy=='blocked')
        strategy = {name: 'direct'};
    else if (opt.force=='peer' && opt.strict)
        strategy = {name: 'proxy', peer: true};
    else if (opt.premium)
        strategy = E.get_rule_strategy({name: 'proxy', peer: false}, url, opt);
    else if (opt.force=='peer')
        strategy = {name: 'proxy'};
    else if (opt.force)
        strategy = {name: opt.force, peer: false};
    else
    {
        strategy = opt.full_vpn ? {name: 'proxy'} : choose_strategy(url, opt);
        if (strategy && strategy.name!='direct')
            strategy = E.get_rule_strategy(strategy, url, opt);
    }
    if (strategy.cache)
    {
        let cached = centry.get(strategy.cache);
        if (cached)
            strategy = {name: cached, peer: strategy.peer};
    }
    let peer = strategy.peer===true || strategy.peer===undefined &&
        peer_domain_re && (peer_domain_re.test(url.hostname) ||
        top_url && peer_domain_re.test(top_url.hostname));
    let res = strategies[strategy.name].bind(null, {
        cache: centry,
        cache_key: strategy.cache,
        policy: policy,
        policy_key: policy_key,
        peer: peer,
        expect_type: strategy.expect_type,
        strict: opt.strict,
        pool: strategy.pool,
        country: opt.country,
    });
    res.desc = strategy.name;
    if (strategy.cache)
        res.desc += ' cache '+strategy.cache;
    if (peer && strategy.name!='direct')
        res.desc += ' peer';
    if (strategy.name=='proxy' && strategy.pool)
    {
        res.desc += ' pool'+(typeof strategy.pool=='string' ?
            '_'+strategy.pool : '');
    }
    return res;
function choose_strategy(url, opt){
    let ext = url.ext && url.ext.toLowerCase();
    if (opt.method && opt.method!='GET')
        return {name: 'proxy'};
    if (opt.type=='stylesheet' || opt.type=='image')
        return {name: 'parallel', cache: 'static', peer: false};
    if (url.query && zurl.qs_parse(url.query, 0, 1).callback) // JSONP
        return {name: 'parallel'};
    if (extensions.archive[ext])
    {
        return {name: 'direct_first', cache: 'archive', peer: false,
            expect_type: expected_types.archive};
    }
    if (extensions.media[ext] || /=\d+-\d+\.dash/.test(url.file) ||
        /Seg\d+-?Frag\d+/.test(url.pathname) ||
        /\/QualityLevels\(\d+\)\/Fragments\(.+=\d+(,format=.+)?\)/
        .test(url.pathname))
    {
        return {name: 'direct_first', cache: 'media', peer: false,
            expect_type: expected_types.media};
    }
    if (extensions.static[ext])
        return {name: 'parallel', cache: 'static', peer: false};
    if (!user_plus && !_app_trial_mode && ext=='m3u8')
    {
        return {name: 'direct_first', cache: 'media', peer: false,
            expect_type: /mpegurl/i};
    }
    if (opt.type=='main_frame')
        return {name: 'proxy'};
    if (url.protocol=='https:')
        return {name: 'proxy'};
    return {name: 'parallel'};
let last_user_type_set;
E.set_user_plus = flag=>{
    if (user_plus==flag || Date.now()-last_user_type_set < 2*ms.SEC)
        return;
    user_plus = flag;
    last_user_type_set = Date.now();
    return Object.keys(agents).map(a=>agents[a].reinit());
E.set_app_trial_mode = mode=>{ _app_trial_mode = mode; };
// deprecated
E.resp_match = (a, b)=>{
    if (a.code!=b.code)
        return {match: false, basis: 'response code'};
    if (a.type!=b.type)
        return {match: false, basis: 'content type'};
    if (a.etag || b.etag)
        return {match: a.etag==b.etag, basis: 'etag'};
    if (a.len || b.len)
        return {match: a.len==b.len, basis: 'content-length'};
E.unblocker_json_set = function(json, opt){
    proxy_rules_engine = new Proxy_rules_engine(json.unblocker_rules,
        {free: json.unblocker_globals, plus: json.unblocker_plus},
        opt||{ext: true});
    if (json.exceptions)
        E.set_peer_sites(json.exceptions.peer);
// deprecated
E.set_peer_sites = list=>{
    if (!list)
        return;
    peer_domain_re = new RegExp(
        '(^|\\.)('+list.map(zescape.regex).join('|')+')$', 'i');
E.gen_rule = opt=>{
    let rule = {
        name: opt.name,
        country: opt.country.toLowerCase(),
        peer: peer_domain_re && peer_domain_re.test(opt.name) || opt.peer,
        force_peer: opt.force_peer,
        link: opt.name,
        description: opt.country.toUpperCase()+' VPN',
        type: opt.type||'url',
        enabled: opt.enabled!==undefined ? !!opt.enabled : true,
        supported: true,
        root_url_orig: ['**.'+opt.name],
        root_url: [zurl.http_glob_url('**.'+opt.name, 1)],
        ts: opt.ts||Date.now(),
        mode: opt.mode||'unblock',
    };
    zutil.if_set(opt.src, rule, 'src');
    zutil.if_set(opt.src_country, rule, 'src_country');
    zutil.if_set(opt.ratings, rule, 'ratings');
    zutil.if_set(opt.def_traffic, rule, 'def_traffic');
    zutil.if_set(opt.proxy_traffic, rule, 'proxy_traffic');
    zutil.if_set(opt.asnum, rule, 'asnum');
    zutil.if_set(opt.cid, rule, 'cid');
    zutil.if_set(opt.peer_country, rule, 'peer_country');
    zutil.if_set(opt.peer_ip, rule, 'peer_ip');
    zutil.if_set(opt.mitm, rule, 'mitm');
    let pref = [];
    ['def_traffic', 'proxy_traffic'].forEach(e=>opt[e] && pref.push(e));
    ['asnum', 'cid', 'peer_country', 'peer_ip']
    .forEach(e=>opt[e] && pref.push(opt[e]));
    rule.id = rule.name+'_'+rule.country+'_vpn'+(pref.length ?
        '_'+pref.join('_') : '');
    return rule;
class Cache_entry {
    constructor(){
        this.timers = {};
    }
    get(key){ return this[key]; }
    put(key, value, ttl){
        this[key] = value;
        if (this.timers[key])
            clearTimeout(this.timers[key]);
        this.timers[key] = ttl && setTimeout(()=>{
            delete this[key];
            delete this.timers[key];
        }, ttl);
        return this;
    }
    put_weak(key, value, ttl){
        if (!this[key])
            this.put(key, value, ttl);
        return this;
    }
    del(key){
        delete this[key];
        delete this.timers[key];
        return this;
    }
E.cache_access = domain=>{
    let entry = cache[domain];
    if (!entry)
        entry = cache[domain] = new Cache_entry();
    return entry;
E.cache_purge = ()=>{ cache = {}; };
let strategies = {
    direct: (opt, direct, proxy)=>{
        let res = {direct: {}};
        if (!direct)
        {
            res.direct.start = true;
            return res;
        }
        if (direct.code || direct.error)
            res.direct.serve = true;
        return res;
    },
    proxy: (opt, direct, proxy)=>{
        if (opt.policy=='blocked' && !opt.strict)
            return strategies.direct(opt, direct, proxy);
        let res = {direct: {}, proxy: {country: opt.country}};
        if (!proxy)
        {
            res.proxy.start = true;
            res.proxy.peer = opt.peer;
            res.proxy.pool = opt.pool;
            res.proxy.allowed = opt.policy=='allowed';
            return res;
        }
        if (opt.strict)
        {
            res.proxy.abort = true;
            return res;
        }
        if (handle_policy(proxy.policy, opt)=='blocked')
        {
            res.direct.start = true;
            return res;
        }
        if (proxy.code || proxy.error)
            res.proxy.serve = true;
        zutil.if_set(opt.peer, res.proxy, 'peer');
        zutil.if_set(opt.pool, res.proxy, 'pool');
        return res;
    },
    parallel: (opt, direct, proxy)=>{
        if (opt.policy=='blocked')
            return strategies.direct(opt, direct, proxy);
        let res = {direct: {}, proxy: {country: opt.country}};
        function serve_proxy(){
            if (proxy.hdrs_only)
            {
                res.proxy.start = true;
                res.proxy.peer = opt.peer;
                res.proxy.pool = opt.pool;
                res.proxy.allowed = opt.policy=='allowed';
            }
            else
                res.proxy.serve = true;
            return !proxy.hdrs_only;
        }
        if (!direct)
        {
            res.direct.start = true;
            res.direct.timeout = true;
        }
        if (!proxy)
        {
            res.proxy.start = true;
            res.proxy.peer = opt.peer;
            res.proxy.pool = opt.pool;
            res.proxy.allowed = opt.policy=='allowed';
            res.proxy.hdrs_only = true;
        }
        if (!direct || !proxy)
            return res;
        if (handle_policy(proxy.policy, opt)=='blocked')
            return strategies.direct(opt, direct, proxy);
        if (proxy.error)
        {
            if (direct.code || direct.error)
                res.direct.serve = true;
            if (direct.error)
                res.proxy.serve = true; // indifferent
            return res;
        }
        if (!proxy.code)
            return res;
        if (!direct.code)
        {
            if (!serve_proxy())
                return res;
            if (opt.cache_key && direct.slow && is_ok(proxy, opt.expect_type))
            {
                res.log = 'cache put weak proxy';
                opt.cache.put_weak(opt.cache_key, 'proxy', CACHE_TTL);
            }
            else if (!opt.cache_key)
                res.direct.abort = true;
            return res;
        }
        let m = E.resp_match(direct, proxy);
        if (!m)
        {
            serve_proxy();
            return res;
        }
        res.log = m.basis + (m.match ? ' match' : ' mismatch');
        if (m.match)
        {
            res.direct.serve = true;
            if (!proxy.hdrs_only)
                res.proxy.serve = true; // indifferent
            if (opt.cache_key)
            {
                res.log += '; cache put direct';
                opt.cache.put(opt.cache_key, 'direct', CACHE_TTL);
            }
        }
        else
        {
            if (!serve_proxy())
                return res;
            if (opt.cache_key)
            {
                res.log += '; cache put proxy';
                opt.cache.put(opt.cache_key, 'proxy', CACHE_TTL);
            }
        }
        return res;
    },
    direct_first: (opt, direct, proxy)=>{
        if (opt.policy=='blocked')
            return strategies.direct(opt, direct, proxy);
        let res = {direct: {}, proxy: {country: opt.country}};
        if (!direct)
        {
            res.direct.start = true;
            res.direct.timeout = true;
            return res;
        }
        if (is_ok(direct, opt.expect_type))
        {
            res.direct.serve = true;
            if (opt.cache_key)
            {
                res.log = 'cache del';
                opt.cache.del(opt.cache_key);
            }
            return res;
        }
        if (!proxy)
        {
            if (direct.slow || direct.code || direct.error)
            {
                res.proxy.start = true;
                res.proxy.peer = opt.peer;
                res.proxy.pool = opt.pool;
                res.proxy.allowed = opt.policy=='allowed';
            }
            return res;
        }
        if (handle_policy(proxy.policy, opt)=='blocked')
            return strategies.direct(opt, direct, proxy);
        if (proxy.code)
        {
            if (opt.cache_key && (is_ok(proxy, opt.expect_type) ||
                !direct.code))
            {
                res.log = 'cache put proxy';
                opt.cache.put(opt.cache_key, 'proxy', CACHE_TTL);
            }
            else if (proxy.code==302 && direct.code==302)
            {
                if (proxy.location==direct.location)
                    res.direct.serve = true;
                else
                {
                    let p = zurl.parse(proxy.location, true);
                    let d = zurl.parse(direct.location, true);
                    if (p.path==d.path)
                        res.direct.serve = true;
                }
            }
            if (!res.direct.serve)
                res.proxy.serve = true;
            return res;
        }
        if (proxy.error && !direct.slow)
        {
            res.direct.serve = true;
            if (direct.error)
                res.proxy.serve = true; // indifferent
            return res;
        }
    },
function is_ok(resp, expect_type){
    var res = resp.code>=200 && resp.code<300 || resp.code==304;
    if (res && expect_type)
        res = expect_type.test(resp.type);
    return res;
function handle_policy(policy, opt){
    var m = /^(allowed|blocked)( domain)?$/.exec(policy);
    if (!m)
        return 'allowed';
    opt.policy = m[1];
    if (opt.policy_key && m[2])
        opt.cache.put(opt.policy_key, opt.policy, POLICY_TTL);
    return opt.policy;
E.is_conf_allowed = on=>{
    if (!on)
        return;
    if (!conf_rand_id)
        api.storage.set('conf_rand_id', conf_rand_id = Math.random());
    return conf_rand_id<on;
// unblocker API:
// lib.init(set)
// 'set' is a json of the following api:
// - perr - generic perr sender, properly handles info.err
// - ajax - can be set to regular util/ajax.js
// - storage - prod local storage - agent_key, agent_key_ts
// - get_auth - get relevant auth data for qs - uuid, session_key, etc
// - get_prod_info - get relevant version info json - ext_ver, browser,
// - get_user_info - user info - country, hola_uid
// lib.uninit() - on destroy
// lib.reset() - remove all existing agents and cancel all active operations
//   - must be called on premium status change
// lib.get_agents(ping_id, rule) - to get chosen agents for given rule
// lib.replace_agents(rule, replace, opt) - same as agent.get_agents
//   but forces replacement of current selection
var api = {}, agents = {}, mem_storage = {};
var def_api = {
    perr: opt=>{ zerr('agent.perr not set, discarding '+opt.id); },
    ajax: opt=>{ zerr('agent.ajax not set, discarding '+opt.url); },
    storage: {get: id=>mem_storage[id],
        set: (id, val)=>mem_storage[id] = val},
    get_auth: ()=>void zerr('agent.get_auth not set'),
    get_prod_info: ()=>({}),
    get_user_info: ()=>({}),
    suspend_client: ()=>({}),
    url_ccgi: 'https://client.hola.org/client_cgi',
    get_url_ccgi: suffix=>api.url_ccgi+suffix,
    trigger: (ev, val)=>{},
    conf_get: (s, def)=>def,
    // XXX nikita: rm after proxy_rules support on android
    get_rule_routes: rule=>{
        var routes = [], s = zutil.pick(rule, 'country');
        routes.push(s);
        if (rule.peer)
            routes.push(assign({peer: true}, s));
        if (rule.pool)
            routes.push(assign({pool: rule.pool}, s));
        return routes;
    },
    // XXX nikita: add support on android/svc
    get_fallback_agent: ()=>{},
    is_proxy_auth_supported: ()=>{},
const ERR_TIMER = 500;
const MAX_ERR_TIMER = 15*ms.MIN;
let lib_sp;
E.init = set=>{
    E.sp = etask(function*unblocker_lib(){ yield this.wait(); });
    return etask('init', function*init(){
        assign(api, def_api, set);
        lib_sp = etask('lib_sp', function*(){
            conf_rand_id = yield api.storage.get('conf_rand_id');
            hola_uid = yield api.storage.get('hola_uid');
            yield this.wait();
        });
        E.sp.spawn(lib_sp);
        yield init_rules();
        const key = yield api.storage.get('agent_key');
        if (key)
            E.emit('lib_event', {id: 'set_agent_key', data: key});
        E.on('not_working', on_not_working);
    });
E.reset = ()=>{
    active_reqs.forEach(e=>e.return());
    active_reqs = [];
    Object.keys(agents).forEach(a=>agents[a].uninit());
    agents = {};
E.trigger = E.emit;
E.uninit = ()=>{
    lib_sp.return();
    E.reset();
    E.removeAllListeners();
    api = {};
    rules = null;
function Agent(o, parent, opt){
    opt = opt||{};
    if (!(this instanceof Agent))
        return new Agent(o, parent);
    this.host = o.host;
    this.ip = o.ip;
    this.port = o.port;
    this.vendor = o.vendor;
    if (o.pool)
        this.pool = o.pool;
    if (o.port_list)
        this.port_list = o.port_list;
    this.parents = [];
    this.last_traffic = 0;
    this.last_verified = 0;
    this.err_timer = ERR_TIMER;
    this.auth_err_timer = ERR_TIMER;
    this.waiting = [];
    this.add_parent(parent);
    this.verify();
    this.proxy_auth();
Agent.prototype.is_verified = function(period){
    if (this.last_verified && get_verify_conf('disable_verify'))
        return true;
    const ts = Math.max(this.last_traffic, this.last_verified);
    period = period || get_verify_conf('period_ms', 10*ms.MIN)+5*ms.MIN;
    return Date.now()-ts<period;
Agent.prototype.is_verify_err = function(){
    return this.error && (this.error.err||'').startsWith('verify_'); };
Agent.prototype.parent_to_ctx = function(parent){
    return this.parents.find(p=>p.up==parent); };
Agent.prototype.is_usable = function(parent){
    return !this.is_not_usable(parent); };
Agent.prototype.err_ts = function(parent){
    parent = parent && this.parent_to_ctx(parent);
    return this.error && this.error.ts || parent && parent.error
        && parent.error.ts;
Agent.prototype.is_not_usable = function(parent){
    parent = parent && this.parent_to_ctx(parent);
    return this.error&&'error' || this.busy&&'busy' || this.bw_busy&&'bw_busy'
        || !this.is_verified() && this.is_verify_running()&&'verifying'
        || !this.is_verified() && !this.is_verify_running()&&'unverified'
        || !this.proxy_authed && (this.proxy_auth_sp||{}).auth_running
        && 'authing' || !this.proxy_authed
        && !(this.proxy_auth_sp||{}).auth_running && 'not_authed'
        || parent && parent.error && 'parent error';
Agent.prototype.to_string = function(parent){
    return this.host+':'+this.port+'\t'+(this.pool||'reg')+'\t'
        +(this.is_not_usable(parent)||'usable');
Agent.prototype.is_verify_running = function(){
    return this.verify_sp && this.verify_sp.verify_running; };
Agent.prototype.reverify = function(period){
    if (period && (this.is_verify_running() || this.last_verified &&
        Date.now()-this.last_verified<=period))
    {
        return;
    }
    this.last_verified = this.last_traffic = 0;
    if (this.verify_sp)
        this.verify_sp.return();
    this.verify();
Agent.prototype.reauth = function(period){
    if (!this.last_proxy_authed || Date.now()-this.last_proxy_authed<=period)
        return;
    this.last_proxy_authed = 0;
    if (this.proxy_auth_sp)
        this.proxy_auth_sp.return();
    this.proxy_auth();
Agent.prototype.add_parent = function(parent){
    this.parents.push({up: parent}); };
Agent.prototype.unset_error = function(){
    var _this = this;
    this.error = undefined;
    this.err_timer = ERR_TIMER;
    this.verify_error_n = 0;
    this.parents.forEach(function(p){
        if (!p.error)
            p.up.unset_exclude(_this);
    });
Agent.prototype.set_error = function(err, desc, rule, forget){
    var _this = this;
    if (rule)
        rule = zutil.pick(rule, 'name', 'country');
    else
        this.error = {err, desc, ts: Date.now()};
    if (this.is_verify_err())
        this.verify_error_n++;
    let c = get_verify_conf('verify_err', {period: 2*ms.HOUR, count: 10});
    forget = forget || !this.is_verified(c.period) &&
        this.verify_error_n>c.count;
    this.parents.forEach(function(p){
        if (rule && !svc_vpn_util.find_rule(p.up.rules, rule))
            return;
        if (rule)
            p.error = {err, desc, ts: Date.now()};
        if (forget)
            p.up.forget(_this, forget ? 'disabled' : 'verify_err');
        else
            p.up.set_exclude(_this);
    });
    if (forget)
        this.uninit();
Agent.prototype.info = function(){
    var ret = {host: this.host, ip: this.ip, port: this.port,
        vendor: this.vendor};
    zutil.if_set(this.version, ret, 'version');
    zutil.if_set(this.bw_available, ret, 'bw_available');
    zutil.if_set(this.rtt, ret, 'rtt');
    zutil.if_set(this.country, ret, 'country');
    zutil.if_set(this.pool, ret, 'pool');
    zutil.if_set(this.error, ret, 'error');
    zutil.if_set(this.port_list, ret, 'port_list');
    return ret;
Agent.prototype.wait_verified = function(){
    var _this = this;
    return etask(function*_wait_verified(){
        this.finally(()=>array.rm_elm(_this.waiting, this));
        _this.waiting.push(this);
        return yield this.wait();
    });
Agent.prototype.next_verify = function(){ return this._next_verify; };
Agent.prototype.next_auth = function(){ return this._next_auth; };
Agent.prototype.get_rule = function(){ return this.parents[0].up.rules[0]; };
Agent.prototype.verify = function(){
    let _this = this, rule = this.get_rule();
    let ping_id = this.ip+'_'+gen_ping_id();
    let qs = assign({proxy_country: this.parents[0].up.country, ping_id,
        root_url: rule.link||rule.name}, api.get_prod_info());
    this.verify_error_n = 0;
    E.sp.spawn(etask({name: 'verify_'+(this.host||this.ip)}, function*(){
        qs.key = yield api.storage.get('agent_key');
        let force_ip;
        let verify_err = (err, desc, url)=>{
            let info = {host: _this.host, ip: _this.ip, err, err_desc: desc};
            unittest_resp_by_route(_this.parents.map(p=>p.up.route), 'ea',
                `<resp(fail=${err})`, this.req_id);
            svc_vpn_util.push_event('<verify_err', info);
            E.emit('lib_event', {id: 'verify_done',
                data: assign({url}, info)});
            _this.perr('verify_err', info);
            force_ip = !CG('disable_verify_fallback') &&
                _this.err_timer>ERR_TIMER && err!='agent_err';
            _this.err_timer = Math.min(_this.err_timer*4, MAX_ERR_TIMER);
            if (_this.err_timer==MAX_ERR_TIMER)
                force_ip = false;
            _this.set_error('verify_'+err, desc, undefined, err=='agent_err');
            _this.waiting.forEach(et=>et.continue());
        };
        this.finally(()=>{ delete _this.verify_sp; });
        for (;;)
        {
            if (this.verify_running)
            {
                svc_vpn_util.push_event('<verify', {host: _this.host,
                    ip: _this.ip, rtt: _this.rtt});
            }
            this.verify_running = false;
            _this.verify_sp = this;
            if (_this.error)
            {
                _this._next_verify = Date.now()+_this.err_timer;
                yield etask.sleep(_this.err_timer);
            }
            if (_this.last_verified && get_verify_conf('disable_verify'))
                return;
            let ts = Math.max(_this.last_traffic||0, _this.last_verified||0);
            let now = Date.now();
            let period = get_verify_conf('period_ms', 10*ms.MIN);
            if (ts && now-ts<period)
            {
                let timer = ts+period-now;
                _this._next_verify = now+timer;
                yield etask.sleep(timer);
                continue;
            }
            this.verify_running = true;
            svc_vpn_util.push_event('>verify',
                {host: _this.host, ip: _this.ip});
            _this._next_verify = 0;
            let url = api.get_verify_url ? api.get_verify_url(_this, force_ip)
                : 'https://'+(_this.host||_this.ip)+':'+_this.port+
                '/verify_proxy';
            this.req_id = unittest_req_by_route(
                _this.parents.map(p=>p.up.route), 'ea', '>req(url='+url+')');
            let ret, t0 = Date.now();
            try {
                if (api.be_dev_mode && api.be_dev_mode.get('verify_ajax_fail'))
                {
                    yield etask.sleep(SEC);
                    svc_vpn_util.push_event('>verify_ajax_fail');
                    throw new Error('DEBUG_FORCE_FAIL');
                }
                ret = yield api.ajax({timeout: 7*SEC, url, qs});
            } catch(e){
                verify_err(/timeout/.test(e) ? 'timeout' : 'unknown', ''+e,
                    url);
                continue;
            }
            if (!ret || !ret.version)
            {
                verify_err('empty', 'empty ret', url);
                continue;
            }
            if (ret.error)
                return verify_err('agent_err', ret.error, url);
            _this.bw_available = ret.bw_available;
            _this.rtt = Date.now()-t0;
            _this.country = ret.country;
            _this.busy = ret.busy;
            _this.bw_busy = ret.bw_busy;
            _this.version = ret.version;
            _this.last_verified = Date.now();
            _this.unset_error();
            E.emit('lib_event', {id: 'verify_done', data: {host: _this.host,
                ip: _this.ip, rtt: _this.rtt, url}});
            if (_this.busy || _this.bw_busy)
                _this.parents.forEach(p=>p.up.set_busy());
            unittest_resp_by_route(_this.parents.map(p=>p.up.route),
                'ea', '<resp(data='+JSON.stringify(ret)+')', this.req_id);
            _this.waiting.forEach(et=>et.continue());
            if (get_verify_conf('disable_periodic'))
                return;
        }
    }));
Agent.prototype.proxy_auth = function(){
    if (!api.is_proxy_auth_supported() || !api.ajax_via_proxy ||
        get_proxy_auth_conf('disable'))
    {
        return void(this.proxy_authed = true);
    }
    let _this = this, rule = this.get_rule();
    let auth = api.get_auth()||{};
    let ping_id = this.ip+'_'+gen_ping_id();
    let data = {ping: 'proxy_auth', proxy_country: this.parents[0].up.country,
        uuid: auth.uuid, root_url: rule.link || rule.name, ping_id};
    let url = 'http://'+this.ip.replace(/\./g, '_')+
        '.proxy_auth.trigger.hola.org/hola_trigger?'+zurl.qs_str(data);
    let on_auth_error = (err, desc)=>{
        _this.perr('proxy_auth_err', {host: _this.host, ip: _this.ip,
            err: err, err_desc: desc});
        _this.auth_err_timer = Math.min(_this.auth_err_timer*4, 15*ms.MIN);
        _this.auth_error = {err, desc};
    };
    return etask({name: 'proxy_auth_'+(this.host||this.ip)}, function*(){
        this.finally(()=>{ delete _this.proxy_auth_sp; });
        this.auth_running = false;
        _this.proxy_auth_sp = this;
        while (true)
        {
            if (_this.auth_error)
            {
                _this._next_auth = Date.now()+_this.auth_err_timer;
                yield etask.sleep(_this.auth_err_timer);
            }
            let now = Date.now(), ts = _this.last_proxy_authed;
            let period = get_proxy_auth_conf('period_ms', 4*ms.HOUR);
            if (ts && now-ts<period)
            {
                let timer = ts+period-now;
                _this._next_auth = now+timer;
                yield etask.sleep(timer);
                continue;
            }
            this.auth_running = true;
            _this._next_auth = 0;
            this.req_id = unittest_req_by_route(
                _this.parents.map(p=>p.up.route), 'ea', '>req(url='+url+')');
            let ret;
            try {
                ret = yield api.ajax_via_proxy({url, type: 'GET'},
                    {timeout: 7*SEC, rule, agent: _this, force: 'proxy',
                        src: 'proxy_auth'});
            } catch(e){
                on_auth_error(/timeout/.test(e) ? 'timeout' : 'unknown', ''+e);
                continue;
            }
            if (!ret || ret.status!=200)
            {
                unittest_resp_by_route(_this.parents.map(p=>p.up.route),
                    'ea', '<resp(fail=empty)', this.req_id);
                on_auth_error('fail', 'empty ret');
                continue;
            }
            _this.proxy_authed = true;
            _this.last_proxy_authed = Date.now();
            _this.auth_error = undefined;
            unittest_resp_by_route(_this.parents.map(p=>p.up.route), 'ea',
                '<resp(status='+ret.status+')', this.req_id);
            if (get_proxy_auth_conf('disable_periodic'))
                return;
        }
    });
Agent.prototype.perr = function(id, info, err){
    let rule = zutil.pick(this.get_rule(), 'name', 'country');
    info = assign({rule, proxy_country: rule.country, hola_uid}, info);
    api.perr({info, id, err, rate_limit: {count: 2}});
Agent.prototype.to_str = function(){
    let s = zutil.pick(this, 'host', 'bw', 'bw_busy', 'busy', 'error',
        'last_verified', 'last_traffic', 'ip', 'pool');
    return JSON.stringify(s);
Agent.prototype.uninit = function(){
    if (this.verify_sp)
        this.verify_sp.return();
    if (this.proxy_auth_sp)
        this.proxy_auth_sp.return();
function Agents(rule, route){
    if (!(this instanceof Agents))
        return new Agents(rule);
    // XXX antonp: why need both rule and rules?
    this.rule = rule;
    this.rules = [rule];
    this.route = route.toLowerCase();
    var m;
    if (m = this.route.match(/\.pool_?(\w+)?/))
        this.pool = m[1]||true;
    this.exclude = [];
    this.agents = [];
    this.deselect = {};
    this.deselected = {};
    this.exclude_vendor = {};
    this._log = [];
    this.limit = CG('verify_proxy.agent_num', 3);
    this.country = rule.country;
    this.get_best_agent_waiting = [];
    this.rl = {};
    agents[this.route] = this;
    this.init_key_monitor(12*ms.HOUR);
Agents.prototype.init_key_monitor = function(monitor_interval){
    if (!CG('enable_agent_key_monitor'))
        return;
    if (this.agent_key_monitor_sp)
        this.agent_key_monitor_sp.return();
    let _this = this;
    this.agent_key_monitor_sp = E.sp.spawn(etask(
        function*agent_key_monitor_sp(){
        yield etask.sleep(monitor_interval);
        for (;;)
        {
            try {
                let ts = yield api.storage.get('agent_key_ts');
                let diff = Date.now()-ts;
                if (ts && diff < monitor_interval)
                {
                    yield etask.sleep(monitor_interval-diff);
                    continue;
                }
                let {agent_key} = yield _this.zgettunnels(null,
                    {agent_key_only: 1});
                if (!agent_key)
                    throw new Error('missing agent_key');
                yield api.storage.set('agent_key', agent_key);
                E.emit('lib_event', {id: 'set_agent_key', data: agent_key});
                yield api.storage.set('agent_key_ts', Date.now());
            } catch(e){
                api.perr({id: 'agent_key_update_err', rate_limit: {count: 2}},
                    e);
                yield etask.sleep(10*ms.MIN);
            }
        }
    }));
Agents.prototype.reverify = function(period){
    this.agents.forEach(a=>a.reverify(period));
    this.exclude.forEach(a=>a.reverify(period));
Agents.prototype.reauth = function(period){
    this.agents.forEach(a=>a.reauth(period));
    this.exclude.forEach(a=>a.reauth(period));
Agents.prototype.uninit = function(){
    if (this.agent_key_monitor_sp)
        this.agent_key_monitor_sp.return();
    this.uninited = true;
    this.agents.forEach(function(a){ a.uninit(); });
    this.agents = [];
    this.exclude.forEach(function(a){ a.uninit(); });
    this.exclude = [];
    this.deselect = {};
    this.deselected = {};
    this.exclude_vendor = {};
    delete agents[this.route];
    if (this.load_agents_info_et)
        this.load_agents_info_et = void this.load_agents_info_et.return();
Agents.prototype.zgettunnels = function(ping_id, opt){
    opt = opt||{};
    let _this = this, fa;
    if ((fa = api.be_dev_mode && api.be_dev_mode.get('force_agent')) &&
        !api.be_dev_mode.get('force_until_change'))
    {
        zerr.info('using force_agent %O instead of /zgettunnels req', fa);
        return {
            ip_list: {[fa.host]: fa.ip},
            protocol: {[fa.host]: 'https'},
            vendor: {[fa.host]: 'force_agent'},
            ztun: {[this.route]: [`HTTP ${fa.host}:${fa.port}`]},
            agent_types: {[this.route]: 'hola'},
        };
    }
    let exclude = array.unique(this.exclude.concat(opt.exclude||[])
        .map(e=>e.host));
    let ex_limit;
    if (ex_limit = CG('exclude_limit', 9))
    {
        if (this.country=='us')
            ex_limit += 3;
        if (exclude.length>ex_limit)
            exclude = rand.rand_subset(exclude, ex_limit);
    }
    let deselect = [];
    for (let host in this.deselect)
    {
        if (!this.deselected[host])
        {
            deselect.push(host);
            this.deselected[host] = true;
        }
    }
    const auth = api.get_auth()||{};
    const data_params = ['uuid', 'session_key', 'install_ts', 'install_ver',
        'identity'];
    let data = zutil.pick(auth, ...data_params);
    zutil.if_set(ping_id, data, 'ping_id');
    let qs = assign({country: this.route, limit: opt.limit||this.limit},
        zutil.omit(auth, data_params));
    zutil.if_set(opt.agent_key_only, qs, 'agent_key_only');
    let params = {exclude, deselect};
    for (let p in params)
    {
        if (params[p].length>1)
        {
            data[p] = params[p].join(',');
            qs[p] = 1;
        }
        else if (params[p].length)
            qs[p] = params[p][0];
    }
    return etask(function*_zgettunnels(){
        this.on('uncaught', e=>{
            api.perr({id: 'zgettunnel_err', info: {country: _this.route,
                exclude, ping_id, hola_info: (e||{}).hola_info},
                rate_limit: {count: 2}}, e);
            throw e;
        });
        this.req_id = unittest_req_by_route(_this.route, 'ec', '>req(url='
            +api.get_url_ccgi('/zgettunnels?'+zurl.qs_str(assign({}, qs, data))
            +')'));
        svc_vpn_util.push_event('>zgettunnels',
            {country: qs.country, exclude});
        if (api.be_dev_mode && api.be_dev_mode.get('zgettunnel_ajax_slow'))
        {
            svc_vpn_util.push_event('>zgettunnel_ajax_slow');
            yield etask.sleep(15*SEC);
        }
        if (api.be_dev_mode && api.be_dev_mode.get('zgettunnel_ajax_fail'))
        {
            svc_vpn_util.push_event('>zgettunnel_ajax_fail');
            throw new Error('DEBUG_FORCE_FAIL');
        }
        if (zutil.is_mocha()) // XXX arik HACK: rm
            api.perr({id: 'zgettunnel_req'});
        let res = yield api.ajax({url: api.get_url_ccgi('/zgettunnels'),
            method: CG('is_zgettunnels_post4') ? 'POST' : 'GET', qs, data,
            retry: 1});
        svc_vpn_util.push_event('<zgettunnels',
            {country: qs.country, ip_list: res.ip_list});
        try {
            unittest_resp_by_route(_this.route, 'ec',
                '<resp(data='+JSON.stringify(res)+')', this.req_id);
        } catch(e){ zerr('failed to generate test %s', e&&e.stack); }
        return res;
    });
Agents.prototype.add_agents = function(_agents, exclude, zgettunnels, opt){
    this.type = zgettunnels.agent_types[this.route];
    for (let proxy_str of _agents)
    {
        let [, host, port] = proxy_str.match(/^.* (.*):(.*)$/);
        let a = {host, port, ip: zgettunnels.ip_list[host],
            vendor: zgettunnels.vendor && zgettunnels.vendor[host]};
        zutil.if_set(zgettunnels.pool && zgettunnels.pool[host], a, 'pool');
        zutil.if_set(zgettunnels.port, a, 'port_list');
        if (!a.pool)
            this.push(a, opt);
        else if (this.pool==true)
        {
            let route = svc_vpn_util.gen_route_str({country: this.country,
                pool: a.pool});
            let route_agent = set_agent_route(assign({}, this.rule,
                {pool: a.pool}), route);
            route_agent.push(a, opt);
            a = route_agent.find(a);
            this.push(a, opt);
        }
        else
            this.push(E.find_agents(a)[0]||a, opt);
    }
Agents.prototype.reinit = function(){
    this.rl = {};
    this.uninited = undefined;
    this.agents.forEach(function(a){ a.uninit(); });
    this.agents = [];
    this.exclude.forEach(function(a){ a.uninit(); });
    this.exclude = [];
    this.deselect = {};
    this.deselected = {};
    this.exclude_vendor = {};
    if (this.load_agents_info_et)
        this.load_agents_info_et = void this.load_agents_info_et.return();
    return this.load_agents_info(null, 'reset');
Agents.prototype.load_agents_info = function(ping_id, reason, opt){
    let _this = this;
    return etask(function*_load_agents_info(){
        if (_this.load_agents_info_et)
            return yield this.wait_ext(_this.load_agents_info_et);
        let usable = _this.agents.filter(a=>!_this.should_replace(a));
        if (usable.length >= _this.limit)
        {
            zerr.info('load_agents_info %s use existing %O', _this.route,
                _this.agents);
            return;
        }
        let since_last_no_agents = Date.now()-_this.last_no_agents_ts;
        if (_this.last_no_agents_ts &&
            since_last_no_agents<CG('no_agents_period', 5*ms.MIN))
        {
            zerr.info('load_agents_info %s - ignore zgettunnels, no agents',
                _this.route);
            return;
        }
        let rl = CG('get_agents_rl', {count: 2, period: ms.MIN});
        if (rl.count && !rate_limit(_this.rl, rl.period, rl.count))
        {
            zerr.info('load_agents_info %s - ignore zgettunnels, max rate',
                _this.route);
            return;
        }
        let exclude = _this.agents.concat(_this.exclude);
        _this.load_agents_info_et = this;
        this.finally(()=>{ delete _this.load_agents_info_et; });
        let zgettunnels = yield _this.zgettunnels(ping_id,
            {exclude, limit: _this.limit - usable.length});
        zerr.info('zgettunnels %s ret %O', _this.route, zgettunnels);
        if (zgettunnels.blocked)
            return void api.suspend_client(zgettunnels.permanent);
        if (zgettunnels.agent_key)
        {
            api.storage.set('agent_key', zgettunnels.agent_key);
            E.emit('lib_event',
                {id: 'set_agent_key', data: zgettunnels.agent_key});
        }
        else
        {
            api.perr({id: 'agent_key_miss', info: {ret: zgettunnels},
                rate_limit: {count: 2}});
        }
        if (!zgettunnels.ztun)
            return;
        if (!_this.agents.length && _this.exclude.length &&
            _this.exclude.every(a=>a.is_verify_err()))
        {
            api.perr({id: 'verify_all_failed',
                info: {agents: _this.exclude.map(a=>a.info()),
                route: _this.route}, rate_limit: {count: 1}});
        }
        let new_agents = zgettunnels.ztun[_this.route]||[];
        zerr.info('%s agents set, reason %s, %O', _this.route, reason,
            new_agents);
        delete _this.last_no_agents_ts;
        if (!new_agents.length)
        {
            _this.last_no_agents_ts = Date.now();
            if (!exclude.length)
            {
                zerr.info('zgettunnels no agents for %s', _this.route);
                if (_this.pool && !_this.agents.length &&
                    !_this.exclude.length)
                {
                    api.perr({id: 'forget_route', info: {route: _this.route},
                        rate_limit: {count: 1}});
                    _this.uninit();
                }
            }
        }
        else
            _this.add_agents(new_agents, exclude, zgettunnels, opt);
    });
Agents.prototype.usable = function(){
    let count = 0;
    for (let agent of this.agents)
        count += agent.is_usable(this);
    return count;
Agents.prototype.dump_agents = function(){
    console.log(this.route+' Agents:');
    this.agents.forEach(a=>console.log(a.to_string(this)));
    console.log(this.route+' Excludes:');
    this.exclude.forEach(a=>console.log(a.to_string(this)));
Agents.prototype.set_busy = function(){
    if (this.usable()<=1)
        this.load_agents_info(gen_ping_id());
Agents.prototype.remove = function(s, agent){
    let i = s.findIndex(a=>a.host==agent.host);
    if (i<0)
        return;
    s.splice(i, 1);
    return true;
Agents.prototype.move = function(s, d, agent){
    if (!this.remove(s, agent))
        return;
    d.push(agent);
    return true;
Agents.prototype.unset_exclude = function(agent){
    this.move(this.exclude, this.agents, agent);
    delete this.deselect[agent.host];
    delete this.deselected[agent.host];
    if (!this.exclude.some(e=>e.vendor==agent.vendor))
        delete this.exclude_vendor[agent.vendor];
Agents.prototype.set_exclude = function(agent){
    this.move(this.agents, this.exclude, agent);
    this.deselect[agent.host] = true;
    this.exclude_vendor[agent.vendor] = Date.now();
    if (this.usable()<=1)
        this.load_agents_info(gen_ping_id());
Agents.prototype.forget = function(agent, reason){
    this.remove(this.exclude, agent);
    this.remove(this.agents, agent);
    delete this.deselect[agent.host];
    delete this.deselected[agent.host];
    api.perr({id: 'forget_agent', info: {agent: agent.info(),
        route: this.route, reason}, rate_limit: {count: 1}});
    if (this.usable()==0)
        this.load_agents_info(gen_ping_id());
Agents.prototype.log = function(s){ this._log.push(s); };
Agents.prototype.has_rule = function(rule){
    return this.rules.some(r=>r.name==rule.name); };
Agents.prototype.add_rule = function(rule){
    if (this.has_rule(rule))
        return;
    this.rules.push(rule);
Agents.prototype.should_replace = function(agent){
    return (!agent||!Object.keys(agent).length) && 'empty'
        || agent.is_not_usable(this);
Agents.prototype.get_best_agent = function(ping_id, opt){
    let _this = this;
    return etask({async: true}, function*get_best_agent(){
        // XXX nikita: async task not cancelled
        if (_this.uninited)
            return;
        this.finally(()=>{
            delete _this.get_best_agent_sp;
            for (let sp of _this.get_best_agent_waiting)
                sp.return(this.retval);
            _this.get_best_agent_waiting = [];
        });
        try {
            if (_this.get_best_agent_sp)
            {
                _this.get_best_agent_waiting.push(this);
                return yield this.wait();
            }
            _this.get_best_agent_sp = this;
            let _agents = _this.get_chosen();
            if (!_agents.length)
            {
                yield _this.load_agents_info(ping_id, 'missing agents', opt);
                _agents = _this.get_chosen();
                if (!_agents.length)
                    return;
            }
            let verified_agent = _agents.find(a=>a.is_verified());
            if (verified_agent)
                return {chosen: verified_agent};
            for (let a of _agents)
            {
                this.spawn(etask(function*wait_verify(){
                    yield a.wait_verified();
                    return a;
                }));
            }
            let res = yield this.wait_child('any', a=>a&&a.is_verified());
            if (res)
                return {chosen: res.child.retval};
        } catch(e){ return {error: ''+e}; }
    });
Agents.prototype.get_all_agents = function(){
    const ret = this.agents.filter(a=>!this.should_replace(a));
    return !ret.length ? this.agents : ret;
Agents.prototype.get_chosen = function(){
    let ret = [];
    let add = (cond, check_vendor)=>{
        let _agents = this.agents.filter(e=>cond(e) && (!check_vendor ||
            check_vendor && e.vendor && !this.exclude_vendor[e.vendor]));
        if (!_agents.length)
            return;
        _agents.sort((a, b)=>a.rtt - b.rtt);
        ret = ret.concat(_agents.slice(0, 3 - ret.length));
    };
    add(a=>!this.should_replace(a, true));
    if (ret.length==3)
        return ret;
    add(a=>this.should_replace(a));
    if (ret.length==3)
        return ret;
    this.exclude.sort((a, b)=>a.err_ts(this) - b.err_ts(this));
    ret = ret.concat(this.exclude.slice(0, 3 - ret.length));
    return ret;
Agents.prototype.get_fallback = function(){
    if (CG('disable_fallback_aagents'))
        return;
    return this.fallback_agent = this.fallback_agent ||
        api.get_fallback_agent(this.country.toUpperCase());
Agents.prototype.push = function(agent, opt){
    if (this.find(agent))
        return -1;
    if (agent instanceof Agent)
        agent.add_parent(this);
    else
        agent = new Agent(agent, this, opt);
    return this.agents.push(agent);
Agents.prototype.find = function(agent){
    function cmp_agents(a, b){ return (a.host==b.host || a.ip == b.ip)
        && (!a.port || a.port==b.port); }
    return this.agents.find(a=>cmp_agents(agent, a))
        || this.exclude.find(a=>cmp_agents(agent, a));
Agents.prototype.to_str = function(){
    let s = '"'+this.route+'":{"agents":[';
    this.agents.forEach((a, i)=>{ s += (i ? ',' : '')+a.to_str(); });
    s += '],"exclude":[';
    this.exclude.forEach((a, i)=>{ s += (i ? ',' : '')+a.to_str(); });
    return s+']}';
E.find_agents = function(agent){
    let res = [];
    if (!agent.host && !agent.ip)
        return res;
    for (let route in agents)
    {
        let a = agents[route].find(agent);
        if (a)
            res.push(a);
    }
    return res;
E.is_agent = o=>!!E.find_agents({ip: o, host: o})[0];
E.update_chosen = agent=>{
    for (agent of E.find_agents(agent))
        agent.last_traffic = Date.now();
E.get_agents_by_route = route=>
    agents[route] ? agents[route].get_all_agents() : [];
// returns agent type from route string
// agent type can be 'hola' for hola agents pool that is used by free users
// and 'vpn' for vpn agents pool used by premium users
// route_str - string in a format "xx[.peer]", where xx - country code,
// examples: 'us.peer', 'ca', etc.
// returns 'hola', 'vpn' or undefined if there are no agents for this route
// string
E.get_agents_type = route=>agents[route] && agents[route].type;
E.get_rule_routes = function(rule, raw){
    let routes = rule && api.get_rule_routes ? api.get_rule_routes(rule) : [];
    return raw ? routes : routes.map(r=>svc_vpn_util.gen_route_str(r));
function set_agent_route(rule, route_str){
    if (agents[route_str])
        agents[route_str].add_rule(rule);
    else
        agents[route_str] = new Agents(rule, route_str);
    return agents[route_str];
E.get_agents = (ping_id, rule, opt)=>etask(function*_get_agents(){
    let succ, res = {rule, proxy_country: rule.country, verify_proxy: {}};
    ping_id = gen_ping_id(ping_id);
    active_reqs.push(this);
    this.finally(()=>array.rm_elm(active_reqs, this));
    for (let route_str of E.get_rule_routes(rule))
    {
        let route_agents = set_agent_route(rule, route_str);
        this.spawn(etask(function*get_best_agent_child(){
            let ret = yield route_agents.get_best_agent(ping_id, opt);
            if (!ret)
                return;
            if (ret.error)
            {
                res.verify_proxy[route_str] = [];
                return;
            }
            succ = true;
            let a = {host: ret.chosen.host, port: ret.chosen.port,
                ip: ret.chosen.ip, type: route_agents.type, verified: true};
            zutil.if_set(ret.chosen.pool, a, 'pool');
            zutil.if_set(ret.chosen.port_list, a, 'port_list');
            let r = {agent: a, res: {version: ret.chosen.version,
                ip: ret.chosen.ip, bw_available: ret.chosen.bw_available,
                country: ret.chosen.country}, t: ret.chosen.t};
            zutil.if_set(ret.chosen.bw_busy, r.res, 'bw_busy');
            zutil.if_set(ret.chosen.busy, r.res, 'busy');
            res.verify_proxy[route_str] = [r];
        }));
    }
    try {
        yield this.wait_child('all', CG('get_agents_timeout', 10*SEC));
        if (!succ)
            throw new Error('not_found');
    } catch(e){
        api.perr({id: 'get_agents_err', rate_limit: {count: 2}}, e);
        res.info = {error: 'catch', err: ''+e};
    }
    return res;
// replace - array of agents in format {host, ip, port}
// opt.user_not_working - should be true if this was called due to user click
// not working
E.replace_agents = (rule, replace, opt)=>etask(function*_change_agents(){
    opt = opt||{};
    if (!force_agent_changed && api.be_dev_mode &&
        api.be_dev_mode.get('force_agent') &&
        api.be_dev_mode.get('force_until_change') &&
        (replace||[])[0].vendor=='force_agent')
    {
        force_agent_changed = true;
    }
    for (let agent_obj of replace)
    {
        for (let agent of E.find_agents(agent_obj))
            agent.set_error('force_change', 'by change agents', rule);
    }
    return yield E.get_agents(null, rule, zutil.pick(opt, 'type', 'src_rule'));
let def_cc_states = {
    wait: {wait: ms.MIN, next: 'hola'},
    hola: {next: 'svd'},
    svd: {next: 'hvpn'},
    hvpn: {next: 'http'},
    http: {next: 'connectivity'},
    connectivity: {urls: ['google.com', 'microsoft.com', 'wikipedia.org'],
        next: 'wait', next_success: 'common_agents'},
    common_agents: {next: 'mem_agents', agents: 'common_agents', retries: 1},
    mem_agents: {next: 'build_agents', agents: 'mem_agents', retries: 1},
    build_agents: {next: 'cloud_agents', agents: 'build_agents', retries: 1},
    cloud_agents: {agents: 'cloud_agents', retries: 1},
let def_cc_api = {
    ajax_via_proxy: undefined,
    perr: function(opt){ zerr('cc.perr not set, discarding '+opt.id); },
function ConnectivityCheck(opt){
    this.api = {};
    assign(this.api, def_cc_api, opt.api);
    this.states = {};
    assign(this.states, def_cc_states, opt.states);
    for (let name in this.states)
    {
        let state = this.states[name] = assign({urls: []}, this.states[name]);
        state.name = name;
        if (opt.ignore)
        {
            state.ignore = opt.ignore instanceof RegExp ?
                opt.ignore.test(name) : opt.ignore.includes(name);
        }
        if (!state.urls || state.urls.length)
            continue;
        let is_agent_state = name.endsWith('_agents');
        let use_urls = !opt[name] || is_agent_state ? opt.hola : opt[name];
        if (typeof use_urls=='string')
            use_urls = [use_urls];
        // zagent proxying will work on any destination, use only first
        if (is_agent_state)
            use_urls = use_urls.slice(0, 1);
        state.urls = use_urls;
    }
    this.opt = opt;
    this.logs = [];
    this.perrs = {};
ConnectivityCheck.prototype.reset = function(){
    if (this.et)
        this.et = this.et.return();
    this.selected = this.state = undefined;
ConnectivityCheck.prototype.uninit = function(){ this.reset(); };
ConnectivityCheck.prototype.log = function(log, state){
    this.logs.push({log: log, state: state}); };
ConnectivityCheck.prototype.perr = function(opt){
    let path = opt.info.name+'.'+opt.id;
    if (zutil.get(this.perrs, path))
        return;
    zutil.set(this.perrs, path, true);
    if (opt.info && this.opt.dbg_logs)
        opt.info.logs = this.logs.map(function(o){ return o.log; });
    return this.api.perr(opt);
ConnectivityCheck.prototype.check = function(state, loop_et){
    let _agents = this.opt[state.agents]||[], ret = {error: 'check failed'};
    let et_timeout, ts, _this = this, next = 0;
    let send_next = ()=>{
        if (_agents[next])
            delete _agents[next].timeout;
        if (!_agents.length || next+1==_agents.length)
            return;
        if (_agents[++next])
            state.urls.forEach(url=>et.spawn(send_ajax(url)));
        else
            et_timeout = setTimeout(()=>et.return(ret), 3000);
    };
    let ajax_via_proxy = (url, type, opt)=>etask(function*ajax_wrapper(){
        let resp = yield _this.api.ajax_via_proxy({url, type}, opt);
        if (resp)
            resp.test_url = url;
        return resp;
    });
    let send_ajax = url=>{
        let proto = state.name=='http' ? 'http' : 'https';
        url = url.startsWith('http') ? url : proto+'://'+url+
            (url.includes('/') ? '' : '/');
        let agent = _agents.length ? _agents[next] : null;
        let opt = {always: true, hdrs_abort: true, ignore_redir: true,
            hdrs: {'Cache-Control': 'no-cache,no-store,must-revalidate,'+
            'max-age=-1'}, force_headers: true, fix_307: true, agent,
            force: agent ? 'proxy' : 'direct', src: 'bg_ajax'};
        if (opt.force=='proxy')
            opt.prot = 'proxy';
        _this.log('ajax '+(_agents.length ? 'proxy ' : '')+url);
        ts = Date.now();
        let _et = ajax_via_proxy(url, 'GET', opt);
        if (agent)
            agent.timeout = setTimeout(send_next, 3000);
        else
        {
            et_timeout = setTimeout(()=>
                _et.return({orig_res: {error: 'timeout'}}), 5000);
        }
        return _et;
    };
    let et = etask(function*connectivity_check(){
        this.finally(()=>{
            clearTimeout(et_timeout);
            if (_agents.length)
                _agents.forEach(a=>clearTimeout(a.timeout));
            if (!ret.error)
            {
                if (state.next_success)
                {
                    _this.state = assign({}, state);
                    _this.retries = 0;
                    _this.state.next = state.next_success;
                }
                else
                {
                    _this.selected = assign({success_url: ret.url}, state);
                    if (_agents.length)
                        _this.selected.agent = _agents[next];
                    _this.perr({id: 'connectivity_check_success',
                        rate_limit: {count: 5}, info: {name: state.name,
                        urls: state.urls, success: ret.url}});
                }
            }
            else
            {
                _this.perr({id: 'connectivity_check_failed',
                    rate_limit: {count: 15}, info: {name: state.name,
                    urls: state.urls, error: ret.error}});
            }
            loop_et.continue();
        });
        if (typeof _agents=='function')
            _agents = yield _agents();
        _agents = _agents||[];
        if (state.agents && !_agents.length)
            return;
        state.urls.forEach(url=>this.spawn(send_ajax(url)));
        yield this.wait_child('any', resp=>{
            if (!ret.error)
                return;
            clearTimeout(et_timeout);
            let _ret = resp && resp.orig_res ? resp.orig_res :
                {error: 'ajax failed'};
            _ret.url = resp && resp.test_url;
            _ret.statusCode = _ret.statusCode!=undefined ? _ret.statusCode
                : _ret.code;
            if (_ret.statusCode>=400)
                _ret.error = 'bad status code '+_ret.statusCode;
            delete _ret.body;
            if (resp && resp.agent && resp.agent.timeout)
                clearTimeout(resp.agent.timeout);
            let proxy_err = _agents.length && /ERR_PROXY_CONNECTION_FAILED/
                .test(_ret.error);
            _this.log('resp '+(_ret.error||_ret.statusCode)+' after '
                +(Date.now()-ts)+'ms', state);
            if (_ret.error || proxy_err)
            {
                ret.error = _ret.error;
                return void send_next();
            }
            if (next)
            {
                _this.perr({id: 'connectivity_check_failed_agent',
                    rate_limit: {count: 2}, info: {name: state.name}});
            }
            return ret = _ret;
        });
    });
    return et;
ConnectivityCheck.prototype.get_state = function(){
    return this.state ? this.states[this.state.next] : this.states.hola; };
ConnectivityCheck.prototype.run = function(opt){
    if (this.error_sent && Date.now()-this.error_sent<10*ms.MIN)
        return;
    opt = opt||{};
    let _this = this;
    return etask({async: true}, function*run(){
        this.finally(()=>{ _this.et = undefined; });
        if (_this.et)
            return yield this.wait_ext(_this.et);
        _this.retries = 0;
        _this.et = this;
        _this.error_sent = false;
        _this.perrs = {};
        while (!_this.selected)
        {
            let state = _this.get_state();
            if (!state)
                break;
            let max = state.retries||_this.opt.retries||3;
            if (_this.retries==max || state.ignore)
            {
                _this.retries = 0;
                _this.state = state;
                continue;
            }
            _this.retries++;
            let _agents = _this.opt[state.agents];
            if (_agents && _agents instanceof ConnectivityCheck)
            {
                if (!_agents.selected)
                    yield _agents.run();
                if (_agents.selected)
                {
                    _this.perr({id: 'connectivity_check_success',
                        rate_limit: {count: 5}, info: {name: state.name,
                        urls: state.urls}});
                    var success_url = _this.opt.def_url ||
                        _agents.selected.success_url;
                    return _this.selected = assign({success_url: success_url,
                        agent: _agents.selected.agent}, state);
                }
                continue;
            }
            let wait_et;
            if (state.wait)
                wait_et = etask.sleep(state.wait);
            else if (_this.check(state, this))
                wait_et = this.wait();
            if (!wait_et)
                break;
            if (opt.on_state)
                opt.on_state(state);
            yield wait_et;
        }
        if (!_this.selected && !_this.error_sent)
        {
            _this.perr({id: 'connectivity_check_all_failed',
                rate_limit: {count: 5}, info: {url: _this.opt.hola}});
            _this.error_sent = Date.now();
        }
        return _this.selected||{error: 'all_failed'};
    });
ConnectivityCheck.prototype.is_completed = function(){
    return !!this.selected; };
ConnectivityCheck.prototype.get_selected_state = function(){
    return this.selected ? this.selected.name : undefined;
ConnectivityCheck.prototype.get_backend_link = function(){
    const s = this.selected;
    return s ? {url: s.success_url, agent: s.agent} : {url: this.opt.def_url};
E.ConnectivityCheck = ConnectivityCheck;
// XXX antonp: unify route_opt and rule as they are almost the same
E.get_chosen_agent = function(route_opt, rule){
    let fa;
    if (!force_agent_changed && (fa = api.be_dev_mode &&
        api.be_dev_mode.get('force_agent')) &&
        api.be_dev_mode.get('force_until_change'))
    {
        return assign({vendor: 'force_agent'}, fa);
    }
    route_opt = route_opt||{};
    const route_str = svc_vpn_util.gen_route_str(route_opt, {no_algo: true});
    let chosen, route_agent = set_agent_route(rule, route_str);
    if (!(chosen = route_agent.get_chosen()) || !chosen.length ||
        chosen[0].replace)
    {
        route_agent.get_best_agent(Math.random());
        return route_agent.get_fallback();
    }
    return chosen[0].info();
E.get_active_agents = function(rule){
    let _agents = [];
    E.get_rule_routes(rule, true).forEach(r=>{
        let a;
        if (a = E.get_chosen_agent(r, rule))
            _agents = _agents.concat(a);
    });
    return _agents;
E.resolve_agents = (rules, opt)=>etask(function*_resolve_agents(){
    let ping_id = gen_ping_id();
    for (let rule of rules)
        this.spawn(E.get_agents(ping_id, rule, opt));
    yield this.wait_child('all');
// XXX antonp: used only by mocha, check why test fails only on prod BAT
E.verify_agents = (period, wait)=>etask(function*_verify_agents(){
    let unverified = new Set(), ts = Date.now();
    for (let a in agents)
        agents[a].agents.forEach(ag=>unverified.add(ag));
    for (let agent of unverified)
    {
        if (!agent.last_verified || ts-agent.last_verified>period)
            agent.reverify(period);
        else
            unverified.delete(agent);
    }
    if (!wait)
        return;
    for (;;)
    {
        for (let agent of unverified)
        {
            if (ts-agent.last_verified<=period)
                unverified.delete(agent);
        }
        if (!unverified.size)
            return;
        yield etask.sleep(wait);
    }
let gen_stamp = ()=>Math.floor(Math.random()*0xffffffff);
function commit_rules(update){
    rules.stamp = gen_stamp();
    if (update)
        gen_rules(rules.set);
    api.storage.set('rules', rules);
E.has_rule = function(o){
    if (!o)
        return;
    if (typeof o=='function')
    {
        for (let i in rules.set)
        {
            if (o(rules.set[i]))
                return true;
        }
        return;
    }
    return svc_vpn_util.find_rule(rules.set, o);
function rules_fixup(){
    let change, now = Date.now();
    let country = api.get_user_info().country;
    for (let i in rules.set)
    {
        let r = rules.set[i];
        let diff = now-(r.ts||0);
        if (!r.enabled)
            continue;
        // disable rule when src_country changed and
        // unblock_country==src_country
        if (r.country==country && diff>ms.DAY && r.mode!='protect' &&
            r.src_country && r.src_country!=country)
        {
            zerr.notice('disabled rule after country change site %s %s -> %s',
                r.name, r.src_country, r.country);
            api.perr({id: 'disable_rule_country_changed',
                info: {rule: {name: r.name, country: r.country,
                src_country: r.src_country}, src_country: country}});
            change = true;
            r.enabled = false;
        }
    }
    if (change)
        commit_rules(true);
function _set_rule(opt, pair){
    let r, i;
    svc_vpn_util.push_event('set_rule', {name: opt.name, src: opt.src, pair,
        country: opt.country, enabled: opt.enabled, mode: opt.mode,
        del: opt.del, mitm: opt.mitm});
    if (!rules || !opt.name || !opt.country)
        return;
    if (!rules.generated)
        rules.generated = {};
    if (!rules.set)
        rules.set = {};
    for (i in rules.set)
    {
        r = rules.set[i];
        if (r.name!=opt.name || opt.src && opt.src!=r.src)
            continue;
        delete rules.generated[i];
        delete rules.set[i];
    }
    if (!opt.del)
    {
        if (opt.name.match(/^https?:\/\//))
            opt.name = svc_vpn_util.get_root_url(opt.name);
        r = E.gen_rule(opt);
        // XXX nikita: rm generated
        rules.generated[r.id] = r;
        rules.set[r.id] = {name: r.name, country: r.country,
            enabled: r.enabled, ts: r.ts, mode: r.mode, src: r.src,
            src_country: r.src_country||''};
        zutil.if_set(opt.def_traffic, rules.set[r.id], 'def_traffic');
        zutil.if_set(opt.proxy_traffic, rules.set[r.id], 'proxy_traffic');
        zutil.if_set(opt.asnum, rules.set[r.id], 'asnum');
        zutil.if_set(opt.cid, rules.set[r.id], 'cid');
        zutil.if_set(opt.peer_country, rules.set[r.id], 'peer_country');
        zutil.if_set(opt.peer_ip, rules.set[r.id], 'peer_ip');
        zutil.if_set(opt.mitm, rules.set[r.id], 'mitm');
    }
    if (pair)
        return;
    let root_urls = get_root_urls(opt.name);
    // get_root_urls() adds opt.name at 0 position. Other root_urls are added
    // if there is site_conf for unblocked site (relevant for bbc)
    for (i = 1; i<root_urls.length; i++)
        _set_rule(assign({}, opt, {name: root_urls[i]}), true);
E.get_site_key = root_url=>{
    let sites = CG('sites', {});
    for (let k in sites)
    {
        let v = sites[k];
        let urls = Array.isArray(v.root_url) ? v.root_url : [v.root_url];
        if (urls.includes(root_url) && v.min_ver &&
            version_util.cmp(ext_ver(), v.min_ver)>=0)
        {
            return k;
        }
    }
E.get_site_conf = root_url=>CG('sites.'+E.get_site_key(root_url));
function get_root_urls(domain){
    let site = E.get_site_conf(domain);
    // XXX nikita: add test
    return site ? [...new Set([domain].concat(site.root_url))] : [domain];
E.set_rule = function(opt){
    if (!rules)
        return;
    opt = assign({ts: Date.now()}, opt);
    unittest_set_rule(opt);
    E.emit('lib_event', {id: 'before_rule_set', data: opt.name});
    _set_rule(opt);
    commit_rules();
function gen_rules(set){
    if (!set)
        return;
    rules.generated = null;
    rules.set = null;
    for (let i in set)
        _set_rule(set[i]);
function is_supported_country(op, rule){
    if (!rule.country)
        return true;
    let c = rule.country;
    if (!c.list || !c.type || !(c.list instanceof Array))
        return true;
    if (c.type=='in' && (!op.country || !c.list.includes(op.country))
        || c.type=='not_in' && op.country && c.list.includes(op.country))
    {
        return false;
    }
    return true;
function get_filtered_rules(urules, op){
    if (!urules)
        return;
    var m, res = {};
    for (var i in urules)
    {
        if (!is_supported_country(op, urules[i]))
            continue;
        res[i] = urules[i];
        if (!(m = urules[i].match))
            continue;
        if (m.min_ver && version_util.cmp(product_ver(), m.min_ver)<0 ||
            (m.browser && !m.browser.includes((api.get_prod_info()||{})
            .browser)))
        {
            delete urules[i].match;
        }
    }
    return res;
E.set_rules = _rules=>{
    let opt = {country: api.get_user_info().country};
    let need_update = rules.enable!=_rules.enable ||
        !zutil.equal_deep(rules.blacklist, _rules.blacklist) ||
        ['globals', 'plus'].some(e=>{
            return !zutil.equal_deep(rules[e],
                get_filtered_rules(_rules['unblocker_'+e], opt));
        });
    if (need_update)
    {
        rules.globals = get_filtered_rules(_rules.unblocker_globals, opt);
        rules.plus = get_filtered_rules(_rules.unblocker_plus, opt);
        rules.blacklist = _rules.blacklist;
        rules.enable = _rules.enable;
        api.storage.set('rules', rules);
    }
E.get_rules = ()=>{
    if (!rules)
        return null;
    rules_fixup();
    if (!rules.globals)
        return null;
    let json = {unblocker_rules: rules.generated||{}, stamp: rules.stamp,
        enable: rules.enable, blacklist: rules.blacklist};
    ['globals', 'plus'].forEach(e=>json['unblocker_'+e] = rules[e]);
    return zutil.clone_deep(json);
E.get_groups = groups=>{
    let ret = {unblocker_rules: {}};
    for (let group of groups)
    {
        let r = E.gen_rule(group);
        ret.unblocker_rules[r.id] = r;
    }
    return zutil.clone_deep(ret);
function agents_to_unittest_str(rule){
    let routes = E.get_rule_routes(rule);
    let _s = '"def":{';
    for (let route in agents)
    {
        if (!rule || routes.includes(route))
            _s += agents[route].to_str();
    }
    return '{'+_s+'}}';
E.reauth_country_agents = (country, period)=>{
    if (CG('reauth.disable'))
        return;
    period = period!==undefined ? period : CG('reauth.period', 5*ms.MIN);
    for (let route in agents)
    {
        let agent = agents[route];
        if (agent.country==country)
            agent.reauth(period);
    }
E.reverify_country_agents = country=>{
    let period = get_verify_conf('reverify_period_ms', 5*ms.MIN);
    for (let a in agents)
    {
        let agent = agents[a];
        if (agent.country==country)
            agent.reverify(period);
    }
let get_ctx = id=>ctx.find(c=>c.id==id);
E.add_ctx = function(id, rule, data){
    try {
        E.del_ctx(id);
        ctx.push({id, rule, data, ts: Date.now(), ids: {ew: 0,
           bw: 0, ea: 0, ec: 0}, reqs: {}, routes: E.get_rule_routes(rule),
           unittest: [], src: data.src});
        if (!gen_auto_unittest())
            return;
        c_unittest_push(id, 'open(country='+api.get_user_info().country
            +' date="'+date.to_sql(new Date())+'" auto user_plus='+user_plus
            +' set_agents='+agents_to_unittest_str(rule)+')');
        c_unittest_push(id, 'set_rule(raw='+JSON.stringify(rule)+')');
        c_unittest_push(id, 'nav(url='+data.src+')');
        if (ctx.length>20)
            ctx.splice(0, 10);
    } catch(e){ console.error(e); }
E.del_ctx = id=>array.rm_elm(ctx, get_ctx(id));
let agent_num = host=>host.match(/[a-z]+([0-9]+)/)[1];
E.on_before = (url, req_id, ctx_id, data)=>{
    if (!gen_auto_unittest())
        return;
    try {
        let c = get_ctx(ctx_id);
        if (!c)
            return;
        c_unittest_push(ctx_id, 'bw', '>req(url='+url+' '+(data.strategy
            ? 'strategy="'+data.strategy+(data.agent ? '.z'
            +agent_num(data.agent) : '')+'"' : '')+')', req_id);
    } catch(e){ console.error(e); }
E.on_completed = (req_id, ctx_id, data)=>{
    if (!gen_auto_unittest())
        return;
    try {
        let c = get_ctx(ctx_id);
        if (!c)
            return;
        let req = c.unittest.find(u=>u.req_id==req_id);
        if (!req)
            return;
        c_unittest_push(ctx_id, 'bw', '<resp(status="'+(data.status||0)
            +'"'+(data.hdrs ? ' hdrs='+JSON.stringify(zutil.pick(data.hdrs,
            'content-length', 'x-hola-response', 'x-hola-error', 'location'))
            : '')+')', req_id, req.roles_id);
    } catch(e){ console.error(e); }
E.dump_unittest = ctx_id=>{
    let c = get_ctx(ctx_id);
    if (c)
        c.unittest.forEach(u=>console.log(u.s));
class Proxy_exceptions {
    constructor(){
        this.ips = [];
        this.match_root_url = {};
        let rule_props = ['hosts', 'root_urls', 'src_country',
            'proxy_country'];
        for (let name of rule_props)
        {
            this[name] = {};
            this[name+'_cache'] = {};
            this[name+'_counter'] = 0;
        }
    }
let regex_ip2 = /^((\d{1,3}\.){3}\d{1,3})(\/(\d+))?$/;
class Proxy_rules_engine {
    constructor(unblocker_rules, proxy_rules, opt){
        proxy_rules = proxy_rules||{};
        opt = opt||{};
        this.opt = {
            redir_direct: opt.redir_direct===undefined||opt.redir_direct,
            proxy_port: opt.proxy_port||6857,
            redir_port: opt.redir_port||6850,
            ext: opt.ext||0,
            by_rules: opt.by_rules||0,
            do_redir: opt.do_redir||0,
        };
        this.hosts = {
            hosts: {},
            ips: [],
            hosts_cache: {},
            hosts_counter: 0,
        };
        if (!this.opt.by_rules)
        {
            for (let i in unblocker_rules)
            {
                let rule = unblocker_rules[i];
                if (rule.internal || this.opt.ext && !rule.enabled)
                    continue;
                this.parse_cmds(i, rule, unblocker_rules, this.hosts);
            }
        }
        else
        {
            this.rules = {};
            for (let i in unblocker_rules)
            {
                let rule = unblocker_rules[i];
                if (rule.internal || this.opt.ext && !rule.enabled)
                    continue;
                if (!rule.root_url)
                {
                    this.parse_cmds(i, rule, unblocker_rules, this.hosts);
                    continue;
                }
                if (!rule.cmds)
                    continue;
                this.rules[i] = {hosts: {}, ips: [], hosts_cache: {},
                    hosts_counter: 0};
                this.parse_cmds(i, rule, unblocker_rules, this.rules[i], 1);
            }
        }
        this.exceptions = this.parse_exceptions(proxy_rules.free);
        this.exceptions_plus = this.parse_exceptions(proxy_rules.plus);
    }
    find_proxy_for_url_exception(url, host, opt){
        opt = opt||{};
        let rules = opt.is_trial || !opt.premium ? this.exceptions :
            this.exceptions_plus;
        let ret;
        if (!(ret = this.find_proxy_for_match(url, host, rules, opt)))
            ret = this.find_proxy_for_url_exception_int(url, host, rules, opt);
        return ret && ret.str;
    }
    find_proxy_for_val(url, host, hosts, opt){
        opt = opt||{};
        if (!opt.name || !opt.val)
            return;
        let do_redir = this.opt.do_redir;
        let cname = opt.name+'_cache', counter = opt.name+'_counter';
        if (hosts[cname])
        {
            let c = hosts[cname][opt.val];
            if (c && c.func)
                return c.func(url, host, do_redir, opt);
            if (c)
                return this.pac_redir(url, host, do_redir);
        }
        // XXX arik: review what is the logic to clear cache
        if (hosts[counter]>5000)
        {
            hosts[counter] = 0;
            hosts[cname] = {};
        }
        let func;
        if (!(func = hosts[opt.name][opt.val]))
            return;
        hosts[cname][opt.val] = {func: func};
        hosts[counter]++;
        return func(url, host, do_redir, opt);
    }
    find_proxy_for_url_exception_int(url, host, rules, opt){
        let ret = this.find_proxy_for_url(url, host, rules, {exception: 1});
        if (!ret && opt.root_url)
        {
            ret = this.find_proxy_for_val(url, host, rules, {name: 'root_urls',
                val: opt.root_url, exception: 1});
        }
        if (!ret && opt.src_country)
        {
            ret = this.find_proxy_for_val(url, host, rules,
                {name: 'src_country', val: opt.src_country, exception: 1});
        }
        if (!ret && opt.proxy_country)
        {
            ret = this.find_proxy_for_val(url, host, rules,
                {name: 'proxy_country', val: opt.proxy_country, exception: 1});
        }
        return ret;
    }
    find_pac_proxy(url, host){
        let ret = this.find_proxy_for_url(url, host, this.hosts);
        if (ret.proxy)
        {
            let ex = this.find_proxy_for_url(url, host, this.exceptions,
                {exception: 1, orig_proxy: ret.str});
            if (ex)
                ret = ex;
        }
        return ret.str;
    }
    find_proxy_for_url(url, host, hosts, opt){
        opt = opt||{};
        let do_redir = this.opt.do_redir;
        if (hosts.hosts_cache)
        {
            let c = hosts.hosts_cache[host];
            if (c && c.func)
                return c.func(url, host, do_redir, opt);
            if (c)
                return this.pac_redir(url, host, do_redir);
        }
        if (hosts.hosts_counter>5000)
        {
            hosts.hosts_counter = 0;
            hosts.hosts_cache = {};
        }
        if (zurl.is_ip(host))
        {
            let ip = zurl.ip2num(host);
            let ips = hosts.ips;
            for (let i=0; i<ips.length; i++)
            {
                let _ip = ips[i];
                if ((ip >>> _ip.bits << _ip.bits)^_ip.mask)
                    continue;
                hosts.hosts_cache[host] = {func: _ip.func};
                hosts.hosts_counter++;
                return _ip.func(url, host, do_redir, opt);
            }
        }
        let index = -1;
        for (;;)
        {
            let func = hosts.hosts['*']||hosts.hosts[host.substr(index+1)];
            if (func)
            {
                hosts.hosts_cache[host] = {func: func};
                hosts.hosts_counter++;
                return func(url, host, do_redir, opt);
            }
            if ((index = host.indexOf('.', index+1))<0)
                break;
        }
        if (opt.exception)
            return null;
        hosts.hosts_cache[host] = {};
        hosts.hosts_counter++;
        return this.pac_redir(url, host, do_redir);
    }
    find_proxy_for_match(url, host, rules, opt){
        opt = opt||{};
        let mr, mrules;
        if (!rules.match_root_url || !opt.root_url ||
            !((mrules = rules.match_root_url[opt.root_url]) ||
            (mrules = rules.match_root_url[''])))
        {
            return opt.orig_proxy;
        }
        mr = mrules.find(r=>{
            if (r.fsrc_country && !r.fsrc_country.includes(opt.src_country))
                return false;
            if (r.fproxy_country &&
                !r.fproxy_country.includes(opt.proxy_country))
            {
                return false;
            }
            return true;
        });
        if (mr)
            return this.find_proxy_for_url_exception_int(url, host, mr, opt);
    }
    parse_exceptions(rules){
        if (!rules)
            rules = {};
        let exceptions = new Proxy_exceptions();
        for (let name in rules)
        {
            let rule = rules[name];
            if (rule.match)
                this.parse_match(name, rule, exceptions);
            else
                this.parse_cmds(name, rule, rules, exceptions);
        }
        return exceptions;
    }
    pac_redir(url, host, do_redir){
        if (!do_redir || !this.opt.redir_direct)
            return {proxy: false, str: 'DIRECT'};
        /* XXX arik/antonp: I disabled the code to avoid blocking dnsResolve
         * need to decide if need to handle (eg. don't allow set_rule for
         * internal domains
        let ip = E.dns_resolver(host);
        if (zbrowser.isInNet(ip, '10.0.0.0', '255.0.0.0') ||
            zbrowser.isInNet(ip, '172.16.0.0', '255.240.0.0') ||
            zbrowser.isInNet(ip, '192.168.0.0', '255.255.0.0') ||
            zbrowser.isInNet(ip, '127.0.0.0', '255.0.0.0'))
        {
            return {proxy: false, str: 'DIRECT'};
        }
        */
        if (zbrowser.isPlainHostName(host))
            return {proxy: false, str: 'DIRECT'};
        let m = url.match(/^.+:([0-9]+)\/.*$/);
        if (m && m.length==2 && m[1]!='80')
            return {proxy: false, str: 'DIRECT'};
        if (url.match(/^https:.*$/))
            return {proxy: false, str: 'DIRECT'};
        return {proxy: false,
            str: 'PROXY 127.0.0.1:'+this.opt.redir_port+'; DIRECT'};
    }
    handle_then(value, url, host, do_redir, exception, orig_proxy){
        if (value=='DIRECT')
            return this.pac_redir(url, host, do_redir);
        let n = value.split(' ');
        if (exception && n[0]=='PROXY')
        {
            if (n.length==1)
                return null;
            if (n[1].startsWith('XX') && orig_proxy)
            {
                let c = orig_proxy.split(' ')[1].split('.')[0];
                return {proxy: true, str: 'PROXY '+n[1].replace('XX', c)};
            }
        }
        if (n.length<2)
            return this.pac_redir(url, host, do_redir);
        else if (!{PROXY: 1, SOCKS: 1, SOCKS5: 1, HTTPS: 1}[n[0]])
            return this.pac_redir(url, host, do_redir);
        if (this.opt.ext)
            return {proxy: true, str: value};
        return {proxy: true, str: 'PROXY 127.0.0.1:'+this.opt.proxy_port};
    }
    static get_ext(url){
        let ext = '', index = url.indexOf('?');
        if (index>=0)
            url = url.slice(0, index);
        let ext_index = url.lastIndexOf('.', url.length);
        let _ext_index = url.lastIndexOf('/', url.length);
        if (ext_index>=0 && ext_index>_ext_index)
            ext = url.slice(ext_index+1);
        else if (_ext_index>=0)
            ext = url.slice(_ext_index+1);
        return ext;
    }
    host_cb(name, rule, cmd, url, host, do_redir, opt){
        if (!cmd['if'])
        {
            if (cmd.dst_dns)
            {
                return this.handle_then(cmd.then, url, host, do_redir,
                    opt.exception, opt.orig_proxy);
            }
            cmd['if'] = [];
        }
        let ext = Proxy_rules_engine.get_ext(url);
        for (let i=0; i<cmd['if'].length; i++)
        {
            let _if = cmd['if'][i];
            let arg = null, value = null;
            let type = '==';
            if (!_if.then)
                continue;
            if (_if.type)
                type = _if.type;
            if (_if.host)
            {
                arg = host;
                value = _if.host;
            }
            else if (_if.url)
            {
                arg = url;
                value = _if.url;
            }
            else if (_if.ext)
            {
                arg = ext;
                value = _if.ext;
            }
            else if (_if.main)
            {
                arg = opt.is_main;
                value = _if.main;
            }
            else
                continue;
            let cmp;
            switch (type)
            {
            case '==': cmp = arg==value; break;
            case '!=': cmp = arg!=value; break;
            case '=~': cmp = arg.match(value); break;
            case '!~': cmp = !arg.match(value); break;
            case '=a':
            case 'in': cmp = value.includes(arg); break;
            case '!a':
            case 'not_in': cmp = !value.includes(arg); break;
            case '=o': cmp = !!value[arg]; break;
            case '!o': cmp = !value[arg]; break;
            default: continue;
            }
            if (cmp)
            {
                return this.handle_then(_if.then, url, host, do_redir,
                    opt.exception, opt.orig_proxy);
            }
            if (_if['else'])
            {
                return this.handle_then(_if['else'], url, host, do_redir,
                    opt.exception, opt.orig_proxy);
            }
        }
        return this.handle_then(cmd.then, url, host, do_redir, opt.exception,
            opt.orig_proxy);
    }
    set_rule(name, rule, cmd, hosts){
        let add = prop=>{
            if (!cmd[prop] || !hosts[prop])
                return;
            for (let i = 0; i<cmd[prop].length; i++)
            {
                let val = cmd[prop][i];
                hosts[prop][val] = (url, host, do_redir, opt)=>{
                    return this.host_cb(prop, rule, cmd, url, host, do_redir,
                        opt);
                };
            }
            return true;
        };
        let _cif = cmd['if'];
        if (_cif)
        {
            for (let i=0; i<_cif.length; i++)
            {
                let _if = _cif[i];
                if (_if.type=='=~' || _if.type=='!~')
                {
                    if (_if.host)
                        _if.host = new RegExp(_if.host);
                    else if (_if.url)
                        _if.url = new RegExp(_if.url);
                    else if (_if.ext)
                        _if.ext = new RegExp(_if.ext);
                }
            }
        }
        if (!cmd.hosts)
        {
            if (add('root_urls'))
                return;
            if (add('src_country'))
                return;
            if (add('proxy_country'))
                return;
            hosts.hosts['*'] = (url, host, do_redir, opt)=>{
                return this.host_cb(name, rule, cmd, url, host, do_redir, opt);
            };
            return;
        }
        for (let i=0; i<cmd.hosts.length; i++)
        {
            let _host = cmd.hosts[i], n;
            if (n = _host.match(regex_ip2))
            {
                if (!cmd.ips)
                    cmd.ips = [];
                let bits = 32 - (n[4] ? +n[4] : 32);
                if (bits<0)
                    bits = 0;
                let mask = zurl.ip2num(n[1]) >>> bits << bits;
                hosts.ips.push({mask: mask, bits: bits,
                    func: (url, host, do_redir, exception)=>{
                        return this.host_cb(name, rule, cmd, url, host,
                            do_redir, exception);
                    }});
                continue;
            }
            hosts.hosts[_host] = (url, host, do_redir, opt)=>{
                return this.host_cb(name, rule, cmd, url, host, do_redir, opt);
            };
        }
    }
    parse_cmds(name, rule, rules, exceptions, by_rules){
        let cmds = rule.cmds;
        if (!cmds)
            return;
        for (let i=0; i<cmds.length; i++)
        {
            let cmd = cmds[i];
            if (cmd.rule)
            {
                let _name = cmd.rule;
                return this.parse_cmds(_name, rules[_name], rules, exceptions);
            }
            if (!cmd.hosts && !by_rules && !cmd.root_urls &&
                !cmd.src_country && !cmd.proxy_country || !cmd.then)
            {
                continue;
            }
            this.set_rule(name, rule, cmd, exceptions);
        }
    }
    parse_match(name, rule, exceptions){
        let match = rule.match;
        if (!match || !match.rules || !match.root_urls)
            return;
        let res = [];
        for (let r of match.rules)
        {
            let ex = new Proxy_exceptions();
            res.push(ex);
            this.parse_cmds(name, r, match, ex);
            ex.fsrc_country = r.src_country;
            ex.fproxy_country = r.proxy_country;
        }
        match.root_urls.forEach(function(root_url){
            exceptions.match_root_url[root_url] = res; });
    }
    push(bw_rule, req){
        let acc = this.data[bw_rule.id];
        if (acc && acc.rule!=req.opt.rule)
            this._send(bw_rule, acc);
        acc = this.data[bw_rule.id] = this.data[bw_rule.id]||{len: 0, ms: 0,
            rule: req.opt.rule};
        acc.len += +req.proxy_resp.len;
        acc.ms += Date.now()-req.start_ts;
        // XXX sergeir: sending bw stats every 10MiB
        if (acc.len>(bw_rule.sample_size||10*1024*1024))
            this._send(bw_rule, acc);
    }
function c_unittest_push(ctx_id, roles, req, req_id, roles_id){
    if (arguments.length==2)
    {
        req = roles;
        roles = null;
    }
    let c;
    if (!(c=get_ctx(ctx_id)))
         return;
    if (!roles)
        return void c.unittest.push({s: req, req_id});
    roles_id = roles_id||++c.ids[roles];
    c.unittest.push({s: roles+roles_id+req, req_id, roles_id: roles_id});
function on_not_working(id, e){
    let c;
    if (gen_auto_unittest() && (c=get_ctx(id)))
        e.unittest = c.unittest.map(u=>u.s).slice(0, 300);
    api.perr({id: 'be_not_working_trigger', info: e,
        rate_limit: {count: 5, ms: date.ms.MIN}}, true);
function unittest_req_by_route(routes, roles, req){
    if (!gen_auto_unittest())
        return;
    try {
        if (!Array.isArray(routes))
            routes = [routes];
        let req_id = gen_stamp();
        ctx.forEach(c=>{
            if (c.routes.some(r=>routes.includes(r)))
                c_unittest_push(c.id, roles, req, req_id);
        });
        return req_id;
    } catch(e){ console.error(e); }
function unittest_resp_by_route(route, roles, resp, req_id){
    if (!gen_auto_unittest())
        return;
    try {
        ctx.forEach(c=>{
            if (!c.routes.includes(route))
                return;
            let req = c.unittest.find(u=>u.req_id==req_id);
            if (req)
                c_unittest_push(c.id, roles, resp, req_id, req.roles_id);
        });
    } catch(e){ console.error(e); }
function unittest_set_rule(opt){
    if (!gen_auto_unittest())
        return;
    try { c_unittest_push(opt.tab_id, 'set_rule('+JSON.stringify(opt)+')'); }
    catch(e){ console.error(e); }
function gen_auto_unittest(){
    let conf = CG('auto_unittest', {});
    return conf && conf.enable && ext_ver() && version_util.cmp(ext_ver(),
        conf.min_ver||'0.0.0')>=0 && E.is_conf_allowed(conf.on||100);
function get_conf(root, name, def){
    let conf = CG(root);
    if (conf && conf.min_ver && product_ver() &&
        version_util.cmp(product_ver(), conf.min_ver)>=0 &&
        E.is_conf_allowed(conf.on||100))
    {
        return zutil.get(conf, name, def);
    }
    return def;
let get_verify_conf = (name, def)=>get_conf('verify_proxy', name, def);
let get_proxy_auth_conf = (name, def)=>get_conf('proxy_auth', name, def);
let rules;
let init_rules = ()=>etask(function*init_rules_(){
    rules = yield api.storage.get('rules');
    if (!rules)
    {
        rules = {ver: ext_ver()};
        commit_rules();
    }
    if (rules.set)
        gen_rules(rules.set);
    if (zutil.is_mocha())
        E.t.rules = rules;
    E.emit('local_rules_set', rules.set);
if (zutil.is_mocha())
    E.t = {Cache_entry, choose_strategy, expected_types, extensions,
        strategies, Agents, get_api: ()=>api, ConnectivityCheck,
        find_agents: E.find_agents, rules_fixup, rules, get_strategy,
        Proxy_rules_engine};
return E; }); }());
