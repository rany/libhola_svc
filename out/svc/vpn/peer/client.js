// LICENSE_CODE ZON
'use strict'; /*zlint node:true*/
require('../../../util/config.js');
const url = require('url');
const fs = require('fs');
const zlib = require('zlib');
const zconf = require('../../../util/config.js');
const attrib = require('../../../util/attrib.js');
const string = require('../../../util/string.js');
const array = require('../../../util/array.js');
const date = require('../../../util/date.js');
const etask = require('../../../util/etask.js');
const zescape = require('../../../util/escape.js');
const file = require('../../../util/file.js');
const zurl = require('../../../util/url.js');
const zerr = require('../../../util/zerr.js');
const conv = require('../../../util/conv.js');
const rate_limit = require('../../../util/rate_limit.js');
const zwget = require('../../../util/wget.js');
const zutil = require('../../../util/util.js');
/* eslint-disable */
const ws = require('ws'); // require for dep scan
const https_proxy_agent = require('https-proxy-agent'); // require for dep scan
/* eslint-enable */
const zws = require('../../../util/ws.js');
const rand_uniform = require('../../../util/rand_uniform.js');
const setdb = require('../../../util/setdb.js');
const unblocker_lib = require('../pub/unblocker_lib.js');
const local_storage = require('../../local_storage.js');
const full_vpn = require('./full_vpn.js');
const conn_mgr = require('./conn_mgr.js');
const ws_server = require('./ws_server.js');
const client_perr = require('./perr.js');
const client_upgrade = require('./upgrade.js');
const util = require('./util.js');
const model = require('./model.js');
const binding = process.zon && process.binding('zsvc');
const dns = require('dns');
const events = require('events');
const net = require('net');
const os = require('os');
const is_win = os.platform().startsWith('win');
const crypto = require('crypto');
const child_process = require('child_process');
const _ = require('lodash');
const netmask = require('netmask');
const https = require('https');
const winreg = is_win ? require('winreg') : null;
const E = exports;
const qw = string.qw, ms = date.ms, env = process.env, assign = Object.assign;
const PING_INTERVAL_MS = ms.HOUR;
let is_vpn = true, last_ping, ping_loop_et;
let conns = 0, conns_by_net = {};
let agree_ts, connect_ts, uuid;
let pprefix = 'hola_client_';
let gw_ip, apkid, appid, partnerid, confdir, idle = false, os_ver;
let after_install, after_update;
let android_ver, mobile_enable = 0, is_plus, usage = {}, conf;
let idle_state, user_token, client_conf = {};
let sdk_version;
let db_cache = new Map(), db_cached_keys = new Set();
let blocked_ip = [];
let tun_bw_count = {};
let install_id;
E.connected = 0;
E.has_internet = 0;
E.cid_js = '';
E.session_key_js = '';
const data_model_init = ()=>{
    model.set('client_status', {
        cid: E.cid_js,
        session_key: E.session_key_js,
        connected: E.connected>0,
        has_internet: E.has_internet>0,
        user_token: E.get_user_token(),
        sync_token: E.get_sync_token(),
    });
    model.set('app_info', {
        start_ts: Date.now(),
        is_android: is_android(),
        is_updater: is_updater(),
        confdir, is_vpn, is_plus, uuid, apkid, appid, install_id, os_ver,
        sdk_version, agree_ts, connect_ts, after_install, after_update,
    });
const get_old_cid = ()=>util.get_last_cid(get_exe());
const parse_makeflags = (makeflags='')=>{
    let flags = {};
    for (let pair of makeflags.split(' '))
    {
        let m = /^(\w+)=(.*)$/.exec(pair);
        if (m)
            flags[m[1]] = m[2];
    }
    return flags;
let makeflags = parse_makeflags(process.zon && process.zon.makeflags);
let global_opt = {
    agent_ping: false,
    agent_ping_interval: 10*ms.MIN,
    agent_ping_timeout: ms.MIN,
    proxy_ping_interval: 10*ms.MIN,
    proxy_ping_timeout: ms.MIN,
    proxy_retry_interval: 10*ms.SEC,
    proxy_retry_max: 30*ms.MIN,
    proxy_handshake_timeout: 10*ms.SEC,
    blocked_ip: [
        '127.0.0.0/8',
        '255.255.255.255',
        '10.0.0.0/8',
        '172.16.0.0/12',
        '192.168.0.0/16',
        '169.254.0.0/16',
    ],
    http_test_sites: [
        {ip: '74.125.195.113', url: 'http://www.google.com/blank.html',
            match: '^$'},
        {ip: '141.101.123.8', url: 'http://ajax.cdnjs.com/ajax/libs/tinymce'
            +'/3.5.8/plugins/example/langs/en.min.js',
            match: 'tinyMCE.addI18n\\("en.example",'+
            '{desc:"This is just a template button"}\\);'},
        {ip: '3.226.172.116',
            url: 'http://http-test1.hola.org/connection/http-test1.html',
            match: 'http-test1\n'},
    ],
// XXX vladimir: similar to net.js:ip_in_list
const ip_in_list = (list, ip)=>list.some(r=>r.contains(ip));
const set_blocked_ip = _blocked_ip=>{
    blocked_ip = _blocked_ip.map(ip=>new netmask.Netmask(ip)); };
const set_opt = opt=>assign(global_opt, opt);
const set_proxy_url = _proxy_url=>{
    proxy_urls = [_proxy_url];
const set_fallback_ips = _fallback_ips=>fallback_ips = _fallback_ips;
const get_fallback_ips = ()=>fallback_ips;
const resolve_addr = (domain, resolve_type, resolver)=>etask(function*(){
    switch (resolve_type)
    {
    case 'resolve4':
        return yield etask.nfn_apply(resolver||dns, '.resolve4', [domain]);
    default:
        return yield etask.nfn_apply(dns, '.lookup',
            [domain, {family: 4, all: true}]);
    }
const cmd_dns = msg=>etask(function*(){
    if (!msg.domain)
        return;
    zerr.notice('cmd_dns '+msg.domain);
    let ret = {};
    try { ret.res = yield resolve_addr(msg.domain, msg.resolve_type); }
    catch(e){ ret.err = `DNS failed: ${e}`; }
    if (msg.get_servers)
        ret.servers = dns.getServers();
    return ret;
const choose_local_addr = (msg, conn)=>{
    if (!msg || msg.skip_local_addr)
        return;
    if (msg.local_addr)
        return msg.local_addr;
    if (!conn)
        return;
    let local_addr = conn.local_addr;
    if (msg.ifname)
    {
        let addr = (_.find(E.devs, {name: msg.ifname})||{}).addr;
        if (!addr)
            zerr('not found addr for '+msg.ifname);
        local_addr = addr||local_addr;
    }
    return local_addr;
let last_vfd = 1;
const milestones = [1, 1e3, 1e4, 1e5];
const cmd_tun = (msg, conn)=>etask(function*(){
    if (!msg.host || !msg.port)
        return;
    let timeline = [], timeline_reported = 0;
    let dn_bytes = 0, up_bytes = 0, milestones_reached = 0;
    let log_timeline = ev=>timeline.push({ev, ts: date.monotonic()});
    let report_timeline = ()=>{
        if (timeline.length>timeline_reported)
        {
            let res = timeline.slice(timeline_reported);
            timeline_reported = timeline.length;
            return res;
        }
    };
    zerr.notice(`cmd_tun ${msg.host}:${msg.port} ${msg.debug||''}`);
    let dns_resolver, resolve_type = msg.resolve_type;
    if (msg.use_dns)
    {
        dns_resolver = new dns.Resolver();
        dns_resolver.setServers(msg.use_dns);
        resolve_type = 'resolve4';
    }
    let open = vfd=>{
        let stream = conn.mux.open(vfd);
        stream.on('error', err=>{
            if (err.message=='1006')
                zerr('ws_stream '+vfd+' unexpected close');
            else
                zerr('ws_stream '+vfd+' err '+zerr.e2s(err));
        });
        return stream;
    };
    let vfd, stream;
    if (msg.vfd)
    {
        vfd = msg.vfd;
        if (last_vfd<vfd)
            last_vfd = vfd;
        stream = open(vfd);
    }
    log_timeline('init');
    let host = msg.host;
    if (!net.isIPv4(host))
    {
        let addr;
        try {
            addr = yield resolve_addr(host, resolve_type, dns_resolver);
            log_timeline('dns');
            if (!addr || !addr.length)
            {
                if (vfd)
                    conn.mux.close(vfd);
                return {err: 'DNS failed: no results'};
            }
            addr = rand_uniform.rand_subset(addr, addr.length);
            host = resolve_type=='resolve4' ? addr[0] : addr[0].address;
        } catch(e){
            zerr(`resolve_err: ${e}`);
            if (vfd)
                conn.mux.close(vfd);
            return {err: `DNS failed: ${e}`};
        }
    }
    if (ip_in_list(blocked_ip, host))
        return {err: `Blocked IP ${host}`};
    let w = etask.wait();
    let sock_opt = {port: msg.port, host};
    let local_addr = choose_local_addr(msg, conn);
    if (local_addr)
        sock_opt.localAddress = local_addr;
    let sock = net.createConnection(sock_opt);
    sock.setNoDelay();
    sock.on('error', e=>w.throw(e));
    sock.on('connect', ()=>w.continue());
    try { yield w; }
    catch(e){
        if (vfd)
            conn.mux.close(vfd);
        return {err: `Connection failed: ${e}`};
    }
    log_timeline('connect');
    if (!vfd)
    {
        vfd = ++last_vfd;
        stream = open(vfd);
    }
    let check_milestones = (dn, up)=>{
        dn_bytes += dn;
        up_bytes += up;
        if (!dn_bytes && up_bytes && up_bytes==up)
            log_timeline('up_1b');
        while (milestones_reached<milestones.length)
        {
            let milestone = milestones[milestones_reached];
            if (dn_bytes<milestone)
                return;
            milestones_reached++;
            log_timeline(milestone<1000
                ? `dn_${milestone}b` : `dn_${milestone/1000|0}kb`);
        }
        // all milestones reached
        conn.ipc.tun_report({vfd, timeline: report_timeline()});
    };
    stream.on('error', err=>{
        try { sock.destroy(); }
        catch(e){ zerr('sock destroy fail'); }
    });
    sock.on('close', ()=>{
        // XXX vladimir: get rid of double close and tun_destroy resp
        try { stream.emit('_close'); }
        catch(e){ zerr('ws_stream on sock close err '+zerr.e2s(e)); }
        conn.ipc.tun_destroy({vfd, timeline: report_timeline()});
    });
    stream.pipe(sock).pipe(stream);
    let bw_type = msg.bw_type;
    sock.on('data', chunk=>{
        bw_update(bw_type, chunk.length, 0);
        check_milestones(chunk.length, 0);
    });
    stream.on('data', chunk=>{
        bw_update(bw_type, 0, chunk.length);
        check_milestones(0, chunk.length);
    });
    timeline_reported = timeline.length;
    return {vfd, timeline, sock_opt};
const cmd_tun_close = (msg, conn)=>{
    zerr.notice(`cmd_tun_close ${msg.vfd} ${msg.debug||''}`);
    let vfd = +msg.vfd;
    if (!vfd)
        return;
    conn.mux.close(vfd);
    return {};
const can_be_peer = ()=>agree_ts && !is_plus;
const check_can_be_peer = (was_peer, is_real_change)=>{
    let is_peer = can_be_peer();
    if (is_peer==was_peer)
        return;
    zerr.notice((is_peer ? 'can' : 'can\'t')+' be peer since now');
    if (is_real_change)
    {
        zerr.perr('vpn.svc.'+pprefix+(is_peer ? 'allow_peer' :
            'disallow_peer'));
    }
const get_ping_wait = function(){
    if (!confdir)
        return 0;
    if (!last_ping)
    {
        let data;
        try { data = file.read(confdir+'/db/last_ping'); } catch(e){}
        last_ping = Number.parseInt(data, 10)||0;
    }
    let now = Date.now();
    return last_ping && now-last_ping<PING_INTERVAL_MS ?
        PING_INTERVAL_MS-now+last_ping : 0;
const set_last_ping = function(){
    last_ping = Date.now();
    if (!confdir)
        return;
    file.write(confdir+'/db/last_ping', last_ping);
const ping_loop = ()=>ping_loop_et = etask(function*(){
    yield etask.sleep(get_ping_wait());
    for (;;)
    {
        try {
            yield zerr.perr(`vpn.svc.${pprefix}ping`, {
                active_conns: conns,
                active_nets: Object.keys(conns_by_net).length});
            set_last_ping();
        } catch(e){}
        yield etask.sleep(PING_INTERVAL_MS);
    }
const background_init = ()=>etask(function*(){
    let path = zescape.uri('/client_cgi/background_init', {uuid, login: 1,
        ver: zconf.ZON_VERSION, os_ver, app: 'svc'});
    try {
        zerr.perr('vpn.svc.background_init');
        let res = yield conn_mgr.wget_client(path, {method: 'GET', json: true,
            expect: 200, retry: 5, retry_interval: 5*date.ms.SEC});
        return ''+zutil.get(res, 'resp.body.key', '');
    } catch(e){ zerr.err('background_init error: '+zerr.e2s(e)); }
const session_use = key=>{
    E.session_key_js = key;
    model.merge('client_status', {session_key: key});
    zerr.notice(`using session key: ${key}`);
const session_init = ()=>etask(function*(){
    let cache, key;
    if (cache = (get_private_data()||{}).session_key)
        session_use(cache);
    if ((key = yield background_init()) && key!=cache)
    {
        session_use(key);
        set_private_data({session_key: key});
    }
E.set_apkid = _apkid=>{
    is_vpn = !_apkid;
    pprefix = is_vpn ? 'hola_client_' : 'hola_client_sdk_';
    zerr.info(`apkid_set ${_apkid}`);
    apkid = _apkid;
    model.set('app_info.apkid', _apkid);
    model.set('app_info.is_vpn', is_vpn);
E.set_appid = _appid=>{
    is_vpn = !_appid;
    pprefix = is_vpn ? 'hola_client_' : 'hola_client_sdk_';
    zerr.info(`appid_set ${_appid}`);
    appid = _appid;
    model.set('app_info.appid', _appid);
    model.set('app_info.is_vpn', is_vpn);
    // XXX colin: add install_id for android
    if (appid.startsWith('win_'))
    {
        install_id = file.read(`${confdir}/lum_sdk_install_id`)||null;
        model.set('app_info.install_id', install_id);
    }
E.set_agree_ts = function(ts){
    let is_real_change = agree_ts!==undefined;
    zerr.info(`set_agree_ts ${agree_ts} -> ${ts}`);
    let was_peer = can_be_peer();
    agree_ts = ts;
    model.set('app_info.agree_ts', agree_ts);
    check_can_be_peer(was_peer, is_real_change);
E.set_uuid = function(_uuid){
    zerr.notice(`set_uuid ${_uuid}`);
    uuid = _uuid;
    model.set('app_info.uuid', uuid);
    if (is_win && !is_updater())
        session_init();
E.get_uuid = ()=>uuid;
E.set_partnerid = _partnerid=>{
    zerr.info(`partnerid ${_partnerid}`);
    partnerid = _partnerid;
E.set_after_install = ()=>{
    zerr.info('after install');
    after_install = true;
    model.set('app_info.after_install', after_install);
E.set_after_update = _after_update=>{
    zerr.info(`after update ${_after_update}`);
    after_update = _after_update;
    model.set('app_info.after_update', after_update);
E.set_sdk_version = _sdk_version=>{
    zerr.info(`sdk_version ${_sdk_version}`);
    sdk_version = _sdk_version;
    model.set('app_info.sdk_version', _sdk_version);
E.get_confdir = ()=>confdir;
E.set_confdir = _confdir=>{
    confdir = _confdir;
    full_vpn.set_confdir(confdir);
    data_model_init();
// XXX shachar: write equivalent version in node
E.set_os_ver = _os_ver=>{
    zerr.info(`set_os_ver ${_os_ver}`);
    os_ver = _os_ver;
    model.set('app_info.os_ver', _os_ver);
E.set_idle = _idle=>{
    zerr.info(`set_idle ${idle}->${!!_idle}`);
    idle = !!_idle;
E.set_mobile_enable = _mobile_enable=>{
    zerr.info(`set_mobile_enable ${mobile_enable} -> ${_mobile_enable}`);
    mobile_enable = _mobile_enable;
E.get_premium_mode = ()=>!!is_plus;
E.set_premium_mode = (_is_plus, is_real_change)=>{
    is_real_change = is_real_change && is_plus!==undefined;
    let was_peer = can_be_peer();
    zerr.info(`set_is_plus ${is_plus} -> ${_is_plus}`);
    is_plus = _is_plus;
    model.set('app_info.is_plus', is_plus);
    check_can_be_peer(was_peer, is_real_change);
E.set_client_conf = _client_conf=>{
    zerr.info(`set_client_conf ${_client_conf}`);
    try { client_conf = JSON.parse(_client_conf); }
    catch(e){ client_conf = {err: 1}; }
    client_conf = client_conf||{};
    model.set('app_info.is_updater', is_updater());
E.bump_dev_generation = ()=>
    setdb.set('devs_updated', (setdb.get('devs_updated')|0)+1);
E.zid_update = ()=>{
    zerr.info(`zid_update`);
    E.bump_dev_generation();
E.usage_update = (app_bytes, total_bytes)=>{
    zerr.info(`usage_update ${app_bytes} ${total_bytes}`);
    usage.app_bytes = app_bytes;
    usage.total_bytes = total_bytes;
    // XXX vladimir: move to zjson and date()
    usage.ts = date.to_sql();
// XXX colin: correct naming
E.mobile_usage = E.usage_update;
E.notify_idle_state = _idle_state=>{
    zerr.notice(`notify_idle_state ${idle_state} -> ${_idle_state}`);
    idle_state = _idle_state;
E.set_user_token = (token, sync_token, force)=>{
    if (force) // trusted source (hola browser)
        check_first('sync');
    let prev_token = E.get_user_token();
    // accept logout from any source
    if (!token && is_login_sync_enabled())
        force = true;
    if (!force && prev_token && prev_token!=token)
        return;
    token = token||'';
    zerr.info(`set_user_token ${user_token} -> ${token}`);
    if (token==user_token)
        return;
    if (!confdir)
        return;
    file.write(confdir+'/db/user_token.json', JSON.stringify({token}));
    user_token = token;
    if (!user_token)
        full_vpn.api.vpn_disconnect();
    if (!sync_token || is_login_sync_enabled())
    {
        sync_token = token&&sync_token||'';
        set_private_data({sync_token});
    }
    else
        sync_token = '';
    model.merge('client_status', {user_token, sync_token});
E.get_user_token = ()=>{
    if (!user_token)
        user_token_load();
    return user_token||null;
// XXX volodymyr: fix all client.get_rmt_conf to use conf model instead
E.get_rmt_conf = (path, def)=>{
    let data = model.get('rmt_conf', {});
    return path ? zutil.get(data, path, def) : data;
const use_rmt_conf = (str, updated_by_remote)=>{
    let data;
    try { data = JSON.parse(str); } catch(e){}
    if (!data||typeof data!='object')
        data = {};
    if (updated_by_remote)
        data.rmt_sync_ts = date.monotonic();
    model.set('rmt_conf', data);
    // remove volatile fields
    data = _.omit(data, 'rmt_sync_ts');
    return JSON.stringify(data);
E.get_sync_token = ()=>{
    return is_login_sync_enabled() &&
        (get_private_data()||{}).sync_token || '';
const is_login_sync_enabled = ()=>!E.get_rmt_conf().disable_login_sync;
let private_data_cache;
const get_private_data = ()=>{
    if (private_data_cache)
        return private_data_cache;
    zerr.notice('get_private_data');
    try {
        let private_data;
        if (private_data = file.read(confdir+'/db/data.dat'))
            return private_data_cache = JSON.parse(util.decrypt(private_data));
    } catch(e){ zerr.err('get_private_data failed: '+zerr.e2s(e)); }
const set_private_data = data=>{
    private_data_cache = assign({}, get_private_data(), data);
    file.write(confdir+'/db/data.dat',
        util.encrypt(JSON.stringify(private_data_cache)));
const add_db_cache = msg=>{
    let name = msg.name;
    zerr.info(`add_db_cache ${name}`);
    if (!name)
        return;
    db_cached_keys.add(name);
    db_get({name});
const get_db_cache = ()=>{
    let res = {};
    for (let [k, v] of db_cache)
        res[k] = v;
    return res;
const is_updater = ()=>client_conf&&client_conf.updater;
const is_mac = ()=>get_platform()=='darwin';
const get_exe = ()=>appid||apkid ? zconf.NET_SVC_EXE :
    is_updater() ? zconf.UPDATER_EXE : zconf.SVC_EXE;
const get_platform = ()=>E.t.platform||process.platform;
const db_hook_process = (name, data, remote)=>{
    if (name=='svc_rmt_conf.json' && !is_updater())
        data = use_rmt_conf(data, remote);
    return data;
const db_cache_update = (name, data, remote)=>{
    if (db_cached_keys.has(name))
        db_cache.set(name, data);
    return data;
const db_set = msg=>{
    let {name, data} = msg;
    if (get_platform()!='win32' || !name || !confdir)
        return;
    zerr.info(`db_set ${name}: ${data}`);
    data = db_hook_process(name, data, true);
    db_cache_update(name, data);
    file.write(confdir+'/db/'+name, data);
const db_get = msg=>{
    let {name, force} = msg;
    if (!name)
        return;
    if (db_cache.has(name) && !force)
    {
        let data = db_cache.get(name);
        zerr.info(`db_get ${name} from cache: ${data}`);
        return data;
    }
    if (get_platform()!='win32' || !confdir)
        return;
    try {
        let data = file.read(confdir+'/db/'+name);
        zerr.info(`db_get ${name}: ${data}`);
        data = db_hook_process(name, data);
        return db_cache_update(name, data);
    } catch(e){ zerr(`db_get ${name} failed`); }
const system_state_get = ()=>{
    try {
        if (is_win)
        {
            let state = binding && binding.state_get();
            if (state)
                return attrib.to_obj(attrib.from_str(state));
        }
    } catch(e){ zerr.err('get_idle_state err '+zerr.e2s(e)); }
const status_get = msg=>{
    zerr.notice(`status_get`);
    let ts_diff;
    if (msg.ts)
        ts_diff = Date.now()-msg.ts;
    let summary = {};
    for (let bw_type in tun_bw_count)
    {
        let bw_use = tun_bw_count[bw_type];
        if (!Array.isArray(bw_use.daily) || !bw_use.daily.length)
            continue;
        let bw_sum = summary[bw_type] = {};
        bw_sum.first = bw_use.daily[0].start;
        bw_sum.last = bw_use.daily[bw_use.daily.length-1].start;
        bw_sum.count = bw_use.daily.length;
        bw_sum.dn = bw_sum.up = 0;
        for (let bw of bw_use.daily)
        {
            bw_sum.dn += bw.dn;
            bw_sum.up += bw.up;
        }
    }
    let system_state = system_state_get();
    return {idle, mobile_enable, usage, ts_diff, idle_state, system_state,
        bw: msg.raw_bw ? tun_bw_count : summary};
const exec_sh = cmd=>{
    try {
        let shell = is_android() ? '/system/bin/sh' : env.SHELL;
        // XXX vladimir: use exec.js instead of execSync
        return child_process.execSync(cmd,
            {shell, stdio: 'pipe', encoding: 'utf8'});
    } catch(e){ return void zerr('exec err '+zerr.e2s(e)); }
const is_android = ()=>makeflags.ARCH=='ANDROID';
const android_sdk_ge = v=>is_android() && E.android_sdk_ver>=v;
const client_connect = (msg, conn, cid)=>etask(function*(){
    this.on('uncaught', e=>zerr('client_connect err '+zerr.e2s(e)));
    zerr.notice(`client_connect start to ${msg.host}:${msg.port}`);
    zerr.perr(`vpn.svc.${pprefix}connect_init`);
    if (!msg.port || !msg.host)
        return void zerr.perr(`vpn.svc.${pprefix}connect_ignored`, {msg});
    let local_addr = choose_local_addr(msg, conn), canceled = true,
      connected = false, conn_label = conn&&conn.label;
    let session = msg.session ? Buffer.from(msg.session, 'base64') : null;
    let agent = new https.Agent({servername: msg.servername, session,
            localAddress: local_addr});
    let client = new zws.Client(`wss://${msg.host}:${msg.port}`, {
        label: 'agent',
        agent: agent,
        ipc_server: {
            dns(_msg){ return cmd_dns(_msg, this); },
            tun(_msg){ return cmd_tun(_msg, this); },
            tun_close(_msg){ return cmd_tun_close(_msg, this); },
            tunnel_init(_msg){ return tunnel_init(_msg, this, cid); },
            status_get(_msg){ return status_get(_msg); },
        },
        ipc_client: {tun_report: 'post', tun_destroy: 'post'},
        ipc_zjson: true,
        zjson_opt: {date: true, re: true, func: false},
        mux: true,
        ping: global_opt.agent_ping,
        ping_interval: global_opt.agent_ping_interval,
        ping_timeout: global_opt.agent_ping_timeout,
    });
    client.on('connected', ()=>{
        connected = true;
        conns++;
        conns_by_net[conn_label] = (conns_by_net[conn_label]||0)+1;
        connect_ts = connect_ts||Date.now();
        model.set('app_info.connect_ts', connect_ts);
        zerr.perr(`vpn.svc.${pprefix}connected`, msg);
        if (!ping_loop_et)
            ping_loop();
        canceled = false;
        this.continue();
    });
    client.on('disconnected', ()=>{
        conns--;
        conns_by_net[conn_label] = (conns_by_net[conn_label]||0)-1;
        if (conns_by_net[conn_label]<=0)
            delete conns_by_net[conn_label];
        let disc_reason = client.reason||'connection failed';
        zerr.perr(connected ? `vpn.svc.${pprefix}disconnected` :
            `vpn.svc.${pprefix}connect_failed`, {disc_reason, msg});
        if (ping_loop_et)
        {
            ping_loop_et.return();
            ping_loop_et = undefined;
        }
        canceled = false;
        this.throw(disc_reason);
        client.close();
    });
    this.finally(()=>{
        if (!canceled)
            return;
        client.close(-2, 'shutdown');
        agent.destroy();
    });
    yield this.wait();
    zerr.notice(`client_connect established ${msg.host}:${msg.port}`);
    return {connected};
const parse_ifconfig = ifconfig=>{
    let lines = ifconfig.split('\n'), ifs = [], cur;
    for (let l of lines)
    {
        if (/^\S/.test(l))
        {
            if (cur)
                ifs.push(cur);
            cur = {name: l.split(/\s/)[0]};
        }
        let m = l.match(/inet addr:(\S*)/);
        if (m)
            cur.addr = m[1];
        if (m = l.match(/LOOPBACK/))
            cur.internal = true;
    }
    if (cur)
        ifs.push(cur);
    return ifs.filter(i=>i.addr && i.addr!='0.0.0.0' && !i.internal);
const parse_netcfg = netcfg=>{
    let ifs = [], lines = netcfg.split('\n');
    for (let l of lines)
    {
        let parts = string.split_ws(l);
        if (parts.length<3)
            continue;
        if (parts[1]!='UP')
            continue;
        if (parts[3] & 0x8)
            continue;
        ifs.push({name: parts[0], addr: parts[2].split('/')[0]});
    }
    return ifs.filter(i=>i.addr && i.addr!='0.0.0.0');
const parse_interfaces = ()=>{
    let ifs = os.networkInterfaces();
    let recs = [];
    for (let [conn, addresses] of Object.entries(ifs))
    {
        let rec = {name: conn};
        for (let addr of addresses)
        {
            if (addr.family!='IPv4' || addr.internal)
                continue;
            rec.addr = addr.address;
        }
        if (rec.addr)
            recs.push(rec);
    }
    return recs.filter(i=>i.addr && i.addr!='0.0.0.0');
const ip_type = (ip, ifs, dev_type)=>{
    if (!ip || !ifs || !dev_type)
        return;
    let dev = _.find(ifs, {addr: ip});
    if (!dev)
        return zerr.warn(`interface not found for ${ip}`);
    let type = dev_type[dev.name];
    if (!type && is_android() && !android_sdk_ge(21))
        type = binding && binding.conf_get(`dev/${dev.name}/type`);
    zerr.notice(`detected interface ${dev.name} of type ${type} for ${ip}`);
    return {name: dev.name, type};
const detect_devs = ()=>{
    let ifs, dev_type;
    if (get_platform()=='win32' || get_platform()=='darwin')
        ifs = parse_interfaces();
    else if (is_android() && android_sdk_ge(29))
    {
        // On Android 10+, we must get the list of devices from Java
        // It is dumped regularly to confdir/net_dev.json
        if (confdir)
        {
            try {
                let net_json = fs.readFileSync(confdir+'/net_dev.json',
                    {encoding: 'utf8'});
                ifs = JSON.parse(net_json);
            } catch(e){
                zerr('Unable to read network interfaces from '
                    +`${confdir}/net_dev.json: `+e);
                ifs = {};
            }
        }
        else
        {
            zerr('configuration directory is not set: no network interfaces '
                +'detection');
            ifs = {};
        }
    }
    else if (!is_android() || android_sdk_ge(23))
    {
        zerr.notice(`detect_devs ifconfig`);
        try {
            // XXX vladimir: os.networkInterfcases() crashes the process
            let ifconfig;
            if (!(ifconfig = exec_sh('ifconfig')))
                return;
            ifs = parse_ifconfig(ifconfig);
        } catch(e){ return void zerr('ifconfig err '+zerr.e2s(e)); }
    }
    else
    {
        zerr.notice(`detect_devs netcfg`);
        try {
            // XXX vladimir: os.networkInterfcases() crashes the process
            let netcfg;
            if (!(netcfg = exec_sh('netcfg')))
                return;
            ifs = parse_netcfg(netcfg);
        } catch(e){ return void zerr('netcfg err '+zerr.e2s(e)); }
    }
    zerr.notice(`detect_devs found ${ifs.length} devs`);
    if (!confdir)
        return void zerr('confdir not set');
    try {
        let zid = file.read(confdir+'/db/z.id')||
            file.read(confdir+'/db/hola.id')||'';
        let ids = attrib.to_obj(attrib.from_str(zid));
        let dev_type_id = string.split_ws(ids.dev_type||'');
        dev_type = _.object(dev_type_id.filter((x, i)=>!(i%2)),
            dev_type_id.filter((x, i)=>i%2));
    } catch(e){ return void zerr('Conf read fail '+zerr.e2s(e)); }
    for (let {name, addr} of ifs)
        zerr.notice(`interface detected: ${name} (${addr})`);
    E.devs = ifs;
    E.dev_type = dev_type;
    zerr.notice(`detect_devs done`);
const binding_cli_call = (args, encoding, cb)=>
etask(function*binding_cli_call_(){
    if (!binding)
        throw new Error('missing zsvc binding');
    let buf;
    binding.cli(args, r=>{
        if (r.retval!=null)
            return this.continue(r);
        if (cb)
            cb(r);
        else
            buf = buf ? Buffer.concat([buf, r.out]) : Buffer.from(r.out);
    });
    let r = yield this.wait();
    let out;
    // XXX vladimir: this means we are missing buf and returning only the
    // final value (to preserve compatibility)
    if (r.out)
        out = encoding ? r.out.toString(encoding) : r.out;
    else if (buf)
        out = encoding ? buf.toString(encoding) : buf;
    return {out, err: r.err, retval: r.retval};
const getprop = key=>etask(function*(){
    let {retval, out, err} = yield binding_cli_call(['getprop', key], 'utf8');
    if (retval)
        throw new Error(string.chomp(err));
    return string.chomp(out);
const detect_android_sdk_ver = ()=>etask(function*(){
    try {
        let res = yield getprop('ro.build.version.sdk');
        E.android_sdk_ver = +res;
        zerr.notice(`Android SDK ${E.android_sdk_ver} detected`);
    } catch(e){
        zerr(`Cannot detect Android SDK version: ${e}`);
        E.android_sdk_ver = -1;
    }
const tunnel_init = (msg, conn, cid)=>{
    let conn_opt = conn.conn_opt||{};
    if (msg)
    {
        assign(global_opt, msg);
        if (msg.blocked_ip)
            set_blocked_ip(global_opt.blocked_ip);
    }
    E.t.detect_devs();
    let info = ip_type(conn.local_addr, E.devs, E.dev_type);
    zerr.notice('js peer init from '+conn.local_addr+' '+
        (info ? info.name+' '+(info.type||'unknown') : ''));
    if (!info)
    {
        zerr('connection info unknown %s %O %O', conn.local_addr, E.devs,
            E.dev_type);
    }
    let _db = get_db_cache();
    zerr.info('db: '+zerr.json(_db));
    return {version: zconf.ZON_VERSION, idle, confdir,
        arch: os.arch(), release: os.release(), old_cid: get_old_cid(),
        makeflags: process.zon&&process.zon.makeflags, t_country: E.t.country,
        platform: get_platform(), apkid, appid, type: info&&info.type,
        ifname: info&&info.name, mobile_enable, cid, android_ver, usage,
        premium: is_plus, android_sdk_ver: E.android_sdk_ver, partnerid,
        user_token, gw_ip, ifs: E.devs, dev_type: E.dev_type,
        client_conf, db: _db, sdk_version, agree_ts, uuid, myip: conn_opt.myip,
        proxy: conn_opt.proxy && zutil.pick(conn_opt.proxy, 'host', 'port')};
const conf_read = ()=>{
    if (process.zon && process.zon.js_conf)
    {
        try {
            conf = JSON.parse(
                Buffer.from(process.zon.js_conf, 'base64').toString());
            zerr.notice('js_conf found %O', conf);
        } catch(e){ zerr('failed conf_read '+zerr.e2s(e)); }
    }
    try {
        let zid = file.read(confdir+'/db/z.id')||
            file.read(confdir+'/db/hola.id')||'';
        let ids = attrib.to_obj(attrib.from_str(zid));
        if (ids.android_ver)
            android_ver = ids.android_ver;
        if (!ids.js_conf)
            return;
        conf = JSON.parse(Buffer.from(ids.js_conf, 'base64').toString());
        zerr.notice('js_conf found %O', conf);
    } catch(e){ zerr('failed conf_read '+zerr.e2s(e)); }
const cli_exec = msg=>etask(function*(){
    let {retval, out, err} = yield binding_cli_call(msg.args, 'utf8');
    return {retval, out, err};
const wget = (_url, fpath, opt)=>etask(function*(){
    opt = opt||{};
    const wreq = zwget.stream(assign({}, opt, {url: _url}));
    let ts = Date.now(), et = etask.wait(), fstream;
    let error_handler = e=>{
        zerr('wget error: %s', e);
        zerr.perr('wget_error', {err: ``+e, url: _url, fpath});
        if (file.exists(fpath))
            file.unlink(fpath);
        et.throw(e);
    };
    if (opt.hola_bw_file)
        fs.createReadStream(opt.hola_bw_file).pipe(wreq);
    wreq.on('error', e=>{
        if (fstream)
            fstream.destroy(e);
        error_handler(e);
    }).on('response', resp=>{
        if (resp.statusCode!=200)
        {
            return error_handler(`Unexpected status code: ${resp.statusCode} `+
                `(${resp.statusMessage})`);
        }
        fstream = fs.createWriteStream(fpath);
        fstream.on('error', error_handler).on('close', ()=>{
            if (!file.exists(fpath))
                return error_handler('File missing on close');
            let fsize = file.stat_e(fpath).size, dur = Date.now()-ts;
            let sha1sum = file.hashsum_e(fpath, 'sha1'), bw_elapsed;
            let answer = {fsize, dur, sha1sum};
            if (bw_elapsed = resp.headers &&
                resp.headers['x-hola-bwgen-elapsed'])
            {
                answer.elapsed = bw_elapsed;
            }
            zerr.notice('downloaded %s (size: %s, dur: %sms)', fpath, fsize,
                dur);
            zerr.perr('wget_success');
            return void et.return(answer);
        });
        return void wreq.pipe(fstream);
    });
    return yield et;
E.perr_wakeup = ()=>{
    client_perr.process_fs();
// XXX volodymyr: fix in file.readdir_r_e
const escape_regexp = s=>s.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&');
const readdir_r_e = (dir, opt)=>{
    dir = file.normalize(dir).replace(/\/+$/, '');
    const strip = new RegExp(`^${escape_regexp(dir)}/`);
    return file.find_e(dir, assign({strip}, opt));
const peer_logs = ({action='list', name, compress})=>etask(function*logs_(){
    let idx, filename;
    const log_filter_regex = /\.(dmp|log|log\.1|jslog)$/;
    const get_log_filename = name=>`${confdir}/${name}`;
    const res = {action, name};
    if (typeof action!='string' || name && typeof name!='string')
    {
        res.err = 'invalid request';
        return res;
    }
    try {
        zerr.notice('checking logs under: '+confdir);
        const files = readdir_r_e(confdir)
            .filter(f=>log_filter_regex.test(f));
        zerr.notice('log files: %s', JSON.stringify(files));
        if (action=='list')
        {
            res.files = files.reduce((r, v)=>{
                const f = get_log_filename(v);
                try { r[v] = {size: file.size_e(f), mtime: file.mtime_e(f)}; }
                // eslint-disable-next-line
                catch(_e){ zerr.err(`stat ${f} failed: `+zerr.e2s(_e)); }
                return r;
            }, {});
            zerr.notice('log res.files: %s', JSON.stringify(res.files));
        }
        else if (!name)
            res.err = 'no name specified';
        else if ((idx = files.indexOf(name))<0)
            res.err = `file ${name} is not listed`;
        else if (!file.exists(filename = get_log_filename(files[idx])))
            res.err = `file ${name} does not exist`;
        else if (action=='fetch')
        {
            let data = file.read_e(filename, null);
            if (compress)
            {
                const w = etask.wait();
                zlib.deflate(data, (err, b)=>{
                    if (err)
                        res.err = err.message||err;
                    else
                        res.data = b;
                    w.continue();
                });
                yield w;
            }
            else
                res.data = data;
        }
        else if (action=='rm')
            file.unlink_e(filename);
        else
            res.err = `unsupported action: ${action}`;
    } catch(e){
        zerr('peer_logs err '+zerr.e2s(e));
        res.err = e.message||'unknown error';
    }
    return res;
// zs-proxyjs-hola..11 (inclusive), total 11
let fallback_ips = qw`3.210.106.128 3.216.157.50 3.217.131.101 54.90.50.83
    3.217.12.237 3.211.186.177 3.215.198.168 3.217.85.237 3.214.232.36
    3.217.21.103 3.226.207.59`;
let proxy_urls = [
    'wss://proxyjs.hola.org:443',
    'wss://proxyjs.h-vpn.org:443',
    'wss://proxyjs.svd-cdn.com:443',
    'wss://proxyjs.hola.org:7010',
    'wss://proxyjs.h-vpn.org:7010',
    'wss://proxyjs.svd-cdn.com:7010',
// [host1, ip1], [host1, ip2]
// [host2, ip3], [host2, ip4]
// ... fallback
// [host1, ip5], [host2, ip6]
const conn_open_single = (dev, fallback_ts)=>etask(function*(){
    // XXX vladimir: should never happen
    this.on('uncaught', e=>zerr('conn_open_single fail '+zerr.e2s(e)));
    let url_idx = 0, url_try_count = 0, next_wait = 10*ms.SEC;
    let force_ip, force_host, force_port, fallback_resolve;
    for (;;)
    {
        let proxy_url, proxy_host, port;
        if (url_idx>=proxy_urls.length)
            fallback_resolve = true;
        if (fallback_resolve)
            url_idx = rand_uniform.rand_range(0, proxy_urls.length);
        proxy_url = proxy_urls[url_idx];
        proxy_host = force_host||url.parse(proxy_url).hostname;
        port = force_port||url.parse(proxy_url).port||443;
        zerr.notice(`conn_open_single ${proxy_host} ${port} `+
            `${fallback_resolve}`);
        let ips;
        if (force_ip)
            ips = [{address: force_ip, family: 4}];
        else if (fallback_resolve)
            ips = fallback_ips.map(ip=>({address: ip, family: 4}));
        else
        {
            try { ips = yield resolve_addr(proxy_host); }
            catch(e){}
            if (!ips || !ips.length)
            {
                url_idx++;
                url_try_count = 0;
                zerr(`conn_open_single failed resolve ${proxy_host}`);
                continue;
            }
        }
        let ip = rand_uniform.rand_element(ips);
        let open_res = yield proxyjs_client_open_dev_once(dev,
            {server: ip.address, port, servername: proxy_host, fallback_ts});
        force_ip = force_host = force_port = undefined;
        if (open_res && open_res.last_success)
        {
            fallback_ts = 0; // use only once
            fallback_resolve = false;
            url_try_count = 0;
            next_wait = global_opt.proxy_retry_interval;
            let redir = open_res.main_conn && open_res.main_conn.redirect_req;
            if (redir)
            {
                force_ip = redir.ip;
                force_host = redir.host;
                force_port = redir.port;
            }
            else // first retry on success should be to same proxyjs server
            {
                force_ip = ip.address;
                force_host = proxy_host;
                force_port = port;
            }
        }
        else
        {
            url_try_count++;
            if (url_try_count>2 || url_try_count>=ips.length)
                url_idx++;
        }
        zerr.info(`conn_open_single sleeping ${next_wait} to next check`);
        yield etask.sleep(next_wait);
        next_wait = Math.min(next_wait*2, global_opt.proxy_retry_max);
    }
// based on connectivity test results, client will open WS conn to proxyjs
// server via ip address (direct) or via zagent (http proxy).
const conn_open_single2 = dev=>etask(function*(){
    this.on('uncaught', e=>zerr('conn_open_single2 fail '+zerr.e2s(e)));
    let force_conn_opt, try_count = 0, next_wait = 10*ms.SEC;
    for (;;)
    {
        let ips, conn_opt, cc_res;
        if (try_count>10)
        {
            zerr('conn_open_single2 too many retries without success, '+
                'falling back to conn_open_single');
            zerr.perr(`vpn.svc.${pprefix}open_fallback`, {},
                open_fallback_perr_opt());
            return yield conn_open_single(dev, Date.now());
        }
        else if (force_conn_opt)
            conn_opt = force_conn_opt;
        else if ((cc_res =
            yield conn_mgr.run_connectivity_check(['proxyjs'], dev))
            && cc_res[0].proxy && cc_res[0].myip)
        {
            conn_opt = {server: cc_res[0].selected, port: cc_res[0].port||443,
                proxy: {host: cc_res[0].proxy.ip, port: cc_res[0].proxy.port,
                secureProxy: true}, myip: cc_res[0].myip};
        }
        else
        {
            let host = cc_res && cc_res[0].selected || 'proxyjs.hola.org';
            let port = cc_res && cc_res[0].port || 443;
            try { ips = yield resolve_addr(host); }
            catch(e){}
            if (!ips || !ips.length)
            {
                zerr(`conn_open_single2 failed resolve ${host} `+
                    `test ${!!cc_res} try ${try_count}`);
                try_count++;
                continue;
            }
            conn_opt = {
                server: rand_uniform.rand_element(ips).address,
                port: port,
                servername: host,
            };
        }
        zerr.notice(`conn_open_single2 ${conn_opt.server} ${conn_opt.port} `+
            `${conn_opt.proxy ? 'proxy via '+conn_opt.proxy.host : 'direct'} `+
            `${force_conn_opt ? 'force ' : ''}n${try_count}`);
        let open_res = yield proxyjs_client_open_dev_once(dev, conn_opt);
        force_conn_opt = undefined;
        if (open_res && open_res.last_success)
        {
            try_count = 0;
            next_wait = global_opt.proxy_retry_interval;
            let redir = open_res.main_conn && open_res.main_conn.redirect_req;
            if (redir)
            {
                force_conn_opt = {server: redir.ip, port: redir.port,
                    servername: redir.host};
            }
            else // first retry on success should be to same proxyjs server
                force_conn_opt = conn_opt;
        }
        else
            try_count++;
        zerr.notice(`conn_open_single2 sleeping ${next_wait} to next check `+
            `(try ${try_count})`);
        yield etask.sleep(next_wait);
        next_wait = Math.min(next_wait*2, global_opt.proxy_retry_max);
    }
class Device extends events.EventEmitter {
    constructor(name, addr){
        super();
        this.name = name;
        this.addr = addr;
        this.state = 'unknown';
        this.sp = etask.wait();
        this.sp.finally(()=>this.close());
        this.on('connected', this._on_connected);
        this.on('disconnected', this._on_disconnected);
        this.on('wait_test', this._on_wait_test);
        this.next_wait = 10*ms.SEC;
        this.test();
    }
    test(){
        let _this = this;
        this.state = 'test';
        this.sp.spawn(etask(function*(){
            for (let site of global_opt.http_test_sites)
            {
                try {
                    let parts = url.parse(site.url);
                    let orig_host = parts.host;
                    if (!zurl.is_ip(parts.hostname))
                    {
                        let ip = site.ip;
                        try {
                            let addr = yield resolve_addr(parts.host);
                            if (addr && addr.length)
                                ip = addr[0].address;
                        } catch(e){}
                        parts.hostname = ip;
                        parts.host = null;
                    }
                    let res = yield zwget({url: url.format(parts), slow: false,
                        timeout: ms.MIN, localAddress: _this.addr,
                        headers: {host: orig_host, 'user-agent': undefined}});
                    if (res.body.match(site.match))
                    {
                        zerr.notice(`${_this.name} matched ${site.url}`);
                        return _this.set_state('connected');
                    }
                } catch(e){ }
            }
            return _this.set_state('wait_test');
        }));
    }
    set_state(state){
        var prev_state = this.state;
        this.state = state;
        this.emit(state, prev_state);
    }
    _on_connected(){
        this._internet_inc(1);
        zerr.notice('dev %s connected (has_internet=%d)', this.name,
            E.has_internet);
    }
    _on_disconnected(prev_state){
        if (prev_state!='connected')
            return;
        this._internet_inc(-1);
        zerr.notice('dev %s disconnected (has_internet=%d)', this.name,
            E.has_internet);
    }
    _on_wait_test(){
        let _this = this;
        let wait_ms = this.next_wait;
        zerr.notice(`${this.name} sleeping ${wait_ms} to next check`);
        this.next_wait = Math.min(this.next_wait*2, ms.MIN*30);
        this.sp.spawn(etask(function*(){
            yield etask.sleep(wait_ms);
            _this.test();
        }));
    }
    _internet_inc(delta){
        E.has_internet += delta;
        model.set('client_status.has_internet', E.has_internet>0);
    }
    close(){
        this.set_state('disconnected');
    }
// main connection to zs-proxyjs
class Main_conn {
    constructor(client, name){
        this.client = client;
        this.name = name;
        this.cid = undefined;
        this.client.data = this;
        this.redirect_req = undefined;
    }
    tunnel_init(msg){ return tunnel_init(msg, this.client); }
    status_get(msg){ return status_get(msg); }
    tunnel_redirect(msg){
        if (!msg)
            return;
        zerr.notice(`${this.name} tunnel_redirect ${msg.ip}`);
        this.redirect_req = msg;
        this.close();
    }
    connect(msg){ return client_connect(msg, this.client, this.cid); }
    cid_set(msg){
        zerr.notice(`${this.name} cid_set ${msg.cid}
            session_key ${msg.session_key}`);
        this.cid = E.cid_js = msg.cid;
        model.merge('client_status', {cid: E.cid_js});
        file.write(`${confdir}/${get_exe()}.jscid`, msg.cid);
    }
    cli_exec(msg){
        if (!Array.isArray(msg.args) || !msg.args.length ||
            !msg.args.every(a=>typeof a=='string'))
        {
            return;
        }
        zerr.notice(`${this.name} cli_exec ${msg.args.join(',')}`);
        return cli_exec(msg);
    }
    wget(msg){
        if (typeof msg.url!='string' || typeof msg.fpath!='string')
            return void zerr('malformed wget request: %O', msg);
        zerr.notice(`${this.name} wget ${msg.url} -> ${msg.fpath}`);
        return wget(msg.url, msg.fpath, msg.opt);
    }
    logs(msg = {}){
        zerr.notice(`${this.name} logs ${msg.action} ${msg.name}`);
        return peer_logs(msg);
    }
    upgrade(msg){
        zerr.notice(`${this.name} upgrade ${msg.cmd}`);
        return client_upgrade.run(msg);
    }
    perr_process(){
        zerr.notice(`${this.name} perr_process`);
        return client_perr.process_fs();
    }
    db_set(msg){ return db_set(msg); }
    db_get(msg){ return db_get(msg); }
    close(){
        this.client.close();
    }
const proxyjs_client_open_dev_once = (dev, conn_opt)=>etask(
function*(){
    let {name, addr} = dev||{}, disconnected = false, agent;
    let {server, port, servername, proxy, fallback_ts} = conn_opt;
    let ipc_server = {
        all: ['tunnel_init', 'tunnel_redirect', 'status_get', 'connect',
            'cid_set', 'perr_process', 'wget'],
        win: ['cli_exec', 'db_set', 'db_get', 'upgrade', 'logs'],
        mac: ['cli_exec'],
    };
    if (!dev)
        name = 'single_conn';
    if (!proxy)
        agent = new https.Agent({localAddress: addr, servername: servername});
    zerr.notice(`proxyjs_client_open_dev_once ${name} ${addr} ${port} `+
        `${servername}`);
    let client = new zws.Client(`wss://${server}:${port}`, {
        label: 'proxy_'+name, agent, proxy,
        ping_interval: global_opt.proxy_ping_interval,
        ping_timeout: global_opt.proxy_ping_timeout,
        handshake_timeout: global_opt.proxy_handshake_timeout,
        ipc_server: ipc_server.all.concat(is_win ? ipc_server.win :
            is_mac() ? ipc_server.mac : []),
        ipc_zjson: true,
        zjson_opt: {date: true, re: true},
        mux: true,
    });
    let inc_connected = delta=>{
        E.connected += delta;
        model.set('client_status.connected', E.connected>0);
    };
    let last_success, main_conn;
    client.conn_opt = conn_opt;
    client.on('connected', ()=>{
        inc_connected(1);
        last_success = true;
        zerr.notice(`proxy connected (conn_cnt=${E.connected})`);
        if (fallback_ts)
        {
            zerr.perr(`vpn.svc.${pprefix}open_fallback_success`,
                {after: Date.now()-fallback_ts}, open_fallback_perr_opt());
        }
    });
    client.on('disconnected', ()=>{
        if (last_success)
            inc_connected(-1);
        zerr.notice(`proxy disconnected (conn_cnt=${E.connected})`);
        client.close();
        // hack - assume a disconnect means devices changed
        if (dev)
            E.bump_dev_generation();
        disconnected = true;
        this.return({last_success, main_conn});
    });
    this.finally(()=>{
        if (!disconnected)
            client.close(-2, 'shutdown');
        if (agent)
            agent.destroy();
        // XXX volodymyr: any uninit for proxy-over-zagent connection?
    });
    main_conn = new Main_conn(client, name);
    yield this.wait();
// XXX volodymyr: possible multiple simultaneous conns when several devs are
// feasible at the same time, is that on purpose?
const dev_init = opt=>{
    let dev = new Device(opt.name, opt.addr);
    dev.on('connected', ()=>dev.sp.spawn(conn_open_single2(opt)));
    return {enabled: true, name: opt.name, sp: dev.sp, addr: opt.addr};
// XXX colin/shachar: make event driven on changes of devices
const dev_monitor = ()=>etask(function*(){
    let set, devs = {};
    this.finally(()=>set&&setdb.off(set));
    set = setdb.on('devs_updated', ()=>{
        zerr.notice('devs updated start');
        try { E.t.detect_devs(); }
        catch(e){}
        if (!E.devs)
            return void zerr('no interfaces detected');
        for (let name in devs)
            devs[name].enabled = false;
        for (let {name, addr} of E.devs)
        {
            if (devs[name] && devs[name].addr!=addr)
            {
                zerr.notice(`dev ${name} change ${devs[name].addr}->${addr}`);
                let dev = devs[name];
                dev.sp.return();
                devs[name] = undefined;
            }
            if (devs[name])
            {
                devs[name].enabled = true;
                continue;
            }
            devs[name] = E.t.dev_init({name, addr});
            this.spawn(devs[name].sp);
        }
        for (let name in devs)
        {
            let dev = devs[name];
            if (dev.enabled)
                continue;
            zerr.notice(`dev ${name} disabled`);
            dev.sp.return();
            delete devs[dev.name];
        }
    });
    return yield etask.wait();
const open_fallback_perr_opt = ()=>{
    let send_log = E.get_rmt_conf('conn_open_fallback_log');
    return {retry: true, filehead: send_log && zerr.log_tail(send_log)};
const bw_update = (type, dn, up)=>{
    let now = Date.now();
    let cur;
    type = type||'unknown';
    if (!tun_bw_count[type] ||
        !Array.isArray(tun_bw_count[type].daily) ||
        !tun_bw_count[type].daily.length)
    {
        tun_bw_count[type] = {daily: []};
    }
    let bw_use = tun_bw_count[type];
    if (!(bw_use.min<=now && bw_use.max>now))
    {
        let start = date.align(date(), 'DAY');
        let end = date.add(start, {day: 1});
        let month_ago = date.add(start, {day: -31});
        bw_use.min = +start;
        bw_use.max = +end;
        bw_use.daily.unshift({dn: 0, up: 0, start});
        bw_use.daily = bw_use.daily.filter(e=>e.start>month_ago);
    }
    cur = bw_use.daily[0];
    cur.dn += dn||0;
    cur.up += up||0;
    tun_bw_count.dirty = true;
const bw_db_path = ()=>confdir+'/db/bw.db';
const bw_store_init = ()=>{
    if (!confdir)
        return void zerr('could not update bw');
    bw_load();
    E.bw_store_et = etask.interval(ms.MIN, ()=>{
        if (!tun_bw_count.dirty)
            return;
        zerr.info('store bw db');
        tun_bw_count = _.omit(tun_bw_count, 'dirty');
        file.write(bw_db_path(),
            conv.JSON_stringify({version: 1, data: tun_bw_count},
            {date: true}));
    });
const bw_load = ()=>{
    if (!confdir)
        return void zerr('could not update bw');
    let bw_history, bw_count;
    try {
        bw_history = conv.JSON_parse(file.read(bw_db_path()));
        if (bw_history && bw_history.version==1)
            tun_bw_count = bw_count = bw_history.data;
    } catch(e){ zerr('bw read failed'); }
    if (!bw_count)
    {
        zerr.info('init bw count');
        tun_bw_count = {};
    }
const user_token_load = ()=>{
    if (get_platform()!='win32')
        return;
    if (!confdir)
        return;
    let token;
    try {
        token = JSON.parse(file.read(confdir+'/db/user_token.json'));
        user_token = (token||{}).token;
        zerr.info(`user_token_load ${user_token}`);
    } catch(e){ zerr('user_token_load failed'); }
const test_connectivity = ()=>{
    if (gw_ip!='0.0.0.0')
        conn_mgr.run_connectivity_check(['www', 'client', 'perr']);
let net_name = 'lum';
let hola_net_name = 'hola';
E.init = _gw_ip=>etask(function*(){
    full_vpn.gw_changed(gw_ip, _gw_ip);
    gw_ip = _gw_ip;
    if (E.started)
    {
        test_connectivity();
        return void E.bump_dev_generation();
    }
    process.on('exit', ()=>this.return());
    E.started = true;
    client_perr.init();
    if (!process.zon && env.CONFDIR)
        E.set_confdir(env.CONFDIR);
    conf_read();
    set_blocked_ip(global_opt.blocked_ip);
    if (is_android())
        yield detect_android_sdk_ver();
    if (conf && conf.disable_svc_js)
        return void zerr('disabling - js_conf');
    if (get_platform()=='win32' && !appid)
    {
        add_db_cache({name: 'svc.ver'});
        add_db_cache({name: 'svc_rmt_conf.json'});
        add_db_cache({name: 'updater.ver'});
    }
    if (conf && conf.fallback_ips)
        fallback_ips = conf.fallback_ips;
    bw_store_init();
    test_connectivity();
    ws_server.init();
    user_token_load();
    process.on('beforeExit', ()=>zerr.perr(`vpn.svc.${pprefix}quit`));
    zerr.perr(`vpn.svc.${pprefix}start`);
    if (is_updater())
        yield conn_open_single2();
    else
        yield dev_monitor();
E.set_remote_data = data=>{
    zerr.notice('set_remote_data '+JSON.stringify(data));
    E.remote_data = Array.isArray(data) ? data.slice(0, 100) : [];
E.add_remote_data = item=>{
    zerr.notice('add_remote_data '+JSON.stringify(item));
    let data = E.remote_data||[];
    data.push(item);
    if (data.length>100)
        data.shift();
    E.remote_data = data;
E.get_remote_data = ()=>E.remote_data||[];
// XXX volodymyr: mv all reg-related data to registry.js
const reg_set_value = (key, name, value)=>etask(function*(){
    let reg = new winreg({hive: winreg.HKLM, key});
    reg.set(name, winreg.REG_SZ, value, err=>{
        if (err)
            zerr.err(`reg_set_value ${key} ${name}: ${err.message}`);
        this.return();
    });
    yield this.wait();
const reg_get_value = (key, name)=>etask(function*(){
    let reg = new winreg({hive: winreg.HKLM, key});
    reg.get(name, (err, item)=>{
        if (err)
            zerr.err(`reg_get_value ${key} ${name}: ${err.message}`);
        this.return(item ? item.value : null);
    });
    yield this.wait();
const HOLA_REG_KEY = '\\Software\\Hola';
const ensure_uuid = ()=>etask(function*(){
    let res = yield reg_get_value(HOLA_REG_KEY, 'uuid');
    if (!res || res.startsWith('sdk-win'))
    {
        let old_uuid = res;
        res = 'win-'+crypto.randomBytes(16).toString('hex');
        yield reg_set_value(HOLA_REG_KEY, 'uuid', res);
        zerr.perr('vpn.svc.new_uuid', {uuid: res});
        // XXX volodymyr/nikita: rm when no perrs
        if (old_uuid)
            zerr.perr('vpn.svc.reset_uuid', {old_uuid, uuid: res});
    }
    return res;
const check_agree = ()=>etask(function*(){
    let agree_ts = yield reg_get_value(HOLA_REG_KEY, 'agree_ts');
    if (!agree_ts)
        return;
    let ts = Number.parseInt(agree_ts)||0;
    E.set_agree_ts(ts);
    zerr.notice(`set_agree_ts ${ts}`);
    let agree_sent = yield reg_get_value(HOLA_REG_KEY, 'agree_sent');
    if (agree_sent)
        return;
    yield zerr.perr('vpn.svc.agree', {ts: agree_ts}, {is_vpn: true,
        retry: true});
    yield reg_set_value(HOLA_REG_KEY, 'agree_sent', 1);
const check_last_plus = ()=>etask(function*(){
    if (is_plus!==undefined)
        return;
    let last_plus = yield reg_get_value(HOLA_REG_KEY, 'ui_last_premium');
    last_plus = !!(Number.parseInt(last_plus)||0);
    E.set_premium_mode(last_plus);
    unblocker_lib.set_user_plus(last_plus);
const check_after_update = ()=>etask(function*(){
    let after_update = !!(yield reg_get_value(HOLA_REG_KEY, 'after_update'));
    E.set_after_update(after_update);
    yield reg_set_value(HOLA_REG_KEY, 'after_update', 1);
    check_first('run');
// verify first_time events (e.g. successful hola browser start etc)
const check_first = name=>{
    let first_run = after_update==false; // ignore early calls (when undefined)
    if (is_win && first_run && !is_updater() && !check_first[name])
        zerr.perr(`vpn.svc.first_${name}`, {}, {retry: true});
    check_first[name] = true;
const check_start_loop = ()=>etask(function*(){
    this.on('uncaught', e=>zerr('check_start_loop failed: '+e));
    let prefix = is_updater() ? 'updater' : 'svc';
    let history_key = `${prefix}_start_history`;
    let val = (yield E.t.reg_get_value(HOLA_REG_KEY, history_key))||'';
    let history = val ? val.split(',') : [], now_ts = Date.now();
    if (history.find(e=>!/^\d+$/.test(e)))
        history = [];
    history.push(''+now_ts);
    yield E.t.reg_set_value(HOLA_REG_KEY, history_key,
        history.slice(-10).join(','));
    // now check if report required
    let restart_count = _.reduceRight(history, (memo, val)=>{
        if (!memo.stop)
        {
            if (!memo.last_val)
                memo.last_val = val;
            else if ((+memo.last_val)-(+val)<60*ms.SEC)
            {
                memo.last_val = val;
                memo.count++;
            }
            else
                memo.stop = true;
        }
        return memo;
    }, {count: 0}).count;
    if (restart_count<5)
        return;
    let report_key = `${prefix}_start_report_ts`;
    let report_ts = +(yield E.t.reg_get_value(HOLA_REG_KEY, report_key));
    if (report_ts && now_ts-(+report_ts)<60*60*1000) // max once per hour
        return;
    yield E.t.reg_set_value(HOLA_REG_KEY, report_key, now_ts);
    let log, log_len;
    if (log_len = +E.get_rmt_conf('start_loop_log', 64*1024))
    {
        try {
            log = confdir&&file.read(`${confdir}/log/${prefix}.log`)||'';
            if (log.length>log_len)
                log = log.slice(-log_len);
        }
        catch(e){ log = 'failed to attach logs'; }
    }
    zerr.perr('vpn.svc.start_loop', {history}, {filehead: log, retry: true});
const init_win = ()=>etask(function*(){
    if (!is_win)
        return;
    zerr.notice('init_win');
    if (!uuid)
        uuid = yield ensure_uuid();
    E.set_uuid(uuid);
    if (!is_updater())
        yield check_after_update();
    yield check_last_plus();
    yield check_agree();
    yield check_start_loop();
    yield client_upgrade.init();
    if (!after_install && !is_updater())
        yield util.cleanup_temp_dir();
E.install_api = api=>{
    // XXX volodymyr: introduce common module for all conf stuff
    full_vpn.init({
        get_rmt_conf: E.get_rmt_conf,
        get_gw: ()=>gw_ip,
        get_confdir: ()=>confdir,
        get_last_cid_js: ()=>E.cid_js||get_old_cid(),
        get_private_data,
        set_private_data,
        is_after_update: ()=>after_install&&after_update,
    });
    assign(api, full_vpn.api);
    assign(api, conn_mgr.api);
    assign(api, ws_server.api);
    assign(api, {init_win, check_agree});
E.t = {detect_devs, client_connect, status_get, set_blocked_ip,
    detect_android_sdk_ver, dev_monitor, dev_init, set_opt, set_proxy_url,
    set_fallback_ips, get_fallback_ips, wget, check_start_loop, reg_get_value,
    reg_set_value};
if (!module.parent)
    E.init();
