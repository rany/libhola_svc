// LICENSE_CODE ZON
'use strict'; /*zlint node:true*/
const zconf = require('../../../util/config.js');
const file = require('../../../util/file.js');
const efile = require('../../../util/efile.js');
const etask = require('../../../util/etask.js');
const exec = require('../../../util/exec.js');
const zwget = require('../../../util/wget.js');
const zerr = require('../../../util/zerr.js');
const date = require('../../../util/date.js');
const zutil = require('../../../util/util.js');
const data_model = require('./model.js');
const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const E = exports;
const get_temp_dir = ()=>{
    if (zutil.is_mocha())
        return '/tmp';
    // cannot use os.tmpdir(), it throws EPERM for some clients on write
    return path.join(data_model.get('app_info.confdir'), 'temp');
E.is_updater = ()=>data_model.get('app_info.is_updater');
E.is_root = ()=>etask(function*(){
    if (E.is_root.cache!==undefined)
        return E.is_root.cache;
    let ret = false;
    try {
        const svc_name = E.is_updater() ? 'hola_updater' : 'hola_svc';
        ret = !(yield exec.sys(['sc', 'query', svc_name], {out: 'retval'}));
    }
    catch(e){}
    return E.is_root.cache = ret;
E.is_process_running = match=>etask(function*(){
    if (!file.is_win)
        throw 'util.is_process_running not implemented';
    const argv = ['wmic', 'path', 'win32_process', 'get', 'name'];
    try {
        const out = yield exec.sys(argv, {out: 'stdout'});
        return !!out.split('\n').slice(1).map(f=>f.trim()).find(iter=>
            match instanceof RegExp ? match.test(iter) : match==iter);
    }
    catch(e){ zerr(`${process} is_running err `+zerr.e2s(e)); }
    return false;
E.get_last_cid = process=>{
    try {
        const confdir = data_model.get('app_info.confdir');
        return file.read(`${confdir}/${process}.jscid`);
    }
    catch(e){ zerr(`${process} jscid read err `+zerr.e2s(e)); }
E.download_stream = opt=>etask(function*(){
    const {cdn_list, name, sha1, dest, log, timeout, proxy_fn} = opt;
    const cdns = cdn_list.slice();
    const unlink = fname=>etask(function*(){
        if (yield efile.exists(fname))
            yield efile.unlink_e(fname);
    });
    log(`downloading ${name} from ${cdns.length} cdns to ${dest}`);
    if (sha1 && (yield efile.exists(dest)) &&
        file.hashsum_e(dest, 'sha1')==sha1)
    {
        return log(`file ${name} already exists`);
    }
    while (true)
    {
        const cdn = cdns.shift();
        if (!cdn)
        {
            log('no more cdn servers left');
            yield unlink(dest);
            throw 'download failed';
        }
        const url = cdn.endsWith('/') ? cdn+name : cdn+'/'+name;
        const proxy = proxy_fn ? proxy_fn(url) : undefined;
        log(`trying ${url} ${proxy ? 'via '+proxy : ''}`);
        yield unlink(dest);
        const fstream = fs.createWriteStream(dest)
        .on('error', err=>{
            log('write stream error: '+err);
            this.continue();
        })
        .on('finish', ()=>fstream.close(err=>{
            if (err)
            {
                log('failed to close file stream: '+err);
                return this.continue();
            }
            let calc_sha1;
            if (sha1 && (calc_sha1 = file.hashsum_e(dest, 'sha1'))!=sha1)
            {
                log(`sha1 checksum mismatch: `+
                    `calc(${calc_sha1})!=exp(${sha1})`);
                return this.continue();
            }
            log('download successfully completed');
            this.return();
        }));
        zwget.stream({url, proxy, timeout})
        .on('error', err=>{
            log('wget stream error: '+err);
            this.continue();
        })
        .on('response', resp=>{
            if (resp.statusCode!=200)
            {
                log(`unexpected status code: ${resp.statusCode}`);
                return this.continue();
            }
            log('headers found');
            resp.pipe(fstream);
        });
        yield this.wait();
    }
E.gen_temp_file = name=>path.join(get_temp_dir(), name);
E.cleanup_temp_dir = ()=>etask(function*_cleanup_temp_dir(){
    let temp_files;
    if (!(temp_files = yield efile.find(get_temp_dir())))
        return;
    let one_day_ago = date.monotonic()-date.ms.DAY;
    for (let name of temp_files)
    {
        let stat = yield efile.stat(name);
        if (stat && +stat.mtime<one_day_ago && +stat.ctime<one_day_ago)
            yield efile.unlink(name);
    }
// XXX volodymyr: organize into a File class with automatic encryption
const crypt_alg = 'aes-256-ctr', crypt_pass = 'eA2cS1gO';
E.encrypt = str=>{
    let cipher = crypto.createCipher(crypt_alg, crypt_pass);
    let res = cipher.update(str, 'utf8', 'hex');
    res += cipher.final('hex');
    return res;
E.decrypt = str=>{
    let decipher = crypto.createDecipher(crypt_alg, crypt_pass);
    let res = decipher.update(str, 'hex', 'utf8');
    res += decipher.final('utf8');
    return res;
