// LICENSE_CODE ZON
'use strict'; /*zlint node:true*/
const zconf = require('../../../util/config.js');
const unblocker_lib = require('../pub/unblocker_lib.js');
const date = require('../../../util/date.js');
const etask = require('../../../util/etask.js');
const exec = require('../../../util/exec.js');
const file = require('../../../util/file.js');
const hash = require('../../../util/hash.js');
const string = require('../../../util/string.js');
const zerr = require('../../../util/zerr.js');
const zescape = require('../../../util/escape.js');
const rate_limit = require('../../../util/rate_limit.js');
const zutil = require('../../../util/util.js');
const conn_mgr = require('./conn_mgr.js');
const model = require('./model.js');
const _ = require('lodash');
const os = require('os');
const E = exports, assign = Object.assign;
const is_win = os.platform().startsWith('win');
const err_meta = {
    645: {desc: 'auth_error', noreconnect: true, noretry: true},
    13801: {desc: 'cert_error', noreconnect: true, noretry: true},
    '*': {},
const get_err_meta = err=>err_meta[+err]||err_meta['*'];
// XXX volodymyr: wrap it up into a state machine
let connecting_to, reconnecting_to, connected_to, disconnecting;
let connecting_et, reconnecting_et, reconnecting_n, connecting_opt;
let connecting_ts, reconnecting_ts, connected_ts;
let last_error, last_error_text, connected_agent;
let last_rasdial_status;
const rasdial_conn = ()=>etask(function*(){
    let t0 = Date.now();
    let res = (yield exec.sys('rasdial', {out: 'stdout'}))||'';
    let m = res.match(/hola_vpn_(\w*)/);
    let country = m&&m[1];
    last_rasdial_status = {ts: t0, exec_dur: Date.now()-t0, out: res,
        connected_agent, country};
    return country;
const update_vpn_status = ()=>etask(function*(){
    // rasdial reports connected in the middle of connecting phase, ignore
    if (connecting_et||reconnecting_et)
    {
        vpn_model_update();
        return;
    }
    let new_connected_to = yield rasdial_conn();
    let last_connected_to = connected_to;
    connected_to = new_connected_to;
    if (last_connected_to && !connected_to && connected_ts)
    {
        zerr.perr('vpn_tunnel_disconnect_n', {country: last_connected_to,
            duration: ms_since(connected_ts), autodisconnect: true},
            {retry: true});
        connected_ts = 0;
        connected_agent = null;
        zerr.notice('vpn autodisconnected from %s, reconnecting...',
            last_connected_to);
        vpn_reconnect(last_connected_to);
        delayed_app_kill();
    }
    vpn_model_update();
const check_vpn_status_on_start = ()=>etask(function*(){
    if (is_vpn_supported() && (yield rasdial_conn()))
        monitor_vpn_status();
const ms_since = d=>date()-d;
let vpn_monitor;
const monitor_vpn_status = ()=>{
    vpn_monitor = vpn_monitor || etask.interval(1000, function*(){
        yield update_vpn_status();
    });
const ps_run = cmd=>etask(function*(){
    return yield exec.sys(['powershell', '-ExecutionPolicy', 'Bypass',
        '-NoLogo', '-NonInteractive', '-NoProfile', '-WindowStyle', 'Hidden',
        '-Command', cmd], {out: 'ret', stdout: true});
const vpn_agent_get = opt=>etask(function*(){
    let {country} = opt;
    let last_req = zutil.obj_pluck(vpn_agent_get, 'last_req')||{};
    let rmt_conf = conf_ops.get_rmt_conf();
    if (!opt.reconnect && opt.host.startsWith('zagent'))
    {
        zerr.perr('vpn_agent_get_manual', _.pick(opt, 'host', 'country'),
            {retry: true});
        return {host: opt.host, source: 'manual'};
    }
    if (rmt_conf.disable_vpn_agents_lib)
        return {host: opt.host, source: 'default'};
    try {
        let res = last_req.country!=country ?
            yield unblocker_lib.get_agents(null, {country}) :
            yield unblocker_lib.replace_agents({country}, [last_req.agent]);
        let agents = zutil.get(res, `verify_proxy.${country}`, []);
        let agent = zutil.get(agents[0], 'agent', {});
        if (!agent.host)
            throw {reason: 'no agents found', data: res};
        if (agent.type!='vpn')
        {
            // must never happen, but if it did reset the cache
            unblocker_lib.reset();
            throw {reason: 'non-vpn agents selected', data: res};
        }
        vpn_agent_get.last_req = {country, agent};
        zerr.perr('vpn_agent_get_lib', {country, agent}, {retry: true});
        // XXX volodymyr: strongswan is configured with certificate
        // issued for *.vpn.hola.org, need to fix /zgettunnels instead
        if (/zagent\d+\.hola\.org/.test(agent.host))
        {
           return {host: agent.host.replace('hola.org', 'vpn.hola.org'),
               source: 'lib'};
        }
        return {host: agent.host, source: 'lib'};
    } catch(e){
        let details = e&&e.reason ? e : {error: e};
        zerr.perr('vpn_agent_get_fallback', assign({country}, details),
            {retry: true, filehead: zerr.log_tail(128*1024)});
        return {host: opt.host, source: 'fallback'};
    }
// see manual for Set-VpnConnectionIPSecConfiguration powershell command,
// order is important, do not remove options, use '_unsupported' instead
const vpn_security_options = {
    integrity: ['md5_unsupported', 'sha1', 'sha256', 'sha384'],
    encryption: ['des_unsupported', 'des3', 'aes128', 'aes192', 'aes256'],
    dhgroup: ['none_unsupported', 'group1_unsupported', 'group2', 'group14',
        'ecp256_unsupported', 'ecp384_unsupported', 'group24'],
    cipher: ['des_unsupported', 'des3_unsupported', 'aes128_unsupported',
        'aes192', 'aes256', 'gcmaes128_unsupported', 'gcmaes192_unsupported',
        'gcmaes256_unsupported', 'none_unsupported'],
    ahtransform: ['md5_unsupported', 'sha1', 'sha256128_unsupported',
        'gcmaes128_unsupported', 'gcmaes192_unsupported',
        'gcmaes256_unsupported', 'none_unsupported'],
    pfs: ['none', 'pfs1', 'pfs2', 'pfs2048', 'ecp256', 'ecp384', 'pfsmm',
        'pfs24'],
const vpn_security_default = {integrity: 'sha1', encryption: 'aes256',
    dhgroup: 'group14', cipher: 'aes256', ahtransform: 'sha1', pfs: 'none'};
const vpn_policy_build = _conf=>{
    _conf = _conf||user_settings;
    const is_valid_conf = c=>_.every(_.keys(vpn_security_options), key=>{
        const group = vpn_security_options[key];
        return group.includes(c[key]) && !c[key].includes('unsupported');
    });
    const conf2code = c=>_.reduce(_.keys(vpn_security_options), (res, key)=>
        res+`0${vpn_security_options[key].indexOf(c[key])}000000`, '');
    const conf2debug = c=>_.reduce(_.keys(vpn_security_options), (res, key)=>
        res+(res.length ? ',' : '')+`${key}=${c[key]}`, '');
    if (!is_valid_conf(_conf))
    {
        zerr.perr('vpn_tunnel_invalid_security', {conf: _conf});
        zerr.notice('unknown security conf: '+JSON.stringify(_conf));
        return conf2code(vpn_security_default);
    }
    return {
        // example: 010000000400000003000000040000000100000000000000'
        code: conf2code(_conf),
        debug: conf2debug(_conf),
    };
const vpn_reconnect = country=>etask(function*(){
    if (reconnecting_et)
        return;
    reconnecting_et = this;
    reconnecting_to = country;
    reconnecting_ts = date();
    reconnecting_n = 1;
    this.finally(()=>{
        reconnecting_et = reconnecting_to = reconnecting_ts = null;
        reconnecting_n = 0;
    });
    for (;;)
    {
        while (conf_ops.get_gw()=='0.0.0.0')
            yield etask.sleep(0.5*date.ms.SEC);
        zerr.notice(`vpn reconnect to ${country} retry ${reconnecting_n}`);
        yield vpn_connect_one({reconnect: reconnecting_n++});
        if (connected_to)
            break;
        if (get_err_meta(last_error).noreconnect)
        {
            zerr.notice('vpn reconnect terminated, no retry for err=%s',
                last_error);
            last_error = null;
            last_error_text = null;
            break;
        }
        yield etask.sleep(10*date.ms.SEC);
    }
// static guid is needed to prevent windows from creating a new
// network profile each time we connect to vpn
const vpn_phonebook_conf = ({name, host})=>{
    if (conf_ops.get_rmt_conf('full_vpn.protocol')=='L2TP')
    {
        zerr.notice('using l2tp protocol configuration');
        return string.align`
            [${name}]
            PBVersion=5
            Type=2
            Guid=726B3F0D67F22249A4F516CFBAED84BF
            VpnStrategy=3
            IpPrioritizeRemote=1
            Ipv6PrioritizeRemote=1
            IpSecFlags=1
            MEDIA=rastapi
            Port=VPN3-0
            Device=WAN Miniport (L2TP)
            DEVICE=vpn
            PhoneNumber=${host}`;
    }
    let policy = vpn_policy_build();
    zerr.notice('using ikev2 protocol configuration: '+policy.debug);
    return string.align`
        [${name}]
        PBVersion=4
        Guid=5629C625B992485C810A48D3004E37D7
        Type=2
        VpnStrategy=7
        IpPrioritizeRemote=1
        Ipv6PrioritizeRemote=1
        NumCustomPolicy=1
        CustomIPSecPolicies=${policy.code}
        MEDIA=rastapi
        Port=VPN2-0
        Device=WAN Miniport (IKEv2)
        DEVICE=vpn
        PhoneNumber=${host}`;
let unblocker_lib_inited;
const vpn_init_unblocker_lib = ()=>{
    if (unblocker_lib_inited)
        return;
    // XXX volodymyr: svc lives long, still need to implement real storage
    unblocker_lib.fake_storage = {};
    unblocker_lib.init({
        perr: opt=>zerr.perr(opt.id, zutil.omit(opt, ['id']), {retry: true}),
        ajax: opt=>etask(function*(){
            let uri = zescape.uri(opt.url, opt.qs);
            let res = yield conn_mgr.wget_detect({url: uri, method: 'GET',
                timeout: opt.timeout, body: opt.data, json: true,
                expect: 200, retry: opt.retry});
            return res.resp.body;
        }),
        conf_get: (path, def)=>{
            let conf = conf_ops.get_rmt_conf(`unblocker_lib.${path}`, def);
            // XXX volodymyr: min_ver and % for svc controlled by peer proxy,
            // so mock as always-enabled, but need to unify conf approaches
            let min_ver_required = ['verify_proxy'];
            return min_ver_required.includes(path) ?
                assign({min_ver: zconf.ZON_VERSION, on: 100}, conf) : conf;
        },
        storage: {
            get: id=>unblocker_lib.fake_storage[id],
            set: (id, val)=>unblocker_lib.fake_storage[id] = val,
        },
        get_auth: ()=>{
            // XXX volodymyr: cid/session_key is dynamic and not reliable (e.g.
            // wont work between vpn reconnections), need to improve js conn
            // authentication method (for now using fake key)
            let cid = conf_ops.get_last_cid_js();
            return {product: 'svc', cid, session_key: hash.hash_string(cid)};
        },
        get_prod_info: ()=>({product: 'svc', svc_ver: zconf.ZON_VERSION}),
    });
    unblocker_lib_inited = true;
const vpn_connect_one = opt=>etask(function*(){
    opt = !opt.reconnect ? vpn_connect_one.last_opt = opt :
        assign({reconnect: opt.reconnect}, vpn_connect_one.last_opt);
    if (disconnecting)
        return;
    vpn_init_unblocker_lib();
    last_error = null;
    last_error_text = null;
    yield vpn_disconnect({internal: true});
    connecting_opt = opt;
    this.on('uncaught', e=>{
        zerr.notice('vpn connect exception '+zerr.e2s(e));
        zerr.perr('vpn_tunnel_connect_error_n', {country: opt.country,
            reconnect: opt.reconnect, auto: opt.auto,
            exception: zerr.e2s(e)}, {retry: true});
    });
    this.finally(()=>connecting_ts = connecting_opt = null);
    connecting_ts = date();
    zerr.perr('vpn_tunnel_connect_try_n', {country: opt.country,
        reconnect: opt.reconnect, auto: opt.auto}, {retry: true});
    let agent = yield vpn_agent_get(opt);
    let agent_lookup = ms_since(connecting_ts);
    let pbk_path = os.tmpdir()+'/hola_vpn.pbk';
    let name = 'hola_vpn_'+opt.country;
    try {
        let conf = vpn_phonebook_conf({name, host: agent.host});
        file.write_e(pbk_path, conf);
    } catch(e){
        zerr.err('pbk file error: '+zerr.e2s(e));
        return;
    }
    zerr.notice(`connecting to ${opt.country} vpn server: ${agent.host}`);
    disconnecting = false;
    let res = yield exec.sys(['rasdial', name, opt.username, opt.password,
        '/phonebook:'+pbk_path], {out: 'ret', stdout: true});
    last_error = res.status_code;
    let duration = ms_since(connecting_ts);
    if (last_error)
    {
        let m = res.stdout.match(/\d{3} - (.*)$/m);
        last_error_text = m ? m[1] : res.stdout;
        zerr.notice(`vpn connect error: ${last_error} ${last_error_text}`);
        zerr.perr('vpn_tunnel_connect_error_n', {country: opt.country,
            agent: agent.host, agent_source: agent.source, agent_lookup,
            duration, code: last_error, desc: last_error_text, auto: opt.auto,
            reconnect: opt.reconnect, connect: opt.connect}, {retry: true});
    }
    else
    {
        last_error = last_error_text = null;
        connected_to = opt.country;
        connected_agent = agent.host;
        connected_ts = date();
        zerr.notice(`vpn connected to ${agent.host}`);
        zerr.perr('vpn_tunnel_connect_n', {country: opt.country,
            agent: agent.host, agent_source: agent.source, agent_lookup,
            duration, auto: opt.auto, reconnect: opt.reconnect,
            connect: opt.connect}, {retry: true});
    }
    monitor_vpn_status();
    return agent;
const vpn_connect = opt=>etask(function*(){
    this.finally(()=>connecting_et = connecting_to = null);
    let agent;
    connecting_et = this;
    connecting_to = opt.country;
    for (let i = 1; i<=3; i++)
    {
        agent = yield vpn_connect_one(assign({connect: i}, opt));
        if (!agent)
            return;
        if (connected_to)
        {
            zerr.notice(`vpn connect succeeded after ${i} attempt`);
            break;
        }
        if (get_err_meta(last_error).noretry)
        {
            zerr.notice('vpn connect unsuccessfull: no retry for err=%s',
                last_error);
            break;
        }
        zerr.notice('vpn connect unsuccessfull: %s',
            i<3 ? 'try again '+i: 'give up');
        if (i<3)
            yield etask.sleep(date.ms.SEC);
    }
    return agent;
const vpn_disconnect = opt=>etask(function*(){
    opt = opt||{};
    last_error = null;
    last_error_text = null;
    yield update_vpn_status();
    // close reconnect loop only by external disconnect call
    if (!opt.internal && (connecting_et || reconnecting_et))
    {
        zerr.perr(`vpn_tunnel_connect_interrupt_n`, {
            country: reconnecting_to||connecting_to,
            duration: ms_since(reconnecting_ts||connecting_ts),
            reconnect: reconnecting_et ? reconnecting_n : undefined,
            auto: connecting_opt && connecting_opt.auto,
        }, {retry: true});
        if (connecting_et)
            connecting_et.return();
        if (reconnecting_et)
            reconnecting_et.return();
    }
    else if (connected_ts)
    {
        zerr.perr('vpn_tunnel_disconnect_n', {country: connected_to,
            duration: ms_since(connected_ts)}, {retry: true});
        connected_ts = 0;
    }
    if (disconnecting||!connected_to)
        return;
    disconnecting = true;
    this.finally(()=>disconnecting = false);
    let name = 'hola_vpn_'+connected_to;
    zerr.notice('disconnecting vpn: '+name);
    let res = yield exec.sys(['rasdial', name, '/d'],
        {out: 'ret', stdout: true});
    last_error = res.status_code;
    if (last_error)
    {
        let m = res.stdout.match(/\d{3} - (.*)$/m);
        last_error_text = m ? m[1] : res.stdout;
        zerr.notice('vpn disconnection error '+last_error+': '+
            last_error_text);
    }
    else
    {
        last_error = last_error_text = connected_to = connected_agent = null;
        zerr.notice('vpn disconnected');
        delayed_app_kill();
    }
    monitor_vpn_status();
const vpn_change_agent = ()=>etask(function*(){
    if (!connected_to)
        return;
    let last = {connected_to, connected_agent}, agent, ts0 = date();
    // reconnect will force-select a different agent
    zerr.notice('vpn agent change request, reconnecting...');
    if (!(agent = yield vpn_connect(vpn_connect_one.last_opt)))
        return; // cancelled
    zerr.perr(connected_to ? 'vpn_tunnel_agent_change_n' :
        'vpn_tunnel_agent_change_error_n', {country: last.connected_to,
        from: last.connected_agent, to: agent.host, duration: ms_since(ts0)});
const is_vpn_supported = ()=>is_win &&
    !!os.release().match(/^10|6\.3|6\.2|6\.1/);
const is_vpn_active = ()=>!!(connecting_to||reconnecting_to||connected_to);
const vpn_capabilities = ()=>{
    return {
        supported: is_vpn_supported(),
        security_options: is_win ?
            zutil.map_obj(vpn_security_options, (choices, key)=>
            choices.filter(val=>!val.includes('unsupported'))) : {},
    };
const vpn_status = calc_only=>{
    if (!is_vpn_supported())
        return {};
    if (!calc_only)
        monitor_vpn_status();
    // we silently attempt to reconnect without any feedback to the user
    if (reconnecting_et)
    {
        return {connected_to: reconnecting_to,
            reconnecting: !!reconnecting_et};
    }
    return connecting_to ? {connecting_to} :
        disconnecting ? {disconnecting} :
        connected_to ? {connected_to} :
        last_error ? {last_error, last_error_text} : {idle: true};
const vpn_model_update = is_on_start=>{
    model.set('vpn_status', vpn_status(is_on_start));
// XXX volodymyr: detect vpn_status bugs (broken sm)
const vpn_model_monitor = is_on_start=>{
    let rl = {}, updates = [];
    model.watch('vpn_status', data=>{
        let conf = assign({ms: date.ms.MIN, count: 10, last_updates: 100},
            model.get('rmt_conf.full_vpn.status_rl'));
        if (updates.length>=conf.last_updates)
            updates.shift();
        let now = Date.now();
        let last = updates.length ? updates[updates.length-1].ts : now;
        updates.push({ts: Date.now(), tdiff: now-last, status: data,
            rasdial: last_rasdial_status});
        if (rate_limit(rl, conf.ms, conf.count))
            return;
        zerr.perr('vpn.svc.vpn_status_loop', {updates}, {
            rate_limit: {count: 1, ms: date.ms.MIN},
            filehead: zerr.log_tail(128*1024),
            level: zerr.L.DEBUG, // avoid polluting log with self-dump
        });
    }, is_on_start);
vpn_model_update(true);
vpn_model_monitor(true);
let user_settings = assign({
    auto_connect: false,
    auto_connect_country: 'us',
    app_kill: false,
    app_kill_apps: [],
    app_kill_delay: 10*date.ms.SEC,
}, vpn_security_default);
const load_user_settings = ()=>{
    zerr.notice('load_user_settings');
    try {
        let settings = file.read(conf_ops.get_confdir()+
            '/db/user_settings.json');
        if (settings)
            assign(user_settings, JSON.parse(settings));
    } catch(e){ zerr.err('load_user_settings failed: '+zerr.e2s(e)); }
const vpn_get_settings = ()=>{
    zerr.notice('vpn_get_settings');
    return user_settings;
const vpn_set_settings = opt=>{
    zerr.notice('vpn_set_settings');
    try {
        opt = _.mapValues(opt, v=>JSON.parse(v));
    } catch(e){
        zerr.err('vpn_set_settings: '+zerr.e2s(e));
        return 'parse error';
    }
    let auth = _.pick(opt, 'username', 'password');
    let sett = _.omit(opt, 'username', 'password');
    assign(user_settings, sett);
    file.write(conf_ops.get_confdir()+'/db/user_settings.json',
        JSON.stringify(user_settings));
    zerr.notice('vpn_set_settings: confdir '+conf_ops.get_confdir());
    if (!_.isEmpty(auth))
        conf_ops.set_private_data(auth);
    return user_settings;
const vpn_auto_connect = ()=>etask(function*(){
    let country = user_settings.auto_connect_country;
    if (!user_settings.auto_connect || !country)
    {
        zerr.notice('auto connect disabled');
        return;
    }
    let auth_data = conf_ops.get_private_data();
    if (!auth_data)
    {
        zerr.err('no auth data');
        return;
    }
    if (connected_to || connecting_to)
    {
        zerr.notice('already '+(connected_to ? 'connected ' : 'connecting ')+
            'to '+(connected_to || connecting_to));
        return;
    }
    if (conf_ops.is_after_update())
    {
        let delay = conf_ops.get_rmt_conf('autoconnect.update_delay',
            60*date.ms.SEC);
        yield etask.sleep(delay);
    }
    country = country.toLowerCase();
    zerr.notice('auto connect to '+country);
    yield vpn_connect(assign({
        country,
        host: country+'.vpn.hola.org',
        auto: true,
    }, _.pick(auth_data, 'username', 'password')));
    zerr.perr('vpn_auto_connect',
        {country, success: !!connected_to, error: last_error}, {retry: true});
let app_kill_timeout;
const delayed_app_kill = ()=>{
    cancel_app_kill();
    app_kill_timeout = setTimeout(()=>{
        if (!connected_to)
            app_kill();
    }, user_settings.app_kill_delay);
const cancel_app_kill = ()=>{
    if (!app_kill_timeout)
        return;
    clearTimeout(app_kill_timeout);
    app_kill_timeout = null;
const app_kill = ()=>etask(function*(){
    let apps = user_settings.app_kill_apps;
    if (!user_settings.app_kill || !Array.isArray(apps) || !apps.length)
        return;
    let res = yield exec.sys(['wmic', 'process', 'get', 'name'],
        {out: 'ret', stdout: true});
    if (res.status_code)
    {
        zerr.err(`app_kill process get err ${res.status_code} ${res.stdout}`);
        zerr.perr('vpn_app_kill_error',
            {code: res.status_code, desc: res.stdout}, {retry: true});
        return;
    }
    let running = res.stdout.split(/\s+/).slice(1);
    apps = apps.filter(app=>running.includes(app))
        .map(app=>app.replace(/\.exe$/, ''));
    if (!apps.length)
        return;
    // XXX volodymyr: ps_run gets stuck on win7, verify and fix
    res = yield ps_run(`Stop-Process -Name ${apps.join(',')} -Force`);
    if (res.status_code)
    {
        zerr.err(`app_kill stop-process err ${res.status_code} ${res.stdout}`);
        zerr.perr('vpn_app_kill_error',
            {code: res.status_code, desc: res.stdout}, {retry: true});
        return;
    }
    zerr.notice('apps killed: '+apps.join(', '));
const vpn_ui_app_loaded = ()=>etask(function*(){
    zerr.notice('vpn_ui_app_loaded');
    if (!is_vpn_supported())
        return;
    yield vpn_auto_connect();
// XXX volodymyr: move common stuff into some conf base layer
let conf_ops;
E.init = ops=>{
    conf_ops = ops;
    check_vpn_status_on_start();
E.gw_changed = (from_gw, to_gw)=>{
    if (from_gw=='0.0.0.0' && connected_ts && connected_to)
        vpn_reconnect(connected_to);
E.set_confdir = confdir=>load_user_settings();
// exposed to C code
E.api = {
    vpn_ui_app_loaded,
    vpn_get_settings,
    vpn_set_settings,
    vpn_capabilities,
    vpn_connect,
    vpn_status,
    vpn_change_agent,
    vpn_disconnect,
    is_vpn_supported,
E.t = {vpn_model_monitor};
