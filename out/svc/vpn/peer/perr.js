// LICENSE_CODE ZON
'use strict'; /*zlint node:true*/
const zconf = require('../../../util/config.js');
const etask = require('../../../util/etask.js');
const date = require('../../../util/date.js');
const file = require('../../../util/file.js');
const zerr = require('../../../util/zerr.js');
const rate_limit = require('../../../util/rate_limit.js');
const zescape = require('../../../util/escape.js');
const conn_mgr = require('./conn_mgr.js');
const data_model = require('./model.js');
const os = require('os');
const E = exports, assign = Object.assign, ms = date.ms;
const data_model_get = (br, p)=>data_model.get(p ? `${br}.${p}` : br)||{};
const dm_app_info = path=>data_model_get('app_info', path);
const dm_client_status = path=>data_model_get('client_status', path);
const perr = (id, opt)=>etask(function*(){
    this.on('uncaught', e=>zerr('perr err '+zerr.e2s(e)));
    let {sdk_version, os_ver, appid, apkid, install_id, agree_ts, start_ts,
        connect_ts, uuid} = dm_app_info();
    let {cid} = dm_client_status();
    opt = opt||{};
    if (!opt.ts)
        opt.ts = Date.now();
    let ts = opt.ts;
    opt.ts = date.to_sql(opt.ts);
    let build = 'Version: '+zconf.ZON_VERSION+' '
        +(process.platform=='win32' ? 'win' : process.platform=='linux' ?
        'android' : process.platform)+' '+os.arch()+'\n'
        +'SDK version: '+sdk_version+'\n';
    if (process.zon)
    {
        build += 'Tag: '+process.zon.tag+'\n'
            +'Makeflags: '+process.zon.makeflags+'\n'
            +'Build date: '+process.zon.build_date+'\n';
    }
    build += 'Os version: '+os_ver+'\n';
    let body = assign({}, opt, {build, appid: appid||apkid, install_id,
        cid, agree_ts: agree_ts && agree_ts*1000, since_start: ts-start_ts,
        since_connect: ts-connect_ts});
    if (opt.filehead)
        body.filehead = opt.filehead.toString('base64');
    let path = zescape.uri('/perr', {
        id: id,
        file: opt.file,
        appid: appid||apkid,
        install_id: install_id,
        ver: zconf.ZON_VERSION,
        ver_sdk: sdk_version,
        uuid,
    });
    let res = yield conn_mgr.wget_perr(path, {method: 'POST', timeout: ms.MIN,
        body, json: true});
    zerr.notice('perr '+res.url);
    return res;
const perr_send_retry = (id, info, opt)=>{
    opt.retry_timeout = opt.retry_timeout ? opt.retry_timeout*2 : 1000;
    opt.retry_attempt = (opt.retry_attempt||0)+1;
    if (opt.retry_timeout<date.ms.MIN)
    {
        zerr.notice('perr %s: retry scheduled in %d', id, opt.retry_timeout);
        setTimeout(()=>perr_send(id, info, opt, true), opt.retry_timeout);
    }
    else
        zerr.notice('perr %s: too many retries, give up', id);
let perr_rl_ctx = {};
const perr_rate_limit = (id, conf)=>{
    let rl = perr_rl_ctx[id] = perr_rl_ctx[id]||{};
    return rate_limit(rl, conf.ms||date.ms.HOUR, conf.count||1);
let perr_order_num = 0;
const perr_send = (id, info, opt, retry)=>etask(function*(){
    this.on('uncaught', e=>{
        zerr('perr send err '+zerr.e2s(e));
        if (opt.retry)
            perr_send_retry(id, info, opt);
    });
    const {sdk_version, agree_ts, start_ts, connect_ts, confdir,
        is_android, is_updater, is_vpn, is_plus, after_install,
        after_update} = dm_app_info();
    const {cid, user_token} = dm_client_status();
    opt = opt||{};
    info = info||{};
    const version = opt.sdk ? sdk_version : zconf.ZON_VERSION;
    const sent_file = `${confdir}/perr_${id}_${version||''}.sent`;
    const sending_file = `${confdir}/perr_${id}_${version||''}.sending`;
    if (opt.once)
    {
        if (file.exists(sent_file) || file.exists(sending_file))
            return;
        file.touch(sending_file);
        this.finally(()=>file.unlink(sending_file));
    }
    if (id instanceof Error)
    {
        info = assign({}, info, {error: id});
        id = id.code||'error';
    }
    if (opt.rate_limit && !retry && !perr_rate_limit(id, opt.rate_limit))
        return;
    if (info instanceof Error)
        info = {error: info};
    if (info.error)
    {
        opt.filehead = opt.filehead || zerr.log_tail();
        opt.backtrace = opt.backtrace || zerr.e2s(info.error);
        info.error_msg = info.error.message;
        delete info.error;
    }
    let prefix = opt.sdk ? 'lum_sdk_' : 'net_svc_';
    // XXX shachar: add support for other OS
    prefix += is_android ? 'android' : 'win';
    if (is_vpn)
        prefix = 'svc';
    let ts = Date.now();
    info.user_token = user_token;
    info.is_plus = is_plus;
    info.order_num = info.order_num||++perr_order_num;
    info.agree_ts = agree_ts && agree_ts*1000;
    info.start_ts = start_ts;
    info.since_start = info.since_start||(ts-start_ts);
    info.since_connect = info.since_connect||(ts-connect_ts);
    if (opt.retry_attempt)
        info.retry = opt.retry_attempt;
    if (after_install)
    {
        info.install = 1;
        if (after_update)
            info.update = 1;
    }
    if (is_updater)
        info.is_updater = true;
    let _id = id.startsWith('vpn.') ? id : `${prefix}_${id}`;
    let res = yield perr(_id, {info, cid, filehead: opt.filehead,
        bt: opt.backtrace});
    let is_success = res && res.resp && +res.resp.statusCode==200;
    if (opt.once)
    {
        if (is_success)
            file.touch(sent_file);
        if (!file.exists(sent_file))
            zerr('perr resp error: %O', res&&res.resp);
    }
    if (!is_success && opt.retry)
        perr_send_retry(id, info, opt);
const perr_install = (perr_orig, pending)=>{
    while (pending.length)
        perr_send.apply(null, pending.shift());
    return (id, info, opt)=>{
        perr_orig(id, info, opt);
        return perr_send(id, info, opt);
    };
E.process_fs = ()=>etask(function*(){
    let confdir = dm_app_info('confdir');
    let dir = confdir+'/log';
    let files = file.readdir(dir);
    if (!files)
        return {err: 'not found'};
    if (!files.length)
        return {ok: true};
    let sent = 0, failed = 0;
    for (let f of files)
    {
        let m = f.match(/^((\d{8}_\d{6})_[a-z0-9_]+)\.log$/);
        if (!m)
            continue;
        let full_sent = dir+'/'+m[1]+'.sent';
        let errid = m[1].replace(/^[0-9]+_[0-9]+_(perr_)*/, '');
        let buf;
        // XXX volodymyr: drop lum perrs flushed to the same logdir on android
        if (errid.includes('lum_sdk_android_') || errid.includes('vpn_api_'))
            continue;
        if (file.exists(full_sent) || !(buf = file.read(dir+'/'+f, null)))
            continue;
        try {
            yield perr(errid, {file: f, filehead: buf.toString('base64'),
                ts: date.strptime(m[2], '%Y%m%d_%H%M%S')});
            file.touch(full_sent);
            sent++;
        } catch(e){
            zerr('perr send failed '+f+' '+zerr.e2s(e));
            failed++;
        }
    }
    return {ok: true, sent, failed};
E.init = ()=>zerr.perr_install(perr_install);
E.t = {perr_send};
