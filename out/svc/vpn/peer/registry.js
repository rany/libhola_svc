// LICENSE_CODE ZON
'use strict'; /*zlint node:true*/
const zconf = require('../../../util/config.js');
const file = require('../../../util/file.js');
const etask = require('../../../util/etask.js');
const zerr = require('../../../util/zerr.js');
const util = require('./util.js');
const winreg = file.is_win && require('winreg');
const E = exports;
const HOLA_REG_KEY = '\\Software\\Hola';
const HOLA_REG_NAMES = {
    version: {svc: 'version', updater: 'updater_version'},
    upgrade_counter: {svc: 'svc_upgrade_counter',
        updater: 'updater_upgrade_counter'},
const detect_role = ()=>util.is_updater() ? 'updater' : 'svc';
const reg_get_value = (name, def=null, role=detect_role())=>etask(function*(){
    let reg = new winreg({hive: winreg.HKLM, key: HOLA_REG_KEY});
    if (HOLA_REG_NAMES[name])
        name = HOLA_REG_NAMES[name][role];
    reg.get(name, (err, item)=>{
        if (err)
            zerr.err(`reg_get_value ${name}: ${err.message}`);
        this.return(item ? item.value : def);
    });
    yield this.wait();
const reg_set_value = (name, val, role=detect_role())=>etask(function*(){
    let reg = new winreg({hive: winreg.HKLM, key: HOLA_REG_KEY});
    if (HOLA_REG_NAMES[name])
        name = HOLA_REG_NAMES[name][role];
    reg.set(name, winreg.REG_SZ, val, err=>{
        if (err)
            zerr.err(`reg_set_value ${name}: ${err.message}`);
        this.return();
    });
    yield this.wait();
E.get_version = role=>reg_get_value('version', null, role);
E.set_version = (ver, role)=>reg_set_value('version', ver, role);
E.get_upgrade_counter = role=>etask(function*(){
    return +(yield reg_get_value('upgrade_counter', 0, role));
E.bump_upgrade_counter = role=>etask(function*(){
    let counter = yield E.get_upgrade_counter(role);
    reg_set_value('upgrade_counter', counter+1, role);
    return counter+1;
E.reset_upgrade_counter = role=>reg_set_value('upgrade_counter', 0, role);
