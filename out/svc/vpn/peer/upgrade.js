// LICENSE_CODE ZON
'use strict'; /*zlint node:true*/
const zconf = require('../../../util/config.js');
const zutil = require('../../../util/util.js');
const zerr = require('../../../util/zerr.js');
const date = require('../../../util/date.js');
const exec = require('../../../util/exec.js');
const etask = require('../../../util/etask.js');
const efile = require('../../../util/efile.js');
const file = require('../../../util/file.js');
const data_model = require('./model.js');
const conn_mgr = require('./conn_mgr.js');
const registry = require('./registry.js');
const util = require('./util.js');
const path = require('path');
const os = require('os');
const fs = require('fs');
const E = exports, assign = Object.assign;
const dump_filename = ()=>
    `hola_${util.is_updater() ? 'updater' : 'svc'}_upgrade.dump`;
class Progress {
    constructor(conf){
        this.conf = conf;
        this.get_source = ()=>this.conf.get('upgrade.source');
        this.get_target_role = ()=>this.conf.get('info.target.role');
        this.get_self_role = ()=>this.conf.get('info.self.role');
        this.get_from_ver = ()=>this.conf.get('info.target.version');
        this.get_to_ver = ()=>this.conf.get('upgrade.version');
        this.logs = [];
        this.stack = [];
        this.stack_path = ()=>this.stack.map(s=>s.step).join('.');
        this.history = [];
    }
    mute(){
        this.muted = true;
    }
    *step(name, fn){
        this.push(name);
        const ret = yield fn();
        yield this.pop();
        return ret;
    }
    push(name){
        this.stack.push({
            step: name,
            ts: date.monotonic(),
        });
        this.history.push({
            start: true,
            step: this.stack_path(),
            ts: date.monotonic(),
        });
        this.log('progress.enter '+this.stack_path());
    }
    *pop(){
        const progress_path = this.stack_path();
        this.log('progress.exit '+progress_path);
        this.history.push({
            end: true,
            step: progress_path,
            ts: date.monotonic(),
        });
        const item = this.stack.pop();
        yield this.report('progress', {
            step: progress_path,
            duration: date.monotonic()-item.ts,
        });
    }
    *start(){ yield this.report('start'); }
    *success(){
        yield this.report('success');
        yield registry.reset_upgrade_counter(this.get_target_role());
    }
    *failure(e){
        let root = this.stack[0]||{};
        yield this.report('failure', {
            interrupted_at: root.step,
            error: zerr.e2s(e),
            count: yield registry.bump_upgrade_counter(this.get_target_role()),
            history: this.history,
            conf: this.conf.dump(),
            log: this.logs,
        });
    }
    *report(name, info={}){
        if (this.muted)
            return;
        try {
            info = assign({
                ts: date.monotonic(),
                source: this.get_source(),
                from_ver: this.get_from_ver(),
                to_ver: this.get_to_ver(),
                updater: this.get_self_role(),
                target: this.get_target_role(),
            }, info);
            yield zerr.perr(`vpn.svc.client_upgrade_test.${name}`, info,
                {retry: true, rate_limit: false});
        } catch(e){}
    }
    log(str){
        if (this.muted)
            return;
        zerr.notice('client_upgrade: '+str);
        this.logs.push(str);
    }
    dump(){
        return zutil.clone_deep({stack: this.stack,
            history: this.history, logs: this.logs});
    }
    restore(data){
        this.stack = zutil.get(data, 'stack', []);
        this.history = zutil.get(data, 'history', []);
        this.logs = zutil.get(data, 'logs', []);
        if (!['local', 'remote'].includes(this.get_source()) ||
            !this.stack.length || !this.history.length || !this.logs.length ||
            this.stack.find(s=>!s.step||!s.ts) ||
            this.history.find(h=>!h.step||!h.ts))
        {
            throw 'corrupted progress - not restored';
        }
    }
class Conf {
    constructor(opt){
        this._conf = {upgrade: opt||{}};
    }
    get(path){
        if (path instanceof Array)
            return path.map(path=>zutil.get(this._conf, path));
        return zutil.get(this._conf, path);
    }
    set(path, val){ zutil.set(this._conf, path, val); }
    dump(){
        this.set('conf.dump_ts', date.monotonic());
        return zutil.clone_deep(this._conf);
    }
    restore(data){
        const self_role = util.is_updater() ? 'updater' : 'svc';
        this._conf = zutil.clone_deep(data);
        if (!(this.get('conf.dump_ts')-date.monotonic()<date.ms.HOUR))
            throw 'old conf - not restored';
        if (this.get('info.self.role')!=self_role)
            throw 'corrupted conf - not restored';
        this.set('conf.restore_ts', date.monotonic());
    }
class Upgrade {
    constructor(opt = {}){
        this.conf = new Conf(opt);
        this.progress = new Progress(this.conf);
        this.CG = this.conf.get.bind(this.conf);
        this.CS = this.conf.set.bind(this.conf);
        this.log = this.progress.log.bind(this.progress);
    }
    *prepare(){
        const {log, conf, CG, CS} = this;
        // initial conf
        if (CG('upgrade.source')=='local')
        {
            let qs = os.arch()=='x64' ? '?arch=x64' : '';
            let res = yield this.progress.step('pull_conf', ()=>
                conn_mgr.wget_www('/hola_setup.json'+qs, {expect: 200,
                json: true, retries: 3}));
            res = zutil.get(res, 'resp.body');
            CS('upgrade.version', zutil.get(res, 'hola.version'));
            CS('upgrade.setup_file', zutil.get(res, 'hola.file'));
            CS('upgrade.setup_sha1', zutil.get(res, 'hola.sha1'));
            CS('upgrade.chromium_file', zutil.get(res, 'chromium.file'));
            CS('upgrade.chromium_sha1', zutil.get(res, 'chromium.sha1'));
            CS('upgrade.cdn_list', zutil.get(res, 'cdn_list'));
            CS('upgrade.analytics', false);
            CS('upgrade.lum_sdk', false);
            if (!CG('upgrade.target'))
                CS('upgrade.target', 'self');
        }
        // current process info
        CS('info.self.role', util.is_updater() ? 'updater': 'svc');
        CS('info.self.version', zconf.ZON_VERSION);
        CS('info.self.exe_name', util.is_updater() ? zconf.UPDATER_EXE :
            zconf.SVC_EXE);
        CS('info.self.root', yield util.is_root());
        CS('info.self.uuid', data_model.get('app_info.uuid'));
        CS('info.self.cid', data_model.get('client_status.cid'));
        CS('info.self.failures', yield registry.get_upgrade_counter(
            CG('info.self.role')));
        // other process info
        CS('info.other.role', util.is_updater() ? 'svc' : 'updater');
        CS('info.other.version', yield registry.get_version(
            CG('info.other.role')));
        CS('info.other.exe_name', util.is_updater() ? zconf.SVC_EXE :
            zconf.UPDATER_EXE);
        CS('info.other.running', yield util.is_process_running(
            CG('info.other.exe_name')));
        CS('info.other.cid', util.get_last_cid(CG('info.other.exe_name')));
        CS('info.other.failures', yield registry.get_upgrade_counter(
            CG('info.other.role')));
        if (!CG('upgrade.target')) // peek command
            return;
        CS('info.target', CG('info.'+CG('upgrade.target')));
        // tmp data
        CS('tmpdir.setup_path', util.gen_temp_file(CG('upgrade.setup_file')));
        CS('tmpdir.chromium_path',
            util.gen_temp_file(CG('upgrade.chromium_file')));
        CS('tmpdir.dump_path', util.gen_temp_file(dump_filename()));
        zerr.info('using conf '+JSON.stringify(conf.dump(), null, 2));
    }
    *download(){
        const {CG, CS, log} = this;
        const _download = (cdn_list, name, sha1, dest)=>util.download_stream({
            cdn_list, name, sha1, dest, log,
            timeout: 30*date.ms.MIN,
            proxy_fn: url=>conn_mgr.hola_proxy(url),
        });
        yield this.progress.step('installer', ()=>_download(
            CG('upgrade.cdn_list'), CG('upgrade.setup_file'),
            CG('upgrade.setup_sha1'), CG('tmpdir.setup_path')));
        if (CG('info.target.role')=='svc')
        {
            yield this.progress.step('chromium', ()=>_download(
                CG('upgrade.cdn_list'), CG('upgrade.chromium_file'),
                CG('upgrade.chromium_sha1'), CG('tmpdir.chromium_path')));
        }
    }
    *install(){
        const {CG, log} = this;
        const setup_exe = CG('tmpdir.setup_path');
        const type = CG('info.self.role')+'>'+CG('info.target.role');
        const exec_assert = args=>etask(function*(){
            log('executing '+args.join(' '));
            const status = yield exec.sys(args, {out: 'retval',
                timeout: 10*date.ms.MIN});
            log(`completed ${status}`);
            if (status)
                throw 'installation failed';
        });
        if (!CG('upgrade.force_run') &&
            (yield util.is_process_running(/hola-setup/i)))
        {
            throw 'another installation is running';
        }
        const argv = [];
        switch (type) {
        case 'svc>svc':
            yield this.save();
            // fallthrough
        case 'updater>svc':
            argv.push(setup_exe, '--remote-install', '--no-updater',
                '--no-rmt-conf', '--force-kill', '--force-install-unfinished');
            if (CG('upgrade.lum_sdk'))
                argv.push('--lum-sdk');
            if (yield util.is_process_running(zconf.UI_EXE))
                argv.push('--ui-running');
            if (yield util.is_process_running(zconf.SVC_EXE))
                argv.push('--svc-running');
            if (!CG('info.self.root'))
                argv.push('--no-admin');
            if (!CG('upgrade.analytics'))
                argv.push('--no-analytics');
            argv.push('--hola-cr', '--hola-cr-path',
                CG('tmpdir.chromium_path'));
            yield exec_assert(argv);
            break;
        case 'svc>updater':
            argv.push(setup_exe, '--remote-install', '--no-svc',
                '--no-hola-cr', '--no-rmt-conf', '--force-kill');
            if (yield util.is_process_running(zconf.UPDATER_EXE))
                argv.push('--updater-running');
            if (!CG('info.self.root'))
                argv.push('--no-admin');
            if (!CG('upgrade.analytics'))
                argv.push('--no-analytics');
            yield exec_assert(argv);
            break;
        case 'updater>updater':
            yield this.save();
            argv.push(setup_exe, '--remote-install', '--no-svc',
                '--no-hola-cr', '--no-rmt-conf', '--updater-running',
                '--updater-no-restart');
            if (!CG('info.self.root'))
                argv.push('--no-admin');
            if (!CG('upgrade.analytics'))
                argv.push('--no-analytics');
            yield exec_assert(argv);
            argv.splice(argv.indexOf('--updater-no-restart'), 1,
                '--updater-only-restart');
            yield exec_assert(argv);
            break;
        }
    }
    *verify(){
        const [upgrade_version, upgrade_target, prev_cid, target_role,
            target_exe] = this.CG(['upgrade.version', 'upgrade.target',
            'info.target.cid', 'info.target.role', 'info.target.exe_name']);
        yield this.progress.step('check_version', ()=>etask(function*(){
            const new_version = upgrade_target=='self' ? zconf.ZON_VERSION :
                yield registry.get_version(target_role);
            if (new_version==upgrade_version)
                return;
            throw `version mismatch: ${new_version}!=${upgrade_version}`;
        }));
        yield this.progress.step('check_cid', ()=>etask(function*(){
            for (let i=0; i<6; i++)
            {
                yield etask.sleep(Math.pow(2, i+1)*date.ms.SEC);
                const next_cid = upgrade_target=='self' ?
                    data_model.get('client_status.cid') :
                    util.get_last_cid(target_exe);
                if (next_cid && next_cid!=prev_cid)
                    return;
            }
            throw 'client not connected to proxyjs';
        }));
    }
    *cleanup(){
        try {
            for (const path of ['tmpdir.setup_path', 'tmpdir.chromium_path',
                'tmpdir.dump_path'])
            {
                const fname = this.CG(path);
                if (yield efile.exists(fname))
                    yield efile.unlink_e(fname);
            }
        } catch(e){}
    }
    *save(){
        // best effort, not critical to complete
        try {
            const dump_file = this.CG('tmpdir.dump_path');
            if (yield efile.exists(dump_file))
                yield efile.unlink_e(dump_file);
            yield efile.write_e(dump_file, util.encrypt(JSON.stringify({
                conf: this.conf.dump(),
                progress: this.progress.dump(),
            })));
        }
        catch(e){ zerr('unable to save upgrade snapshot: '+e); }
    }
    *restore(){
        let result, dump_file = util.gen_temp_file(dump_filename());
        try {
            if (!(yield efile.exists(dump_file)))
                return;
            this.log('continue upgrade after restart');
            const snapshot = yield efile.read_e(dump_file);
            const data = JSON.parse(util.decrypt(snapshot));
            this.conf.restore(data.conf);
            this.progress.restore(data.progress);
            yield this.progress.pop(); // exit once-interrupted state
            result = true;
        }
        catch(e){ zerr('unable to read upgrade dump: '+e); }
        try { yield efile.unlink_e(dump_file); } catch(e){}
        return result;
    }
    *run(){
        try {
            const st = ['prepare', 'download', 'install', 'verify', 'cleanup'];
            yield this.progress.start();
            for (const name of st)
                yield this.progress.step(name, ()=>this[name]());
            yield this.progress.success();
        }
        catch(e){
            zerr.notice('upgrade-run exception: '+zerr.e2s(e));
            yield this.progress.failure(e);
            yield this.cleanup();
        }
    }
    *resume(){
        try {
            const st = ['verify', 'cleanup'];
            for (const name of st)
                yield this.progress.step(name, ()=>this[name]());
            yield this.progress.success();
        }
        catch(e){
            zerr.notice('client_upgrade: resume exception: '+zerr.e2s(e));
            yield this.progress.failure(e);
            yield this.cleanup();
        }
    }
    *peek(){
        try {
            this.progress.mute();
            yield this.prepare();
            const [self, other] = this.CG(['info.self', 'info.other']);
            const use_other = other.running && other.cid;
            return {
                upgrade_via: use_other ? other.role : self.role,
                using_cid: use_other ? other.cid : self.cid,
                info: {self, other},
            };
        }
        catch (e){
            zerr.notice('client_upgrade: peek exception '+zerr.e2s(e));
            return {error: 'peek_failed', info: zerr.e2s(e)};
        }
    }
E.run = msg=>etask(function*(){
    if (!file.is_win)
        return {error: 'os_not_supported'};
    const up = new Upgrade(msg.opt);
    if (msg.cmd=='peek')
        return yield up.peek();
    else if (msg.cmd=='run')
    {
        // upgrade may terminate its own process, caller must not wait
        etask(function*(){ yield up.run(); });
        return {started: true};
    }
    else
        return {error: 'unknown_cmd'};
E.init = ()=>etask(function*(){
    if (!file.is_win)
        return;
    const up = new Upgrade();
    if (yield up.restore())
        yield up.resume();
