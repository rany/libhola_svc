// LICENSE_CODE ZON
'use strict'; /*zlint node:true*/
const zconf = require('../../../util/config.js');
const etask = require('../../../util/etask.js');
const rand_uniform = require('../../../util/rand_uniform.js');
const wget = require('../../../util/wget.js');
const zurl = require('../../../util/url.js');
const zerr = require('../../../util/zerr.js');
const zutil = require('../../../util/util.js');
const unblocker_lib = require('../pub/unblocker_lib.js');
const model = require('./model.js');
const https = require('https');
const os = require('os');
const is_win = os.platform().startsWith('win');
const E = exports, assign = Object.assign;
let conn_tests = {
    www: {
        evaluating: false,
        selected: 'hola.org',
        path: '/conn_test',
        check_myip: true,
    },
    client: {
        evaluating: false,
        selected: 'client.hola.org',
        path: '/client_cgi/conn_test',
    },
    perr: {
        evaluating: false,
        selected: 'perr.hola.org',
        path: '/client_cgi/perr/conn_test',
    },
    proxyjs: {
        evaluating: false,
        selected: 'proxyjs.hola.org',
        ports: [443, 7010],
        path: '/ping',
        check_myip: true,
        parallel: true, // simultaneous tests possible (running per device)
    },
}, conn_tests_init = zutil.clone_deep(conn_tests);
const shuffle = list=>rand_uniform.rand_subset(list, list.length);
const build_agents = ()=>shuffle(zconf.SVC_AGENTS_POOL.split(',')).map(n=>{
    let m = n.split(':');
    return {ip: m[0], port: m[1], name: m[2]};
const cloud_agents = ()=>etask(function*(){
    let res, url = model.get('rmt_conf.conn_test_cloud_url',
        'https://www.dropbox.com/s/jemizcvpmf2qb9v/cloud_failover.conf?dl=1');
    try {
        res = yield wget(url);
        res = res.resp.body;
        res = res.substr(res.length-3)+res.substr(0, res.length-3);
        if (res.length%4)
            res += '='.repeat(4-res.length%4);
        res = Buffer.from(res, 'base64').toString();
        res = JSON.parse(res);
        cloud_agents.agents = shuffle(res instanceof Array ? res : res.agents);
    } catch(e){}
    return cloud_agents.agents||[];
const is_mode = mode=>{
    let conf_mode = get_mode();
    if (!['disabled', 'trial', 'enabled'].includes(conf_mode))
        conf_mode = 'disabled';
    return mode==conf_mode;
const is_test_enabled = name=>model.get(`rmt_conf.conn_test_${name}`, true);
const get_mode = ()=>model.get('rmt_conf.conn_test_mode',
    is_win ? 'enabled' : 'disabled');
const get_ignored = ()=>{
    let not_implemented = ['common_agents', 'mem_agents'];
    let conf_ignored = model.get('rmt_conf.conn_test_ignore', []);
    return not_implemented.concat(conf_ignored);
// server certificate issued for *.hola.org, so a fake name also does the trick
const proxy2server = p=>`${p.name||'zagent'}.hola.org`;
const proxy2str = p=>`${p.ip}:${p.port}`;
const port2str = port=>!port||port==443||port==80 ? '' : `:${port}`;
const port2proto = port=>port==80 ? 'http:' : 'https:';
const server2prf = server=>server=='www' ? '' : `${server}.`;
const mk_urls = (server, domain, ports, path)=>{
    ports = ports||[443];
    return ports.map(port=>
        port2proto(port)+'//'+server2prf(server)+domain+port2str(port)+path);
const hola_server_detect = url=>{
    let {hostname, path} = zurl.parse(url)||{};
    let found = ['client', 'www', 'perr'].find(s=>
        hostname==`${s}.hola.org` || s=='www' && hostname=='hola.org');
    return {hola_server: found, hostname, path};
const myip_get = (name, proxy, dev)=>etask(function*(){
    // relevant for proxy mode only
    if (!proxy)
        return;
    this.on('uncaught', e=>zerr(`connectivity test for ${name} failed to `+
        `detect myip: ${zerr.e2s(e)}`));
    try {
        let url = `https://${proxy.ip}:${proxy.port}/myip?full=1`;
        let agent = new https.Agent({servername: proxy2server(proxy)});
        let res = yield wget({url, agent, localAddress: dev && dev.addr,
            json: true, retry: 3, expect: 200});
        return res.resp.body;
    }
    catch(e){
        zerr(`connectivity test: failed to detect myip for ${name} connection`+
            zerr.e2s(e));
    }
const flush_pending = name=>{
    let queue = conn_tests[name].wait_queue||[];
    while (queue.length)
        queue.pop().continue();
const model_sync = ()=>model.set('conn_status', E.api.conn_status());
const run_one = (name, dev)=>etask({cancel: true}, function*(){
    if (!is_test_enabled(name))
        return;
    this.on('uncaught', e=>
        zerr(`connectivity test for ${name} failed: ${zerr.e2s(e)}`));
    this.finally(()=>{
        cc.evaluating = false;
        cc.et = null;
        model_sync();
    });
    let cc = conn_tests[name];
    cc.run_count = (cc.run_count||0)+1;
    if (cc.parallel)
        cc = zutil.clone_deep(cc);
    else if (cc.et)
        cc.et.return();
    cc.et = this;
    cc.evaluating = true;
    model_sync();
    zerr.notice(`starting connectivity test for ${name} `+
         `dev ${dev ? dev.addr : 'gw'} run ${cc.run_count} `);
    let test = new unblocker_lib.ConnectivityCheck({
        def_url: mk_urls(name, 'hola.org', cc.ports, cc.path)[0],
        hola: mk_urls(name, 'hola.org', cc.ports, cc.path),
        svd: mk_urls(name, 'svd-cdn.com', cc.ports, cc.path),
        hvpn: mk_urls(name, 'h-vpn.org', cc.ports, cc.path),
        http: mk_urls(name, 'hola.org', [80], cc.path),
        build_agents, cloud_agents,
        ignore: get_ignored(),
        api: {
            perr: opt=>zerr.perr('vpn.svc.'+opt.id, opt.info),
            ajax_via_proxy: (url, opt)=>etask({cancel: true}, function*(){
                let res = yield wget({
                    url: url.url,
                    method: 'HEAD',
                    headers: opt.hdrs,
                    proxy: opt.force=='proxy' && opt.agent &&
                        `https://${opt.agent.ip}:${opt.agent.port}`,
                    localAddress: dev && dev.addr,
                });
                return {orig_res: res.resp};
            }),
        },
    });
    yield test.run();
    if (test.is_completed())
    {
        let be_link = test.get_backend_link(), url = zurl.parse(be_link.url);
        cc.proto = url.protocol;
        cc.selected = url.hostname;
        cc.port = url.port;
        cc.proxy = be_link.agent &&
            zutil.pick(be_link.agent, 'ip', 'port', 'name');
        if (cc.check_myip)
            cc.myip = yield myip_get(name, cc.proxy, dev);
        zerr.notice('connectivity test completed successfully: '+
            'using %s%s%s for %s server run %s', cc.selected,
            port2str(cc.port), cc.proxy ? ` via ${proxy2str(cc.proxy)}`: '',
            name, cc.run_count);
    }
    else
    {
        cc.myip = undefined;
        zerr.notice('connectivity test failed: keep using %s%s%s for %s '+
            'server run %s', cc.selected, port2str(cc.port),
            cc.proxy ? ` via ${proxy2str(cc.proxy)})`: '', name, cc.run_count);
    }
    zerr.perr('vpn.svc.connectivity_test', {name, run: cc.run_count,
        mode: get_mode(), status: test.is_completed() ? 'success' : 'failure',
        test_state: test.get_selected_state(), selected: cc.selected,
        port: cc.port, proxy: cc.proxy&&proxy2str(cc.proxy),
        dev: dev&&dev.addr, myip: cc.myip});
    test.uninit();
    flush_pending(name);
    return zutil.pick(cc, 'selected', 'port', 'proxy', 'myip', 'proto');
const wget_complete = (url, res)=>assign({url}, res);
const wget_server = (name, path, opt)=>etask({cancel: true}, function*(){
    let cc = is_mode('enabled') ? conn_tests[name] : conn_tests_init[name];
    if (is_mode('enabled') && is_test_enabled(name) && (cc.running ||
        !cc.started))
    {
        // delay all server requests until connectivity test completed
        cc.wait_queue = cc.wait_queue||[];
        cc.wait_queue.push(this);
        yield this.wait();
    }
    let proxy = cc.proxy && `https://${proxy2str(cc.proxy)}`;
    let wget_opt = assign({proxy: proxy}, opt);
    let url = (cc.proto||'https:')+'//'+cc.selected+path;
    return wget_complete(url, yield wget(url, wget_opt));
E.run_connectivity_check = (servers, dev)=>etask({cancel: true}, function*(){
    servers.forEach(name=>conn_tests[name].started = true);
    if (is_mode('disabled'))
        return;
    return yield etask.all(servers.map(name=>run_one(name, dev)));
E.wget_client = (path, opt)=>wget_server('client', path, opt);
E.wget_www = (path, opt)=>wget_server('www', path, opt);
E.wget_perr = (path, opt)=>wget_server('perr', path, opt);
E.wget_detect = opt=>etask(function*(){
    let {hola_server, path} = hola_server_detect(opt.url);
    if (!hola_server)
        return wget_complete(opt.url, yield wget(opt));
    opt = zutil.omit(opt, ['url']);
    return yield E[`wget_${hola_server}`](path, opt);
E.hola_proxy = url=>{
    let name = hola_server_detect(url).hola_server;
    let cc = is_mode('enabled') ? conn_tests[name] : conn_tests_init[name];
    return cc && cc.proxy && `https://${proxy2str(cc.proxy)}`;
// exposed to C code
E.api = {
    conn_status: ()=>{
        let source = is_mode('enabled') ? conn_tests : conn_tests_init;
        return zutil.map_obj(zutil.pick(source, 'www', 'client', 'perr'), val=>
            zutil.pick(val, 'evaluating', 'selected', 'myip'));
    },
    conn_proxy: server=>{
        let source = is_mode('enabled') ? conn_tests : conn_tests_init;
        let proxy = source[server].proxy;
        return proxy ? `${proxy.ip}:${proxy.port}` : '';
    },
model_sync();
