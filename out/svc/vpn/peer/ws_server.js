// LICENSE_CODE ZON
'use strict'; /*zlint node:true*/
const zconf = require('../../../util/config.js');
const zws = require('../../../util/ws.js');
const zerr = require('../../../util/zerr.js');
const etask = require('../../../util/etask.js');
const full_vpn = require('./full_vpn.js');
const model = require('./model.js');
const http = require('http');
const _ = require('lodash');
const E = exports, assign = Object.assign;
class Client {
    constructor(conn, req){
        this.conn = conn;
        this.conn_id = Client.last_conn_id = (Client.last_conn_id||0)+1;
        this.watches = [];
        this.subscribe_components = {
            // prepared data must match callback.json format
            client: {branch: 'client_status', prepare: data=>({
                cid_js: data.cid,
                session_key_js: data.session_key,
                has_internet_js: data.has_internet,
                connected_js: data.connected,
                user_token: data.user_token,
                sync_token: data.sync_token,
            })},
            connectivity: {branch: 'conn_status',
                prepare: data=>({connectivity: data})},
            vpn: {branch: 'vpn_status', prepare: data=>({full_vpn: data})},
        };
        conn.on('disconnected', ()=>this.uninit.bind(this));
        zerr.notice('ws_server: client %d connected from %s', this.conn_id,
            req.headers['user-agent']);
    }
    ipc_subscribe(msg){
        if (!msg || !(msg.components instanceof Array))
            return;
        this.unwatch_all();
        let init_msg = {init: true, changed: [], data: {}};
        for (let name of msg.components)
        {
            let comp;
            if (!(comp = this.subscribe_components[name]))
                continue;
            this.watches.push(model.watch(comp.branch, data=>{
                this.conn.ipc.push_notify({changed: [name],
                    data: comp.prepare(data)});
            }, false));
            init_msg.changed.push(name);
            assign(init_msg.data, comp.prepare(model.get(comp.branch)));
        }
        if (init_msg.changed.length)
            this.conn.ipc.push_notify(init_msg);
        return {status: 'success'};
    }
    unwatch_all(){
        let watch_uninit;
        while (watch_uninit = this.watches.pop())
            watch_uninit();
    }
    uninit(){
        zerr.notice('ws_server: client %d disconnected', this.conn_id);
        this.unwatch_all();
        this.conn = null;
    }
class Server {
    constructor(){
        this.watch_uninit = model.watch('rmt_conf', this.reconf.bind(this));
    }
    uninit(){
        this.watch_uninit();
        this.stop();
    }
    reconf(conf){
        conf = conf||{};
        if (conf.wss_enabled && !this.ws_server)
            this.start();
        else if (!conf.wss_enabled && this.ws_server)
            this.stop();
    }
    ready(et){ return et.wait_ext(this.start_et); }
    start(){ return etask({state0_args: [this]}, function*(_this){
        if (_this.ws_server)
            return;
        this.on('uncaught', e=>zerr('ws_server: start failed: '+zerr.e2s(e)));
        this.finally(()=>_this.start_et = null);
        _this.start_et = this;
        let server;
        if (!(server = yield _this.find_free_port()))
        {
            zerr.perr('vpn.svc.wss_start_err', {},
                {filehead: zerr.log_tail()});
            return zerr('ws_server: start failed');
        }
        _this.ws_server = new zws.Server({
            http_server: server,
            ipc_server: {
                vpn_connect: full_vpn.api.vpn_connect,
                vpn_disconnect: full_vpn.api.vpn_disconnect,
                vpn_change_agent: full_vpn.api.vpn_change_agent,
                vpn_status: full_vpn.api.vpn_status,
                push_subscribe: _this.bridge_client_fn('ipc_subscribe'),
            },
            ipc_client: {
                push_notify: 'post',
            },
            ping: true,
            mux: true,
            ping_interval: 60*1000,
            ping_timeout: 10*1000,
            origin_whitelist: [
                'chrome-extension://'+zconf.BEXT_CHROME_ID_REL,
                'chrome-extension://'+zconf.BEXT_CHROME_CWS_ID_REL,
                'chrome-extension://'+zconf.BEXT_OPERA_ADDONS_ID_REL,
                zconf.BEXT_FF_ORIGIN, zconf.BEXT_FF_ORIGIN_SIGNED,
                'https://hola.org', 'https://www.hola.org',
                'https://h-vpn.org', 'https://www.h-vpn.org',
                'https://svd-cdn.com', 'https://www.svd-cdn.com',
                'app://hola-ui',
            ],
        }, (conn, req)=>new Client(conn, req));
        _this.ws_port = server.address().port;
        zerr.notice('ws_server: started on %d', _this.ws_port);
        return _this; });
    }
    stop(){
        if (this.start_et)
            this.start_et.return();
        if (this.ws_server)
        {
            zerr.notice(`ws_server: shutdown ${this.ws_port}`);
            this.ws_server.close();
            this.ws_server = null;
            this.ws_port = 0;
        }
    }
    get_port(){ return this.ws_port||0; }
    find_free_port(){ return etask({state0_args: [this]}, function(_this){
        let server = http.createServer();
        server.on('error', err=>{
            zerr.notice('ws_server: http server start error %s', err.code);
            server.close();
            this.return();
        });
        server.on('listening', ()=>this.return(server));
        server.listen(0, '127.0.0.1');
        return this.wait(); });
    }
    bridge_client_fn(name){
        return function(msg){ return this[name](msg); };
    }
var ws_srv;
E.api = {
    wss_port: ()=>ws_srv ? ws_srv.get_port() : 0,
E.init = ()=>ws_srv = new Server();
E.uninit = ()=>ws_srv = ws_srv && ws_srv.uninit();
E.t = {Client, Server};
