// LICENSE_CODE ZON
'use strict'; /*zlint node:true*/
require('../../../util/config.js');
let events = require('events');
let zutil = require('../../../util/util.js');
let E = exports;
var global_events, global_conf;
E.reset = ()=>{
    global_events = new events.EventEmitter();
    global_conf = {};
E.reset();
E.get = (path, def)=>zutil.get(global_conf, path, def);
E.get_clone = (path, def)=>zutil.clone_deep(E.get(path, def));
E.set = (path, new_value)=>{
    let old_value = E.get(path);
    if (zutil.equal_deep(old_value, new_value))
        return;
    let branch = path.split('.')[0];
    let from = E.get_clone(branch);
    zutil.set(global_conf, path, zutil.clone(new_value));
    let to = E.get_clone(branch);
    global_events.emit(branch, to, from);
E.merge = (path, new_value)=>{
    let old_value = E.get(path);
    new_value = Object.assign({}, old_value, new_value);
    E.set(path, new_value);
E.watch = (branch, cb, immediate_update=true)=>{
    global_events.addListener(branch, cb);
    if (immediate_update)
        cb(E.get_clone(branch));
    return E.unwatch.bind(E, branch, cb);
E.unwatch = (branch, cb)=>global_events.removeListener(branch, cb);
