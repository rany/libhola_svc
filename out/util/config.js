// LICENSE_CODE ZON
'use strict'; /*jslint node:true, es6:false*/ // ios
require('./es6_shim.js');
require('./libpatch.js');
var env = process.env;
function conf(path){ try { E = require(path); } catch(err){} }
var E;
conf('../zon_config.js');
if (!E && env.BUILD)
    conf('../../build.'+env.BUILD+'/pkg/zon_config.js');
if (!E && (env.ZON_CONFIG_FALLBACK || global.zon_config_fallback))
    var is_win = /^win/.test(process.platform);
    conf((is_win ? 'C:/cygwin' : '')+'/usr/local/hola/zon_config.js');
    E = E||global.zon_config_fallback||{};
    E.BUILDDIR = E.TOPDIR = undefined;
if (!E)
    if (env.BUILD)
        throw new Error('BUILD='+env.BUILD+' not built, run jmake');
    throw new Error('BUILD not selected, use sb first');
module.exports = E;
