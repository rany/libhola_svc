// LICENSE_CODE ZON
'use strict'; /*zlint node*/
require('./config.js');
var zutil = require('./util.js');
var array = require('./array.js');
var fs = require('fs');
var asn1js_common = require('asn1js/org/pkijs/common');
var asn1js = require('asn1js');
var pkijs = require('pkijs');
var x509schema = require('pkijs/org/pkijs/x509_schema');
var crypto = require('crypto');
var E = exports;
E.pkijs = pkijs.org.pkijs;
function uint32_to_buffer(i){
    let res = Buffer.alloc(4);
    res.writeUInt32BE(i);
    return res;
function x509_find_extension(cert, id){
    for (var i=0; i<cert.extensions.length; i++)
    {
        if (cert.extensions[i].extnID==id)
            return cert.extensions[i].parsedValue;
    }
var typemap = {
    '2.5.4.6': 'C',
    '2.5.4.10': 'OU',
    '2.5.4.11': 'O',
    '2.5.4.3': 'CN',
    '2.5.4.7': 'L',
    '2.5.4.8': 'S',
    '2.5.4.12': 'T',
    '2.5.4.42': 'GN',
    '2.5.4.43': 'I',
    '2.5.4.4': 'SN',
function x509_parse_id(id){
    var res = {};
    for (var i=0, parts = id.types_and_values; i<parts.length; i++)
    {
        var key = typemap[parts[i].type];
        res[key] = parts[i].value.value_block.value;
    }
    return res;
E.x509_subject = function(cert){ return x509_parse_id(cert.subject); };
E.x509_issuer = function(cert){ return x509_parse_id(cert.issuer); };
E.x509_alt_names = function(cert){
    var res = [], ext_value;
    // 'Subject Alternative Name'
    if (!(ext_value = x509_find_extension(cert, '2.5.29.17')))
        return res;
    ext_value.altNames.forEach(function(alt_name){
        // type is 'dNSName'
        if (alt_name.NameType==2)
            res.push(alt_name.Name);
    });
    return res;
E.x509_all_names = function(cert){
    var res = E.x509_alt_names(cert), cn;
    if (cn = E.x509_subject(cert).CN)
        array.add_elm(res, cn);
    return res;
E.x509_der_parse = function(node_buffer){
    var asn1 = E.pkijs.fromBER(new Uint8Array(node_buffer).buffer);
    var cert = new E.pkijs.simpl.CERT({schema: asn1.result});
    return cert;
E.x509_pem_parse = function(str){
    var base64 = str.replace(/(-----(BEGIN|END) CERTIFICATE-----|\n)/g, '');
    return E.x509_der_parse(Buffer.from(base64, 'base64'));
E.x509_pubkey = function(x509){
    let asn1_pub = new E.pkijs.fromBER(
        x509.subjectPublicKeyInfo.subjectPublicKey.value_block.value_hex);
    let rsa_pub = new E.pkijs.simpl.x509.RSAPublicKey({
        schema: asn1_pub.result});
    let mod = Buffer.from(rsa_pub.modulus.value_block.value_hex);
    let exp = Buffer.from(rsa_pub.publicExponent.value_block.value_hex);
    return {mod, exp};
function ssh_next(opt){
    let len = opt.buf.readUInt32BE(opt.pos);
    opt.pos += 4;
    let res = opt.buf.slice(opt.pos, opt.pos+len);
    opt.pos += len;
    return res;
E.ssh_pubkey = function(pub_str){
    let data = {pos: 0, buf: Buffer.from(pub_str.split(' ')[1], 'base64')};
    let type = ssh_next(data).toString();
    if (type!='ssh-rsa')
        throw new Error('Non-RSA keys are not supported: '+type);
    let exp = ssh_next(data);
    let mod = ssh_next(data);
    return {mod, exp};
function encode_ssh_pubkey(pub){
    return Buffer.concat([uint32_to_buffer(7), Buffer.from('ssh-rsa'),
        uint32_to_buffer(pub.exp.length), pub.exp,
        uint32_to_buffer(pub.mod.length), pub.mod]);
function pub_fp(pub, opt){
    opt = opt||{};
    let hash = crypto.createHash(opt.ssh_compat ? 'md5' : 'sha1');
    if (opt.ssh_compat)
        hash.update(encode_ssh_pubkey(pub));
    else
        hash.update(pub.mod).update(pub.exp);
    return hash.digest('hex');
E.cert_pub_fingerprint = function(fname, opt){
    let src = fs.readFileSync(fname).toString();
    let pub = E.x509_pubkey(E.x509_pem_parse(src));
    return pub_fp(pub, opt);
E.ssh_pub_fingerprint = function(fname, opt){
    let src = fs.readFileSync(fname).toString();
    let pub = E.ssh_pubkey(src);
    return pub_fp(pub, opt);
zutil.extend_deep(asn1js, asn1js_common);
zutil.extend_deep(x509schema, asn1js);
zutil.extend_deep(pkijs, x509schema, asn1js);
// z* prefixed self defined asn1 rules to parse only needed parts
let asn1_js, asn1_js_5280, zstr, zext, zname, znames, zcert;
E.x509_all_names_der = buf=>{
    asn1_js = asn1_js||require('asn1.js');
    asn1_js_5280 = asn1_js_5280||require('asn1.js-rfc5280');
    zstr = zstr||asn1_js.define('zstr', function(){ this.choice({
        prt: this.printstr(), utf: this.utf8str(), uni: this.unistr(),
        bmp: this.bmpstr(), bit: this.bitstr(), chr: this.charstr(),
        gen: this.genstr(), grp: this.graphstr(), ia5: this.ia5str(),
        iso: this.iso646str(), num: this.numstr(), oct: this.octstr(),
        t61: this.t61str(), vid: this.videostr()});
    });
    zext = zext||asn1_js.define('zext', function(){ this.seq().obj(
        this.key('extnID').objid(asn1_js_5280.x509OIDs),
        this.key('critical').bool().def(false),
        this.key('extnValue').octstr()
    ); });
    zname = zname||asn1_js.define('zname', function(){ this.choice({
        dNSName: this.implicit(2).ia5str(),
        iPAddress: this.implicit(7).octstr(),
        otherName: this.implicit(0).seq().obj(),
        rfc822Name: this.implicit(1).ia5str(),
        directoryName: this.explicit(4).use(asn1_js_5280.Name),
        ediPartyName: this.implicit(5).seq().obj(),
        uniformResourceIdentifier: this.implicit(6).ia5str(),
        registeredID: this.implicit(8).objid()});
    });
    znames = znames||asn1_js.define('znames', function(){
        this.seqof(zname); });
    zcert = zcert||asn1_js.define('zcert', function(){ this.seq().obj(
        this.key('tbsCertificate').seq().obj(
            this.key('version').explicit(0).int(),
            this.key('serialNumber').int(),
            this.key('signature').seq().obj(),
            this.key('issuer').use(asn1_js_5280.Name),
            this.key('validity').seq().obj(),
            this.key('subject').use(asn1_js_5280.Name),
            this.key('subjectPublicKeyInfo').seq().obj(),
            this.key('issuerUniqueID').optional().implicit(1).bitstr(),
            this.key('subjectUniqueID').optional().implicit(2).bitstr(),
            this.key('extensions').optional().explicit(3).seqof(zext))
    ); });
    let cert, names = [], cn;
    cert = zcert.decode(buf, 'der', {label: 'CERTIFICATE'}).tbsCertificate;
    for (let item of cert.extensions)
    {
        if (item.extnID.join('.')!='2.5.29.17')
            continue;
        znames.decode(item.extnValue).forEach(ev=>{
            if (ev.type=='dNSName')
                names.push(ev.value);
        });
    }
    if (cn = cert.subject.value.find(v=>typemap[v[0].type.join('.')]=='CN'))
        array.add_elm(names, zstr.decode(cn[0].value).value);
    return names;
