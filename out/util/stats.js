// LICENSE_CODE ZON
'use strict'; /*zlint node, br*/
(function(){
var define;
var is_node = typeof module=='object' && module.exports && module.children;
if (!is_node)
    define = self.define;
else
    define = require('./require_node.js').define(module, '../');
define([], function(){
var E = {};
// Exponentially weighted moving average:
// This statistic is a progressively updated average of a time series where
// more recent data points have bigger weights than earlier data pooints.
// A similar statistic is used by the Linux kernel for the load average.
// Unlike more typical EWMA implementations, this one takes datapoints with
// optional timestamps, which must be strictly monotonic; if omitted, they are
// assumed to be 0, 1, 2...
// The half_life argument to the constructor specifies the time period after
// which an old datapoint weighs exactly half as much as a new one. After two
// such periods, it weighs a quarter of a new one, and so on.
E.ewma_series = function(half_life){
    this.half_life = half_life||1;
    this.reset();
E.ewma_series.prototype.reset = function(){
    this.current = null;
    this.timestamp = null;
    return this;
E.ewma_series.prototype.append = function(value, timestamp){
    if (this.timestamp===null)
    {
        this.current = value;
        this.timestamp = timestamp||0;
        return;
    }
    if (!timestamp)
        timestamp = this.timestamp+1;
    var factor = Math.pow(0.5, (timestamp-this.timestamp) / this.half_life);
    this.current = (this.current*factor+value) / (factor+1);
    this.timestamp = timestamp;
    return this;
E.ewma_series.prototype.get = function(){ return this.current; };
return E; }); }());
